SELECT adm.`id_admission` as id , adm.`code`, adm.`code_preinscription`, adm.`code_cab`, adm.`code_organisme`, adm.`statut`, adm.`fermer`, adm.`code_statut`, adm.`date_creation` ,
pre.id_preinscription as t_preinscription_id , 
org.id as p_organisme_id , 
st.id as p_statut_id 
FROM `t_admission` adm
left join t_preinscription pre on pre.code = adm.code_preinscription 
left join p_organisme org on org.code = adm.code_organisme 
left join p_statut st on st.code = adm.code_statut 
order by adm.id_admission asc