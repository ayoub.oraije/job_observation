<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TPreinscription
 *
 * @ORM\Table(name="t_preinscription")
 * @ORM\Entity
 */
class TPreinscription {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="code_etudiant", type="string", length=100, nullable=true)
     */
    private $codeEtudiant;

    /**
     * @var string
     *
     * @ORM\Column(name="code_cab", type="string", length=100, nullable=true)
     */
    private $codeCab;

    /**
     * @var string
     *
     * @ORM\Column(name="inscription_valide", type="string", length=1, nullable=false)
     */
    private $inscriptionValide;

    /**
     * @var integer
     *
     * @ORM\Column(name="rang_p", type="integer", nullable=false)
     */
    private $rangP;

    /**
     * @var integer
     *
     * @ORM\Column(name="rang_s", type="integer", nullable=false)
     */
    private $rangS;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_liste", type="string", length=100, nullable=true)
     */
    private $categorieListe;

    /**
     * @var string
     *
     * @ORM\Column(name="admission_liste", type="string", length=100, nullable=true)
     */
    private $admissionListe;

    /**
     * @var string
     *
     * @ORM\Column(name="tele_liste", type="string", length=100, nullable=true)
     */
    private $teleListe;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_deliberation", type="string", length=255, nullable=true)
     */
    private $statutDeliberation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="utilisateur", type="string", length=100, nullable=true)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="TEtudiant")
     * @ORM\JoinColumn(name="t_etudiant_id", referencedColumnName="id")
     */
    private $etudiant;

    /**
   
     * * @ORM\Column(name="t_preinscriptioncab_id", type="string", length=255, nullable=true)
     
     */
    
    
    private $preinscriptionCab;

    /**
     * @ORM\ManyToOne(targetEntity="PStatut")
     * @ORM\JoinColumn(name="p_statut_categorie_id", referencedColumnName="id")
     */
    private $statutCategorie;

    /**
     * @ORM\ManyToOne(targetEntity="PStatut")
     * @ORM\JoinColumn(name="p_statut_admission_id", referencedColumnName="id")
     */
    private $statutAdmission;

    /**
     * @ORM\ManyToOne(targetEntity="PStatut")
     * @ORM\JoinColumn(name="p_statut_tele_id", referencedColumnName="id")
     */
    private $statutTele;

     
     
     
     
     
     
    

    public function __construct() {


      
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TPreinscription
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeEtudiant
     *
     * @param string $codeEtudiant
     *
     * @return TPreinscription
     */
    public function setCodeEtudiant($codeEtudiant)
    {
        $this->codeEtudiant = $codeEtudiant;
    
        return $this;
    }

    /**
     * Get codeEtudiant
     *
     * @return string
     */
    public function getCodeEtudiant()
    {
        return $this->codeEtudiant;
    }

    /**
     * Set codeCab
     *
     * @param string $codeCab
     *
     * @return TPreinscription
     */
    public function setCodeCab($codeCab)
    {
        $this->codeCab = $codeCab;
    
        return $this;
    }

    /**
     * Get codeCab
     *
     * @return string
     */
    public function getCodeCab()
    {
        return $this->codeCab;
    }

    /**
     * Set inscriptionValide
     *
     * @param string $inscriptionValide
     *
     * @return TPreinscription
     */
    public function setInscriptionValide($inscriptionValide)
    {
        $this->inscriptionValide = $inscriptionValide;
    
        return $this;
    }

    /**
     * Get inscriptionValide
     *
     * @return string
     */
    public function getInscriptionValide()
    {
        return $this->inscriptionValide;
    }

    /**
     * Set rangP
     *
     * @param integer $rangP
     *
     * @return TPreinscription
     */
    public function setRangP($rangP)
    {
        $this->rangP = $rangP;
    
        return $this;
    }

    /**
     * Get rangP
     *
     * @return integer
     */
    public function getRangP()
    {
        return $this->rangP;
    }

    /**
     * Set rangS
     *
     * @param integer $rangS
     *
     * @return TPreinscription
     */
    public function setRangS($rangS)
    {
        $this->rangS = $rangS;
    
        return $this;
    }

    /**
     * Get rangS
     *
     * @return integer
     */
    public function getRangS()
    {
        return $this->rangS;
    }

    /**
     * Set categorieListe
     *
     * @param string $categorieListe
     *
     * @return TPreinscription
     */
    public function setCategorieListe($categorieListe)
    {
        $this->categorieListe = $categorieListe;
    
        return $this;
    }

    /**
     * Get categorieListe
     *
     * @return string
     */
    public function getCategorieListe()
    {
        return $this->categorieListe;
    }

    /**
     * Set admissionListe
     *
     * @param string $admissionListe
     *
     * @return TPreinscription
     */
    public function setAdmissionListe($admissionListe)
    {
        $this->admissionListe = $admissionListe;
    
        return $this;
    }

    /**
     * Get admissionListe
     *
     * @return string
     */
    public function getAdmissionListe()
    {
        return $this->admissionListe;
    }

    /**
     * Set teleListe
     *
     * @param string $teleListe
     *
     * @return TPreinscription
     */
    public function setTeleListe($teleListe)
    {
        $this->teleListe = $teleListe;
    
        return $this;
    }

    /**
     * Get teleListe
     *
     * @return string
     */
    public function getTeleListe()
    {
        return $this->teleListe;
    }

    /**
     * Set statutDeliberation
     *
     * @param string $statutDeliberation
     *
     * @return TPreinscription
     */
    public function setStatutDeliberation($statutDeliberation)
    {
        $this->statutDeliberation = $statutDeliberation;
    
        return $this;
    }

    /**
     * Get statutDeliberation
     *
     * @return string
     */
    public function getStatutDeliberation()
    {
        return $this->statutDeliberation;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return TPreinscription
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set utilisateur
     *
     * @param string $utilisateur
     *
     * @return TPreinscription
     */
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;
    
        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return string
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set preinscriptionCab
     *
     * @param string $preinscriptionCab
     *
     * @return TPreinscription
     */
    public function setPreinscriptionCab($preinscriptionCab)
    {
        $this->preinscriptionCab = $preinscriptionCab;
    
        return $this;
    }

    /**
     * Get preinscriptionCab
     *
     * @return string
     */
    public function getPreinscriptionCab()
    {
        return $this->preinscriptionCab;
    }

    /**
     * Set etudiant
     *
     * @param \AppBundle\Entity\TEtudiant $etudiant
     *
     * @return TPreinscription
     */
    public function setEtudiant(\AppBundle\Entity\TEtudiant $etudiant = null)
    {
        $this->etudiant = $etudiant;
    
        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \AppBundle\Entity\TEtudiant
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set statutCategorie
     *
     * @param \AppBundle\Entity\PStatut $statutCategorie
     *
     * @return TPreinscription
     */
    public function setStatutCategorie(\AppBundle\Entity\PStatut $statutCategorie = null)
    {
        $this->statutCategorie = $statutCategorie;
    
        return $this;
    }

    /**
     * Get statutCategorie
     *
     * @return \AppBundle\Entity\PStatut
     */
    public function getStatutCategorie()
    {
        return $this->statutCategorie;
    }

    /**
     * Set statutAdmission
     *
     * @param \AppBundle\Entity\PStatut $statutAdmission
     *
     * @return TPreinscription
     */
    public function setStatutAdmission(\AppBundle\Entity\PStatut $statutAdmission = null)
    {
        $this->statutAdmission = $statutAdmission;
    
        return $this;
    }

    /**
     * Get statutAdmission
     *
     * @return \AppBundle\Entity\PStatut
     */
    public function getStatutAdmission()
    {
        return $this->statutAdmission;
    }

    /**
     * Set statutTele
     *
     * @param \AppBundle\Entity\PStatut $statutTele
     *
     * @return TPreinscription
     */
    public function setStatutTele(\AppBundle\Entity\PStatut $statutTele = null)
    {
        $this->statutTele = $statutTele;
    
        return $this;
    }

    /**
     * Get statutTele
     *
     * @return \AppBundle\Entity\PStatut
     */
    public function getStatutTele()
    {
        return $this->statutTele;
    }
}
