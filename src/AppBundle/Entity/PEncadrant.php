<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PEncadrant
 *
 * @ORM\Table(name="p_encadrant")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PEncadrantRepository")
 */
class PEncadrant {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true, unique=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true, unique=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="encNiv1")
     */
    private $patient_encadrant1;

    /**
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="encNiv2")
     */
    private $patient_encadrant2;

    /**
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="encHop1")
     */
    private $patient_encadrant3;

    /**
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="encHop2")
     */
    private $patient_encadrant4;

    /**
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="encRes1")
     */
    private $patient_encadrant5;

    /**
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="encRes2")
     */
    private $patient_encadrant6;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->patient_encadrant1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patient_encadrant2 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patient_encadrant3 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patient_encadrant4 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patient_encadrant5 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patient_encadrant6 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return PEncadrant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return PEncadrant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PEncadrant
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add patientEncadrant1
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant1
     *
     * @return PEncadrant
     */
    public function addPatientEncadrant1(\AppBundle\Entity\PPatient $patientEncadrant1)
    {
        $this->patient_encadrant1[] = $patientEncadrant1;
    
        return $this;
    }

    /**
     * Remove patientEncadrant1
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant1
     */
    public function removePatientEncadrant1(\AppBundle\Entity\PPatient $patientEncadrant1)
    {
        $this->patient_encadrant1->removeElement($patientEncadrant1);
    }

    /**
     * Get patientEncadrant1
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientEncadrant1()
    {
        return $this->patient_encadrant1;
    }

    /**
     * Add patientEncadrant2
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant2
     *
     * @return PEncadrant
     */
    public function addPatientEncadrant2(\AppBundle\Entity\PPatient $patientEncadrant2)
    {
        $this->patient_encadrant2[] = $patientEncadrant2;
    
        return $this;
    }

    /**
     * Remove patientEncadrant2
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant2
     */
    public function removePatientEncadrant2(\AppBundle\Entity\PPatient $patientEncadrant2)
    {
        $this->patient_encadrant2->removeElement($patientEncadrant2);
    }

    /**
     * Get patientEncadrant2
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientEncadrant2()
    {
        return $this->patient_encadrant2;
    }

    /**
     * Add patientEncadrant3
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant3
     *
     * @return PEncadrant
     */
    public function addPatientEncadrant3(\AppBundle\Entity\PPatient $patientEncadrant3)
    {
        $this->patient_encadrant3[] = $patientEncadrant3;
    
        return $this;
    }

    /**
     * Remove patientEncadrant3
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant3
     */
    public function removePatientEncadrant3(\AppBundle\Entity\PPatient $patientEncadrant3)
    {
        $this->patient_encadrant3->removeElement($patientEncadrant3);
    }

    /**
     * Get patientEncadrant3
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientEncadrant3()
    {
        return $this->patient_encadrant3;
    }

    /**
     * Add patientEncadrant4
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant4
     *
     * @return PEncadrant
     */
    public function addPatientEncadrant4(\AppBundle\Entity\PPatient $patientEncadrant4)
    {
        $this->patient_encadrant4[] = $patientEncadrant4;
    
        return $this;
    }

    /**
     * Remove patientEncadrant4
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant4
     */
    public function removePatientEncadrant4(\AppBundle\Entity\PPatient $patientEncadrant4)
    {
        $this->patient_encadrant4->removeElement($patientEncadrant4);
    }

    /**
     * Get patientEncadrant4
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientEncadrant4()
    {
        return $this->patient_encadrant4;
    }

    /**
     * Add patientEncadrant5
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant5
     *
     * @return PEncadrant
     */
    public function addPatientEncadrant5(\AppBundle\Entity\PPatient $patientEncadrant5)
    {
        $this->patient_encadrant5[] = $patientEncadrant5;
    
        return $this;
    }

    /**
     * Remove patientEncadrant5
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant5
     */
    public function removePatientEncadrant5(\AppBundle\Entity\PPatient $patientEncadrant5)
    {
        $this->patient_encadrant5->removeElement($patientEncadrant5);
    }

    /**
     * Get patientEncadrant5
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientEncadrant5()
    {
        return $this->patient_encadrant5;
    }

    /**
     * Add patientEncadrant6
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant6
     *
     * @return PEncadrant
     */
    public function addPatientEncadrant6(\AppBundle\Entity\PPatient $patientEncadrant6)
    {
        $this->patient_encadrant6[] = $patientEncadrant6;
    
        return $this;
    }

    /**
     * Remove patientEncadrant6
     *
     * @param \AppBundle\Entity\PPatient $patientEncadrant6
     */
    public function removePatientEncadrant6(\AppBundle\Entity\PPatient $patientEncadrant6)
    {
        $this->patient_encadrant6->removeElement($patientEncadrant6);
    }

    /**
     * Get patientEncadrant6
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientEncadrant6()
    {
        return $this->patient_encadrant6;
    }
}
