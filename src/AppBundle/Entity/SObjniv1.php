<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SObjniv1
 *
 * @ORM\Table(name="s_objniv1")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SObjniv1Repository")
 */
class SObjniv1 {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=255, nullable=true)
     */
    private $abreviation;

    /**
     * @ORM\ManyToOne(targetEntity="s_stages", inversedBy="objNiv1")
     * @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     */
    private $stage;

    /**
     * @ORM\OneToMany(targetEntity="SObjniv2", mappedBy="objNiv1")
     */
    private $objNiv2;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;

  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->objNiv2 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return SObjniv1
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return SObjniv1
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SObjniv1
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return SObjniv1
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     *
     * @return SObjniv1
     */
    public function setStage(\AppBundle\Entity\s_stages $stage = null)
    {
        $this->stage = $stage;
    
        return $this;
    }

    /**
     * Get stage
     *
     * @return \AppBundle\Entity\s_stages
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Add objNiv2
     *
     * @param \AppBundle\Entity\SObjniv2 $objNiv2
     *
     * @return SObjniv1
     */
    public function addObjNiv2(\AppBundle\Entity\SObjniv2 $objNiv2)
    {
        $this->objNiv2[] = $objNiv2;
    
        return $this;
    }

    /**
     * Remove objNiv2
     *
     * @param \AppBundle\Entity\SObjniv2 $objNiv2
     */
    public function removeObjNiv2(\AppBundle\Entity\SObjniv2 $objNiv2)
    {
        $this->objNiv2->removeElement($objNiv2);
    }

    /**
     * Get objNiv2
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjNiv2()
    {
        return $this->objNiv2;
    }
}
