<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * AcAnnee
 *
 * @ORM\Table(name="ac_annee")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcAnneeRepository")
 */
 
class AcAnnee
{
       /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="code_etablissement", type="string", length=100, nullable=true)
     */
    private $codeEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="code_formation", type="string", length=100, nullable=true)
     */
    private $codeFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="code_promotion", type="string", length=100, nullable=true)
     */
    private $codePromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="validation_academique", type="string", length=3, nullable=true)
     */
    private $validationAcademique ;

    /**
     * @var string
     *
     * @ORM\Column(name="cloture_academique", type="string", length=3, nullable=true)
     */
    private $clotureAcademique;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="AcEtablissement")
     * @ORM\JoinColumn(name="ac_etablissement_id", referencedColumnName="id")
     */
    private $etablissement;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="AcFormation")
     * @ORM\JoinColumn(name="ac_formation_id", referencedColumnName="id")
     */
    private $formation;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return AcAnnee
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeEtablissement
     *
     * @param string $codeEtablissement
     *
     * @return AcAnnee
     */
    public function setCodeEtablissement($codeEtablissement)
    {
        $this->codeEtablissement = $codeEtablissement;

        return $this;
    }

    /**
     * Get codeEtablissement
     *
     * @return string
     */
    public function getCodeEtablissement()
    {
        return $this->codeEtablissement;
    }

    /**
     * Set codeFormation
     *
     * @param string $codeFormation
     *
     * @return AcAnnee
     */
    public function setCodeFormation($codeFormation)
    {
        $this->codeFormation = $codeFormation;

        return $this;
    }

    /**
     * Get codeFormation
     *
     * @return string
     */
    public function getCodeFormation()
    {
        return $this->codeFormation;
    }

    /**
     * Set codePromotion
     *
     * @param string $codePromotion
     *
     * @return AcAnnee
     */
    public function setCodePromotion($codePromotion)
    {
        $this->codePromotion = $codePromotion;

        return $this;
    }

    /**
     * Get codePromotion
     *
     * @return string
     */
    public function getCodePromotion()
    {
        return $this->codePromotion;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return AcAnnee
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set validationAcademique
     *
     * @param string $validationAcademique
     *
     * @return AcAnnee
     */
    public function setValidationAcademique($validationAcademique)
    {
        $this->validationAcademique = $validationAcademique;

        return $this;
    }

    /**
     * Get validationAcademique
     *
     * @return string
     */
    public function getValidationAcademique()
    {
        return $this->validationAcademique;
    }

    /**
     * Set clotureAcademique
     *
     * @param string $clotureAcademique
     *
     * @return AcAnnee
     */
    public function setClotureAcademique($clotureAcademique)
    {
        $this->clotureAcademique = $clotureAcademique;

        return $this;
    }

    /**
     * Get clotureAcademique
     *
     * @return string
     */
    public function getClotureAcademique()
    {
        return $this->clotureAcademique;
    }

    /**
     * Set etablissement
     *
     * @param \AppBundle\Entity\AcEtablissement $etablissement
     *
     * @return AcAnnee
     */
    public function setEtablissement(\AppBundle\Entity\AcEtablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;
    
        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \AppBundle\Entity\AcEtablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Set formation
     *
     * @param \AppBundle\Entity\AcFormation $formation
     *
     * @return AcAnnee
     */
    public function setFormation(\AppBundle\Entity\AcFormation $formation = null)
    {
        $this->formation = $formation;
    
        return $this;
    }

    /**
     * Get formation
     *
     * @return \AppBundle\Entity\AcFormation
     */
    public function getFormation()
    {
        return $this->formation;
    }
}
