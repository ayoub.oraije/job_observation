<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * TAdmission
 *
 * @ORM\Table(name="t_admission")
 * @ORM\Entity
 */
class TAdmission
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="code_preinscription", type="string", length=100, nullable=true)
     */
    private $codePreinscription;

    /**
     * @var string
     *
     * @ORM\Column(name="code_cab", type="string", length=100, nullable=true)
     */
    private $codeCab;

    /**
     * @var string
     *
     * @ORM\Column(name="code_organisme", type="string", length=10, nullable=true)
     */
    private $codeOrganisme;

    /**
     * @var integer
     *
     * @ORM\Column(name="statut", type="integer", nullable=false)
     */
    private $st;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fermer", type="boolean", nullable=false)
     */
    private $fermer ;

    /**
     * @var string
     *
     * @ORM\Column(name="code_statut", type="string", length=10, nullable=true)
     */
    private $codeStatut ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    private $dateCreation ;
    
    
    
    
    
     /**
     * @ORM\ManyToOne(targetEntity="TPreinscription")
     * @ORM\JoinColumn(name="t_preinscription_id", referencedColumnName="id")
     */
    private $preinscription;
    
    
    
    
     /**
     * @ORM\ManyToOne(targetEntity="POrganisme")
     * @ORM\JoinColumn(name="p_organisme_id", referencedColumnName="id")
     */
    private $organisme;
    
    
     /**
     * @ORM\ManyToOne(targetEntity="PStatut")
     * @ORM\JoinColumn(name="p_statut_id", referencedColumnName="id")
     */
    private $statut;
    
    
    
    

    

    public function __construct() {


        
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TAdmission
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codePreinscription
     *
     * @param string $codePreinscription
     *
     * @return TAdmission
     */
    public function setCodePreinscription($codePreinscription)
    {
        $this->codePreinscription = $codePreinscription;
    
        return $this;
    }

    /**
     * Get codePreinscription
     *
     * @return string
     */
    public function getCodePreinscription()
    {
        return $this->codePreinscription;
    }

    /**
     * Set codeCab
     *
     * @param string $codeCab
     *
     * @return TAdmission
     */
    public function setCodeCab($codeCab)
    {
        $this->codeCab = $codeCab;
    
        return $this;
    }

    /**
     * Get codeCab
     *
     * @return string
     */
    public function getCodeCab()
    {
        return $this->codeCab;
    }

    /**
     * Set codeOrganisme
     *
     * @param string $codeOrganisme
     *
     * @return TAdmission
     */
    public function setCodeOrganisme($codeOrganisme)
    {
        $this->codeOrganisme = $codeOrganisme;
    
        return $this;
    }

    /**
     * Get codeOrganisme
     *
     * @return string
     */
    public function getCodeOrganisme()
    {
        return $this->codeOrganisme;
    }

    /**
     * Set st
     *
     * @param integer $st
     *
     * @return TAdmission
     */
    public function setSt($st)
    {
        $this->st = $st;
    
        return $this;
    }

    /**
     * Get st
     *
     * @return integer
     */
    public function getSt()
    {
        return $this->st;
    }

    /**
     * Set fermer
     *
     * @param boolean $fermer
     *
     * @return TAdmission
     */
    public function setFermer($fermer)
    {
        $this->fermer = $fermer;
    
        return $this;
    }

    /**
     * Get fermer
     *
     * @return boolean
     */
    public function getFermer()
    {
        return $this->fermer;
    }

    /**
     * Set codeStatut
     *
     * @param string $codeStatut
     *
     * @return TAdmission
     */
    public function setCodeStatut($codeStatut)
    {
        $this->codeStatut = $codeStatut;
    
        return $this;
    }

    /**
     * Get codeStatut
     *
     * @return string
     */
    public function getCodeStatut()
    {
        return $this->codeStatut;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return TAdmission
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set preinscription
     *
     * @param \AppBundle\Entity\TPreinscription $preinscription
     *
     * @return TAdmission
     */
    public function setPreinscription(\AppBundle\Entity\TPreinscription $preinscription = null)
    {
        $this->preinscription = $preinscription;
    
        return $this;
    }

    /**
     * Get preinscription
     *
     * @return \AppBundle\Entity\TPreinscription
     */
    public function getPreinscription()
    {
        return $this->preinscription;
    }

    /**
     * Set organisme
     *
     * @param \AppBundle\Entity\POrganisme $organisme
     *
     * @return TAdmission
     */
    public function setOrganisme(\AppBundle\Entity\POrganisme $organisme = null)
    {
        $this->organisme = $organisme;
    
        return $this;
    }

    /**
     * Get organisme
     *
     * @return \AppBundle\Entity\POrganisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set statut
     *
     * @param \AppBundle\Entity\PStatut $statut
     *
     * @return TAdmission
     */
    public function setStatut(\AppBundle\Entity\PStatut $statut = null)
    {
        $this->statut = $statut;
    
        return $this;
    }

    /**
     * Get statut
     *
     * @return \AppBundle\Entity\PStatut
     */
    public function getStatut()
    {
        return $this->statut;
    }
}
