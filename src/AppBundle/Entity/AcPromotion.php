<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * AcPromotion
 *
 * @ORM\Table(name="ac_promotion")
 * @ORM\Entity
 */
class AcPromotion {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="code_formation", type="string", length=100, nullable=true)
     */
    private $codeFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $ordre;

    /**
     * @var string
     *
     * @ORM\Column(name="validation_academique", type="string", length=3, nullable=true)
     */
    private $validationAcademique;

    /**
     * @var string
     *
     * @ORM\Column(name="cloture_academique", type="string", length=3, nullable=true)
     */
    private $clotureAcademique;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    
     /**
     * @ORM\ManyToOne(targetEntity="AcFormation", inversedBy="promotions")
     * @ORM\JoinColumn(name="formation_id", referencedColumnName="id")
     */
    private $formation;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="s_stages", mappedBy="promotion")
     */
    private $stages;
   
       /**
     * 
     * @ORM\OneToMany(targetEntity="SInsStg", mappedBy="promotion")
     */
    private $ins_group;
   

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ins_group = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return AcPromotion
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeFormation
     *
     * @param string $codeFormation
     *
     * @return AcPromotion
     */
    public function setCodeFormation($codeFormation)
    {
        $this->codeFormation = $codeFormation;
    
        return $this;
    }

    /**
     * Get codeFormation
     *
     * @return string
     */
    public function getCodeFormation()
    {
        return $this->codeFormation;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return AcPromotion
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     *
     * @return AcPromotion
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;
    
        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set validationAcademique
     *
     * @param string $validationAcademique
     *
     * @return AcPromotion
     */
    public function setValidationAcademique($validationAcademique)
    {
        $this->validationAcademique = $validationAcademique;
    
        return $this;
    }

    /**
     * Get validationAcademique
     *
     * @return string
     */
    public function getValidationAcademique()
    {
        return $this->validationAcademique;
    }

    /**
     * Set clotureAcademique
     *
     * @param string $clotureAcademique
     *
     * @return AcPromotion
     */
    public function setClotureAcademique($clotureAcademique)
    {
        $this->clotureAcademique = $clotureAcademique;
    
        return $this;
    }

    /**
     * Get clotureAcademique
     *
     * @return string
     */
    public function getClotureAcademique()
    {
        return $this->clotureAcademique;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return AcPromotion
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set formation
     *
     * @param \AppBundle\Entity\AcFormation $formation
     *
     * @return AcPromotion
     */
    public function setFormation(\AppBundle\Entity\AcFormation $formation = null)
    {
        $this->formation = $formation;
    
        return $this;
    }

    /**
     * Get formation
     *
     * @return \AppBundle\Entity\AcFormation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     *
     * @return AcPromotion
     */
    public function addStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stages[] = $stage;
    
        return $this;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     */
    public function removeStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stages->removeElement($stage);
    }

    /**
     * Get stages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStages()
    {
        return $this->stages;
    }

    /**
     * Add insGroup
     *
     * @param \AppBundle\Entity\SInsStg $insGroup
     *
     * @return AcPromotion
     */
    public function addInsGroup(\AppBundle\Entity\SInsStg $insGroup)
    {
        $this->ins_group[] = $insGroup;
    
        return $this;
    }

    /**
     * Remove insGroup
     *
     * @param \AppBundle\Entity\SInsStg $insGroup
     */
    public function removeInsGroup(\AppBundle\Entity\SInsStg $insGroup)
    {
        $this->ins_group->removeElement($insGroup);
    }

    /**
     * Get insGroup
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInsGroup()
    {
        return $this->ins_group;
    }
}
