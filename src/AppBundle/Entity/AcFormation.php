<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * AcFormation
 *
 * @ORM\Table(name="ac_formation")
 * @ORM\Entity
 */
class AcFormation {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="code_etablissement", type="string", length=100, nullable=true)
     */
    private $codeEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="code_departement", type="string", length=100, nullable=true)
     */
    private $codeDepartement;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=100, nullable=true)
     */
    private $abreviation;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_annee", type="integer", nullable=true)
     */
    private $nbrAnnee;

    /**
     * @var float
     *
     * @ORM\Column(name="seuil", type="float", precision=10, scale=0, nullable=true)
     */
    private $seuil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="AcEtablissement", inversedBy="formations")
     * @ORM\JoinColumn(name="ac_etablissement_id", referencedColumnName="id")
     */
    private $etablissement;

    /**
     * @ORM\OneToMany(targetEntity="AcPromotion", mappedBy="formation")
     */
    private $promotions;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="s_stages", mappedBy="formation")
     */
    private $stages;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->promotions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->stages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return AcFormation
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeEtablissement
     *
     * @param string $codeEtablissement
     *
     * @return AcFormation
     */
    public function setCodeEtablissement($codeEtablissement)
    {
        $this->codeEtablissement = $codeEtablissement;
    
        return $this;
    }

    /**
     * Get codeEtablissement
     *
     * @return string
     */
    public function getCodeEtablissement()
    {
        return $this->codeEtablissement;
    }

    /**
     * Set codeDepartement
     *
     * @param string $codeDepartement
     *
     * @return AcFormation
     */
    public function setCodeDepartement($codeDepartement)
    {
        $this->codeDepartement = $codeDepartement;
    
        return $this;
    }

    /**
     * Get codeDepartement
     *
     * @return string
     */
    public function getCodeDepartement()
    {
        return $this->codeDepartement;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return AcFormation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return AcFormation
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set nbrAnnee
     *
     * @param integer $nbrAnnee
     *
     * @return AcFormation
     */
    public function setNbrAnnee($nbrAnnee)
    {
        $this->nbrAnnee = $nbrAnnee;
    
        return $this;
    }

    /**
     * Get nbrAnnee
     *
     * @return integer
     */
    public function getNbrAnnee()
    {
        return $this->nbrAnnee;
    }

    /**
     * Set seuil
     *
     * @param float $seuil
     *
     * @return AcFormation
     */
    public function setSeuil($seuil)
    {
        $this->seuil = $seuil;
    
        return $this;
    }

    /**
     * Get seuil
     *
     * @return float
     */
    public function getSeuil()
    {
        return $this->seuil;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return AcFormation
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return AcFormation
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set etablissement
     *
     * @param \AppBundle\Entity\AcEtablissement $etablissement
     *
     * @return AcFormation
     */
    public function setEtablissement(\AppBundle\Entity\AcEtablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;
    
        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \AppBundle\Entity\AcEtablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Add promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     *
     * @return AcFormation
     */
    public function addPromotion(\AppBundle\Entity\AcPromotion $promotion)
    {
        $this->promotions[] = $promotion;
    
        return $this;
    }

    /**
     * Remove promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     */
    public function removePromotion(\AppBundle\Entity\AcPromotion $promotion)
    {
        $this->promotions->removeElement($promotion);
    }

    /**
     * Get promotions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     *
     * @return AcFormation
     */
    public function addStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stages[] = $stage;
    
        return $this;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     */
    public function removeStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stages->removeElement($stage);
    }

    /**
     * Get stages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStages()
    {
        return $this->stages;
    }
}
