<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * POrganisme
 *
 * @ORM\Table(name="p_organisme")
 * @ORM\Entity
 *
 */
class POrganisme
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=100, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=100, nullable=true)
     */
    private $abreviation;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active ;
    
    
    
    
    
   
    

    public function __construct() {

       
       
        
      
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return POrganisme
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return POrganisme
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return POrganisme
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;

        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return POrganisme
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add admissionsOrganisme
     *
     * @param \AppBundle\Entity\TAdmission $admissionsOrganisme
     *
     * @return POrganisme
     */
    public function addAdmissionsOrganisme(\AppBundle\Entity\TAdmission $admissionsOrganisme)
    {
        $this->admissionsOrganisme[] = $admissionsOrganisme;

        return $this;
    }

    /**
     * Remove admissionsOrganisme
     *
     * @param \AppBundle\Entity\TAdmission $admissionsOrganisme
     */
    public function removeAdmissionsOrganisme(\AppBundle\Entity\TAdmission $admissionsOrganisme)
    {
        $this->admissionsOrganisme->removeElement($admissionsOrganisme);
    }

    /**
     * Get admissionsOrganisme
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdmissionsOrganisme()
    {
        return $this->admissionsOrganisme;
    }
}
