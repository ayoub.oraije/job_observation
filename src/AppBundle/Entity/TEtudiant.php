<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * TEtudiant
 *
 * @ORM\Table(name="t_etudiant")
* @ORM\Entity(repositoryClass="AppBundle\Repository\TEtudiantRepository")
 
 */
class TEtudiant {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="inscription_valide", type="string", length=1, nullable=true)
     */
    private $inscriptionValide;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=150, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=150, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="url_image", type="string", length=555, nullable=true)
     * 
     * 
     * @Assert\NotBlank(message="Svp inserer une forme valide (png,jpg,jpeg)")
     * @Assert\File(mimeTypes={"image/jpeg","image/gif","image/png"})
     * 
     */
    private $urlImage;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_naissance", type="date", nullable=true)
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu_naissance", type="string", length=100, nullable=true)
     */
    private $lieuNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=10, nullable=true)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="st_famille", type="string", length=40, nullable=true)
     */
    private $stFamille;

    /**
     * @var string
     *
     * @ORM\Column(name="st_famille_parent", type="string", length=100, nullable=true)
     */
    private $stFamilleParent;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalite", type="string", length=100, nullable=true)
     */
    private $nationalite;

    /**
     * @var string
     *
     * @ORM\Column(name="cin", type="string", length=100, nullable=true)
     */
    private $cin;

    /**
     * @var string
     *
     * @ORM\Column(name="passeport", type="string", length=100, nullable=true)
     */
    private $passeport;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="text", length=16777215, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=100, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="tel1", type="string", length=20, nullable=true)
     */
    private $tel1;

    /**
     * @var string
     *
     * @ORM\Column(name="tel2", type="string", length=20, nullable=true)
     */
    private $tel2;

    /**
     * @var string
     *
     * @ORM\Column(name="tel3", type="string", length=20, nullable=true)
     */
    private $tel3;

    /**
     * @var string
     *
     * @ORM\Column(name="mail1", type="string", length=100, nullable=true)
     */
    private $mail1;

    /**
     * @var string
     *
     * @ORM\Column(name="mail2", type="string", length=100, nullable=true)
     */
    private $mail2;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_p", type="string", length=100, nullable=true)
     */
    private $nomP;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_p", type="string", length=100, nullable=true)
     */
    private $prenomP;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalite_p", type="string", length=100, nullable=true)
     */
    private $nationaliteP;

    /**
     * @var string
     *
     * @ORM\Column(name="profession_p", type="string", length=100, nullable=true)
     */
    private $professionP;

    /**
     * @var string
     *
     * @ORM\Column(name="employe_p", type="string", length=100, nullable=true)
     */
    private $employeP;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_p", type="string", length=100, nullable=true)
     */
    private $categorieP;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_p", type="string", length=20, nullable=true)
     */
    private $telP;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_p", type="string", length=100, nullable=true)
     */
    private $mailP;

    /**
     * @var string
     *
     * @ORM\Column(name="salaire_p", type="string", length=100, nullable=true)
     */
    private $salaireP;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_m", type="string", length=100, nullable=true)
     */
    private $nomM;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_m", type="string", length=100, nullable=true)
     */
    private $prenomM;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalite_m", type="string", length=100, nullable=true)
     */
    private $nationaliteM;

    /**
     * @var string
     *
     * @ORM\Column(name="profession_m", type="string", length=100, nullable=true)
     */
    private $professionM;

    /**
     * @var string
     *
     * @ORM\Column(name="employe_m", type="string", length=100, nullable=true)
     */
    private $employeM;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_m", type="string", length=100, nullable=true)
     */
    private $categorieM;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_m", type="string", length=20, nullable=true)
     */
    private $telM;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_m", type="string", length=100, nullable=true)
     */
    private $mailM;

    /**
     * @var string
     *
     * @ORM\Column(name="salaire_m", type="string", length=100, nullable=true)
     */
    private $salaireM;

    /**
     * @var string
     *
     * @ORM\Column(name="cne", type="string", length=40, nullable=true)
     */
    private $cne;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement", type="string", length=150, nullable=true)
     */
    private $etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="annee_bac", type="string", length=20, nullable=true)
     */
    private $anneeBac;

    /**
     * @var string
     *
     * @ORM\Column(name="moyenne_bac", type="string", length=100, nullable=true)
     */
    private $moyenneBac;

    /**
     * @var string
     *
     * @ORM\Column(name="concours_medbup", type="string", length=100, nullable=true)
     */
    private $concoursMedbup;

    /**
     * @var string
     *
     * @ORM\Column(name="obs", type="text", length=16777215, nullable=true)
     */
    private $obs;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_preinscription", type="string", length=100, nullable=true)
     */
    private $categoriePreinscription;

    /**
     * @var string
     *
     * @ORM\Column(name="frais_preinscription", type="string", length=100, nullable=true)
     */
    private $fraisPreinscription;

    /**
     * @var string
     *
     * @ORM\Column(name="bourse", type="string", length=100, nullable=true)
     */
    private $bourse;

    /**
     * @var string
     *
     * @ORM\Column(name="logement", type="string", length=50, nullable=true)
     */
    private $logement;

    /**
     * @var string
     *
     * @ORM\Column(name="parking", type="string", length=50, nullable=true)
     */
    private $parking;

    /**
     * @var integer
     *
     * @ORM\Column(name="statut", type="integer", nullable=true)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="actif", type="string", length=20, nullable=true)
     */
    private $actif;

    /**
     * @var string
     *
     * @ORM\Column(name="code_organisme", type="string", length=10, nullable=true)
     */
    private $codeOrganisme;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_enfants", type="string", length=11, nullable=true)
     */
    private $nombreEnfants;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_liste", type="string", length=100, nullable=true)
     */
    private $categorieListe;

    /**
     * @var string
     *
     * @ORM\Column(name="admission_liste", type="string", length=100, nullable=true)
     */
    private $admissionListe;

    /**
     * @var string
     *
     * @ORM\Column(name="tele_liste", type="string", length=100, nullable=true)
     */
    private $teleListe;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_deliberation", type="string", length=255, nullable=true)
     */
    private $statutDeliberation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="utilisateur", type="string", length=100, nullable=true)
     */
    private $utilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="id_nature_demande", type="string", length=200, nullable=true)
     */
    private $idNatureDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="id_type_bac", type="string", length=100, nullable=true)
     */
    private $idTypeBac;

    /**
     * @var string
     *
     * @ORM\Column(name="id_academie", type="string", length=100, nullable=true)
     */
    private $idAcademie;

    /**
     * @var string
     *
     * @ORM\Column(name="langue_concours", type="string", length=100, nullable=true)
     */
    private $langueC;

    /**
     * @var string
     *
     * @ORM\Column(name="id_filiere", type="string", length=100, nullable=true)
     */
    private $idFiliere;

    /**
     * @ORM\ManyToOne(targetEntity="NatureDemande")
     * @ORM\JoinColumn(name="nature_demande_id", referencedColumnName="id")
     */
    private $natureDemande;

    /**
     * @ORM\ManyToOne(targetEntity="XTypeBac")
     * @ORM\JoinColumn(name="x_type_bac_id", referencedColumnName="id")
     */
    private $typeBac;

    /**
     * @ORM\ManyToOne(targetEntity="XAcademie")
     * @ORM\JoinColumn(name="x_academie_id", referencedColumnName="id")
     */
    private $academie;

    /**
     * @ORM\ManyToOne(targetEntity="XLangues")
     * @ORM\JoinColumn(name="x_langues_id", referencedColumnName="id")
     */
    private $langueConcours;

    /**
     * @ORM\ManyToOne(targetEntity="XFiliere")
     * @ORM\JoinColumn(name="x_filiere_id", referencedColumnName="id")
     */
    private $filiere;


    
    
   
    
    
    
    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="User", mappedBy="etudiant")
     */
    private $user;
    

    
    
    
    
    

    public function __construct() {
        
    }

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TEtudiant
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set inscriptionValide
     *
     * @param string $inscriptionValide
     *
     * @return TEtudiant
     */
    public function setInscriptionValide($inscriptionValide)
    {
        $this->inscriptionValide = $inscriptionValide;
    
        return $this;
    }

    /**
     * Get inscriptionValide
     *
     * @return string
     */
    public function getInscriptionValide()
    {
        return $this->inscriptionValide;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return TEtudiant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return TEtudiant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set urlImage
     *
     * @param string $urlImage
     *
     * @return TEtudiant
     */
    public function setUrlImage($urlImage)
    {
        $this->urlImage = $urlImage;
    
        return $this;
    }

    /**
     * Get urlImage
     *
     * @return string
     */
    public function getUrlImage()
    {
        return $this->urlImage;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return TEtudiant
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     *
     * @return TEtudiant
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;
    
        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set lieuNaissance
     *
     * @param string $lieuNaissance
     *
     * @return TEtudiant
     */
    public function setLieuNaissance($lieuNaissance)
    {
        $this->lieuNaissance = $lieuNaissance;
    
        return $this;
    }

    /**
     * Get lieuNaissance
     *
     * @return string
     */
    public function getLieuNaissance()
    {
        return $this->lieuNaissance;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return TEtudiant
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    
        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set stFamille
     *
     * @param string $stFamille
     *
     * @return TEtudiant
     */
    public function setStFamille($stFamille)
    {
        $this->stFamille = $stFamille;
    
        return $this;
    }

    /**
     * Get stFamille
     *
     * @return string
     */
    public function getStFamille()
    {
        return $this->stFamille;
    }

    /**
     * Set stFamilleParent
     *
     * @param string $stFamilleParent
     *
     * @return TEtudiant
     */
    public function setStFamilleParent($stFamilleParent)
    {
        $this->stFamilleParent = $stFamilleParent;
    
        return $this;
    }

    /**
     * Get stFamilleParent
     *
     * @return string
     */
    public function getStFamilleParent()
    {
        return $this->stFamilleParent;
    }

    /**
     * Set nationalite
     *
     * @param string $nationalite
     *
     * @return TEtudiant
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;
    
        return $this;
    }

    /**
     * Get nationalite
     *
     * @return string
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set cin
     *
     * @param string $cin
     *
     * @return TEtudiant
     */
    public function setCin($cin)
    {
        $this->cin = $cin;
    
        return $this;
    }

    /**
     * Get cin
     *
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * Set passeport
     *
     * @param string $passeport
     *
     * @return TEtudiant
     */
    public function setPasseport($passeport)
    {
        $this->passeport = $passeport;
    
        return $this;
    }

    /**
     * Get passeport
     *
     * @return string
     */
    public function getPasseport()
    {
        return $this->passeport;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return TEtudiant
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return TEtudiant
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set tel1
     *
     * @param string $tel1
     *
     * @return TEtudiant
     */
    public function setTel1($tel1)
    {
        $this->tel1 = $tel1;
    
        return $this;
    }

    /**
     * Get tel1
     *
     * @return string
     */
    public function getTel1()
    {
        return $this->tel1;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     *
     * @return TEtudiant
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;
    
        return $this;
    }

    /**
     * Get tel2
     *
     * @return string
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set tel3
     *
     * @param string $tel3
     *
     * @return TEtudiant
     */
    public function setTel3($tel3)
    {
        $this->tel3 = $tel3;
    
        return $this;
    }

    /**
     * Get tel3
     *
     * @return string
     */
    public function getTel3()
    {
        return $this->tel3;
    }

    /**
     * Set mail1
     *
     * @param string $mail1
     *
     * @return TEtudiant
     */
    public function setMail1($mail1)
    {
        $this->mail1 = $mail1;
    
        return $this;
    }

    /**
     * Get mail1
     *
     * @return string
     */
    public function getMail1()
    {
        return $this->mail1;
    }

    /**
     * Set mail2
     *
     * @param string $mail2
     *
     * @return TEtudiant
     */
    public function setMail2($mail2)
    {
        $this->mail2 = $mail2;
    
        return $this;
    }

    /**
     * Get mail2
     *
     * @return string
     */
    public function getMail2()
    {
        return $this->mail2;
    }

    /**
     * Set nomP
     *
     * @param string $nomP
     *
     * @return TEtudiant
     */
    public function setNomP($nomP)
    {
        $this->nomP = $nomP;
    
        return $this;
    }

    /**
     * Get nomP
     *
     * @return string
     */
    public function getNomP()
    {
        return $this->nomP;
    }

    /**
     * Set prenomP
     *
     * @param string $prenomP
     *
     * @return TEtudiant
     */
    public function setPrenomP($prenomP)
    {
        $this->prenomP = $prenomP;
    
        return $this;
    }

    /**
     * Get prenomP
     *
     * @return string
     */
    public function getPrenomP()
    {
        return $this->prenomP;
    }

    /**
     * Set nationaliteP
     *
     * @param string $nationaliteP
     *
     * @return TEtudiant
     */
    public function setNationaliteP($nationaliteP)
    {
        $this->nationaliteP = $nationaliteP;
    
        return $this;
    }

    /**
     * Get nationaliteP
     *
     * @return string
     */
    public function getNationaliteP()
    {
        return $this->nationaliteP;
    }

    /**
     * Set professionP
     *
     * @param string $professionP
     *
     * @return TEtudiant
     */
    public function setProfessionP($professionP)
    {
        $this->professionP = $professionP;
    
        return $this;
    }

    /**
     * Get professionP
     *
     * @return string
     */
    public function getProfessionP()
    {
        return $this->professionP;
    }

    /**
     * Set employeP
     *
     * @param string $employeP
     *
     * @return TEtudiant
     */
    public function setEmployeP($employeP)
    {
        $this->employeP = $employeP;
    
        return $this;
    }

    /**
     * Get employeP
     *
     * @return string
     */
    public function getEmployeP()
    {
        return $this->employeP;
    }

    /**
     * Set categorieP
     *
     * @param string $categorieP
     *
     * @return TEtudiant
     */
    public function setCategorieP($categorieP)
    {
        $this->categorieP = $categorieP;
    
        return $this;
    }

    /**
     * Get categorieP
     *
     * @return string
     */
    public function getCategorieP()
    {
        return $this->categorieP;
    }

    /**
     * Set telP
     *
     * @param string $telP
     *
     * @return TEtudiant
     */
    public function setTelP($telP)
    {
        $this->telP = $telP;
    
        return $this;
    }

    /**
     * Get telP
     *
     * @return string
     */
    public function getTelP()
    {
        return $this->telP;
    }

    /**
     * Set mailP
     *
     * @param string $mailP
     *
     * @return TEtudiant
     */
    public function setMailP($mailP)
    {
        $this->mailP = $mailP;
    
        return $this;
    }

    /**
     * Get mailP
     *
     * @return string
     */
    public function getMailP()
    {
        return $this->mailP;
    }

    /**
     * Set salaireP
     *
     * @param string $salaireP
     *
     * @return TEtudiant
     */
    public function setSalaireP($salaireP)
    {
        $this->salaireP = $salaireP;
    
        return $this;
    }

    /**
     * Get salaireP
     *
     * @return string
     */
    public function getSalaireP()
    {
        return $this->salaireP;
    }

    /**
     * Set nomM
     *
     * @param string $nomM
     *
     * @return TEtudiant
     */
    public function setNomM($nomM)
    {
        $this->nomM = $nomM;
    
        return $this;
    }

    /**
     * Get nomM
     *
     * @return string
     */
    public function getNomM()
    {
        return $this->nomM;
    }

    /**
     * Set prenomM
     *
     * @param string $prenomM
     *
     * @return TEtudiant
     */
    public function setPrenomM($prenomM)
    {
        $this->prenomM = $prenomM;
    
        return $this;
    }

    /**
     * Get prenomM
     *
     * @return string
     */
    public function getPrenomM()
    {
        return $this->prenomM;
    }

    /**
     * Set nationaliteM
     *
     * @param string $nationaliteM
     *
     * @return TEtudiant
     */
    public function setNationaliteM($nationaliteM)
    {
        $this->nationaliteM = $nationaliteM;
    
        return $this;
    }

    /**
     * Get nationaliteM
     *
     * @return string
     */
    public function getNationaliteM()
    {
        return $this->nationaliteM;
    }

    /**
     * Set professionM
     *
     * @param string $professionM
     *
     * @return TEtudiant
     */
    public function setProfessionM($professionM)
    {
        $this->professionM = $professionM;
    
        return $this;
    }

    /**
     * Get professionM
     *
     * @return string
     */
    public function getProfessionM()
    {
        return $this->professionM;
    }

    /**
     * Set employeM
     *
     * @param string $employeM
     *
     * @return TEtudiant
     */
    public function setEmployeM($employeM)
    {
        $this->employeM = $employeM;
    
        return $this;
    }

    /**
     * Get employeM
     *
     * @return string
     */
    public function getEmployeM()
    {
        return $this->employeM;
    }

    /**
     * Set categorieM
     *
     * @param string $categorieM
     *
     * @return TEtudiant
     */
    public function setCategorieM($categorieM)
    {
        $this->categorieM = $categorieM;
    
        return $this;
    }

    /**
     * Get categorieM
     *
     * @return string
     */
    public function getCategorieM()
    {
        return $this->categorieM;
    }

    /**
     * Set telM
     *
     * @param string $telM
     *
     * @return TEtudiant
     */
    public function setTelM($telM)
    {
        $this->telM = $telM;
    
        return $this;
    }

    /**
     * Get telM
     *
     * @return string
     */
    public function getTelM()
    {
        return $this->telM;
    }

    /**
     * Set mailM
     *
     * @param string $mailM
     *
     * @return TEtudiant
     */
    public function setMailM($mailM)
    {
        $this->mailM = $mailM;
    
        return $this;
    }

    /**
     * Get mailM
     *
     * @return string
     */
    public function getMailM()
    {
        return $this->mailM;
    }

    /**
     * Set salaireM
     *
     * @param string $salaireM
     *
     * @return TEtudiant
     */
    public function setSalaireM($salaireM)
    {
        $this->salaireM = $salaireM;
    
        return $this;
    }

    /**
     * Get salaireM
     *
     * @return string
     */
    public function getSalaireM()
    {
        return $this->salaireM;
    }

    /**
     * Set cne
     *
     * @param string $cne
     *
     * @return TEtudiant
     */
    public function setCne($cne)
    {
        $this->cne = $cne;
    
        return $this;
    }

    /**
     * Get cne
     *
     * @return string
     */
    public function getCne()
    {
        return $this->cne;
    }

    /**
     * Set etablissement
     *
     * @param string $etablissement
     *
     * @return TEtudiant
     */
    public function setEtablissement($etablissement)
    {
        $this->etablissement = $etablissement;
    
        return $this;
    }

    /**
     * Get etablissement
     *
     * @return string
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Set anneeBac
     *
     * @param string $anneeBac
     *
     * @return TEtudiant
     */
    public function setAnneeBac($anneeBac)
    {
        $this->anneeBac = $anneeBac;
    
        return $this;
    }

    /**
     * Get anneeBac
     *
     * @return string
     */
    public function getAnneeBac()
    {
        return $this->anneeBac;
    }

    /**
     * Set moyenneBac
     *
     * @param string $moyenneBac
     *
     * @return TEtudiant
     */
    public function setMoyenneBac($moyenneBac)
    {
        $this->moyenneBac = $moyenneBac;
    
        return $this;
    }

    /**
     * Get moyenneBac
     *
     * @return string
     */
    public function getMoyenneBac()
    {
        return $this->moyenneBac;
    }

    /**
     * Set concoursMedbup
     *
     * @param string $concoursMedbup
     *
     * @return TEtudiant
     */
    public function setConcoursMedbup($concoursMedbup)
    {
        $this->concoursMedbup = $concoursMedbup;
    
        return $this;
    }

    /**
     * Get concoursMedbup
     *
     * @return string
     */
    public function getConcoursMedbup()
    {
        return $this->concoursMedbup;
    }

    /**
     * Set obs
     *
     * @param string $obs
     *
     * @return TEtudiant
     */
    public function setObs($obs)
    {
        $this->obs = $obs;
    
        return $this;
    }

    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * Set categoriePreinscription
     *
     * @param string $categoriePreinscription
     *
     * @return TEtudiant
     */
    public function setCategoriePreinscription($categoriePreinscription)
    {
        $this->categoriePreinscription = $categoriePreinscription;
    
        return $this;
    }

    /**
     * Get categoriePreinscription
     *
     * @return string
     */
    public function getCategoriePreinscription()
    {
        return $this->categoriePreinscription;
    }

    /**
     * Set fraisPreinscription
     *
     * @param string $fraisPreinscription
     *
     * @return TEtudiant
     */
    public function setFraisPreinscription($fraisPreinscription)
    {
        $this->fraisPreinscription = $fraisPreinscription;
    
        return $this;
    }

    /**
     * Get fraisPreinscription
     *
     * @return string
     */
    public function getFraisPreinscription()
    {
        return $this->fraisPreinscription;
    }

    /**
     * Set bourse
     *
     * @param string $bourse
     *
     * @return TEtudiant
     */
    public function setBourse($bourse)
    {
        $this->bourse = $bourse;
    
        return $this;
    }

    /**
     * Get bourse
     *
     * @return string
     */
    public function getBourse()
    {
        return $this->bourse;
    }

    /**
     * Set logement
     *
     * @param string $logement
     *
     * @return TEtudiant
     */
    public function setLogement($logement)
    {
        $this->logement = $logement;
    
        return $this;
    }

    /**
     * Get logement
     *
     * @return string
     */
    public function getLogement()
    {
        return $this->logement;
    }

    /**
     * Set parking
     *
     * @param string $parking
     *
     * @return TEtudiant
     */
    public function setParking($parking)
    {
        $this->parking = $parking;
    
        return $this;
    }

    /**
     * Get parking
     *
     * @return string
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set statut
     *
     * @param integer $statut
     *
     * @return TEtudiant
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    
        return $this;
    }

    /**
     * Get statut
     *
     * @return integer
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set actif
     *
     * @param string $actif
     *
     * @return TEtudiant
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    
        return $this;
    }

    /**
     * Get actif
     *
     * @return string
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set codeOrganisme
     *
     * @param string $codeOrganisme
     *
     * @return TEtudiant
     */
    public function setCodeOrganisme($codeOrganisme)
    {
        $this->codeOrganisme = $codeOrganisme;
    
        return $this;
    }

    /**
     * Get codeOrganisme
     *
     * @return string
     */
    public function getCodeOrganisme()
    {
        return $this->codeOrganisme;
    }

    /**
     * Set nombreEnfants
     *
     * @param string $nombreEnfants
     *
     * @return TEtudiant
     */
    public function setNombreEnfants($nombreEnfants)
    {
        $this->nombreEnfants = $nombreEnfants;
    
        return $this;
    }

    /**
     * Get nombreEnfants
     *
     * @return string
     */
    public function getNombreEnfants()
    {
        return $this->nombreEnfants;
    }

    /**
     * Set categorieListe
     *
     * @param string $categorieListe
     *
     * @return TEtudiant
     */
    public function setCategorieListe($categorieListe)
    {
        $this->categorieListe = $categorieListe;
    
        return $this;
    }

    /**
     * Get categorieListe
     *
     * @return string
     */
    public function getCategorieListe()
    {
        return $this->categorieListe;
    }

    /**
     * Set admissionListe
     *
     * @param string $admissionListe
     *
     * @return TEtudiant
     */
    public function setAdmissionListe($admissionListe)
    {
        $this->admissionListe = $admissionListe;
    
        return $this;
    }

    /**
     * Get admissionListe
     *
     * @return string
     */
    public function getAdmissionListe()
    {
        return $this->admissionListe;
    }

    /**
     * Set teleListe
     *
     * @param string $teleListe
     *
     * @return TEtudiant
     */
    public function setTeleListe($teleListe)
    {
        $this->teleListe = $teleListe;
    
        return $this;
    }

    /**
     * Get teleListe
     *
     * @return string
     */
    public function getTeleListe()
    {
        return $this->teleListe;
    }

    /**
     * Set statutDeliberation
     *
     * @param string $statutDeliberation
     *
     * @return TEtudiant
     */
    public function setStatutDeliberation($statutDeliberation)
    {
        $this->statutDeliberation = $statutDeliberation;
    
        return $this;
    }

    /**
     * Get statutDeliberation
     *
     * @return string
     */
    public function getStatutDeliberation()
    {
        return $this->statutDeliberation;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return TEtudiant
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set utilisateur
     *
     * @param string $utilisateur
     *
     * @return TEtudiant
     */
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;
    
        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return string
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set idNatureDemande
     *
     * @param string $idNatureDemande
     *
     * @return TEtudiant
     */
    public function setIdNatureDemande($idNatureDemande)
    {
        $this->idNatureDemande = $idNatureDemande;
    
        return $this;
    }

    /**
     * Get idNatureDemande
     *
     * @return string
     */
    public function getIdNatureDemande()
    {
        return $this->idNatureDemande;
    }

    /**
     * Set idTypeBac
     *
     * @param string $idTypeBac
     *
     * @return TEtudiant
     */
    public function setIdTypeBac($idTypeBac)
    {
        $this->idTypeBac = $idTypeBac;
    
        return $this;
    }

    /**
     * Get idTypeBac
     *
     * @return string
     */
    public function getIdTypeBac()
    {
        return $this->idTypeBac;
    }

    /**
     * Set idAcademie
     *
     * @param string $idAcademie
     *
     * @return TEtudiant
     */
    public function setIdAcademie($idAcademie)
    {
        $this->idAcademie = $idAcademie;
    
        return $this;
    }

    /**
     * Get idAcademie
     *
     * @return string
     */
    public function getIdAcademie()
    {
        return $this->idAcademie;
    }

    /**
     * Set langueC
     *
     * @param string $langueC
     *
     * @return TEtudiant
     */
    public function setLangueC($langueC)
    {
        $this->langueC = $langueC;
    
        return $this;
    }

    /**
     * Get langueC
     *
     * @return string
     */
    public function getLangueC()
    {
        return $this->langueC;
    }

    /**
     * Set idFiliere
     *
     * @param string $idFiliere
     *
     * @return TEtudiant
     */
    public function setIdFiliere($idFiliere)
    {
        $this->idFiliere = $idFiliere;
    
        return $this;
    }

    /**
     * Get idFiliere
     *
     * @return string
     */
    public function getIdFiliere()
    {
        return $this->idFiliere;
    }

    /**
     * Set natureDemande
     *
     * @param \AppBundle\Entity\NatureDemande $natureDemande
     *
     * @return TEtudiant
     */
    public function setNatureDemande(\AppBundle\Entity\NatureDemande $natureDemande = null)
    {
        $this->natureDemande = $natureDemande;
    
        return $this;
    }

    /**
     * Get natureDemande
     *
     * @return \AppBundle\Entity\NatureDemande
     */
    public function getNatureDemande()
    {
        return $this->natureDemande;
    }

    /**
     * Set typeBac
     *
     * @param \AppBundle\Entity\XTypeBac $typeBac
     *
     * @return TEtudiant
     */
    public function setTypeBac(\AppBundle\Entity\XTypeBac $typeBac = null)
    {
        $this->typeBac = $typeBac;
    
        return $this;
    }

    /**
     * Get typeBac
     *
     * @return \AppBundle\Entity\XTypeBac
     */
    public function getTypeBac()
    {
        return $this->typeBac;
    }

    /**
     * Set academie
     *
     * @param \AppBundle\Entity\XAcademie $academie
     *
     * @return TEtudiant
     */
    public function setAcademie(\AppBundle\Entity\XAcademie $academie = null)
    {
        $this->academie = $academie;
    
        return $this;
    }

    /**
     * Get academie
     *
     * @return \AppBundle\Entity\XAcademie
     */
    public function getAcademie()
    {
        return $this->academie;
    }

    /**
     * Set langueConcours
     *
     * @param \AppBundle\Entity\XLangues $langueConcours
     *
     * @return TEtudiant
     */
    public function setLangueConcours(\AppBundle\Entity\XLangues $langueConcours = null)
    {
        $this->langueConcours = $langueConcours;
    
        return $this;
    }

    /**
     * Get langueConcours
     *
     * @return \AppBundle\Entity\XLangues
     */
    public function getLangueConcours()
    {
        return $this->langueConcours;
    }

    /**
     * Set filiere
     *
     * @param \AppBundle\Entity\XFiliere $filiere
     *
     * @return TEtudiant
     */
    public function setFiliere(\AppBundle\Entity\XFiliere $filiere = null)
    {
        $this->filiere = $filiere;
    
        return $this;
    }

    /**
     * Get filiere
     *
     * @return \AppBundle\Entity\XFiliere
     */
    public function getFiliere()
    {
        return $this->filiere;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return TEtudiant
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
