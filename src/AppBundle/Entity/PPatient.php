<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PPatient
 *
 * @ORM\Table(name="p_patient")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PPatientRepository")
 */
class PPatient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="ipp", type="string", length=50, nullable=true)
     */
    private $ipp;

    /**
     * @ORM\ManyToOne(targetEntity="PCategorie", inversedBy="patients")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $categorie;

    /**
    * @var int
    *
    * @ORM\ManyToOne(targetEntity="TInscription")
    * @ORM\JoinColumn(name="t_inscription_id", referencedColumnName="id")
    */
    private $inscription;

    /**
     * @var string
     *
     * @ORM\Column(name="imprimable", type="string", length=3, nullable=true)
     */
    private $imprimable;

    /**
     * @var int
     *
     * @ORM\Column(name="is_patient", type="integer", nullable=true)
     */
    private $isPatient;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="TProcessus", mappedBy="patient")
     */
    private $processus;

    /**
     * @ORM\OneToMany(targetEntity="TObjectifEtudiant", mappedBy="patients")
     */
    private $obj_etudiant;

    /**
     * @ORM\ManyToOne(targetEntity="PEncadrant", inversedBy="patient_encadrant1")
     * @ORM\JoinColumn(name="enc_univ1_Id", referencedColumnName="id")
     */
    private $encNiv1;

    /**
     * @ORM\ManyToOne(targetEntity="PEncadrant", inversedBy="patient_encadrant2")
     * @ORM\JoinColumn(name="enc_univ2_Id", referencedColumnName="id")
     */
    private $encNiv2;

    /**
     * @ORM\ManyToOne(targetEntity="PEncadrant", inversedBy="patient_encadrant3")
     * @ORM\JoinColumn(name="enc_hop1_Id", referencedColumnName="id")
     */
    private $encHop1;

    /**
     * @ORM\ManyToOne(targetEntity="PEncadrant", inversedBy="patient_encadrant4")
     * @ORM\JoinColumn(name="enc_hop2_Id", referencedColumnName="id")
     */
    private $encHop2;

    /**
     * @ORM\ManyToOne(targetEntity="PEncadrant", inversedBy="patient_encadrant5")
     * @ORM\JoinColumn(name="enc_res1_Id", referencedColumnName="id")
     */
    private $encRes1;

    /**
     * @ORM\ManyToOne(targetEntity="PEncadrant", inversedBy="patient_encadrant6")
     * @ORM\JoinColumn(name="enc_res2_Id", referencedColumnName="id")
     */
    private $encRes2;

    /**
     * @var string
     *
     * @ORM\Column(name="enc_autre", type="string", length=255, nullable=true)
     */
    private $autre;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_created", referencedColumnName="id")
     */
    private $userCreated;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_updated", referencedColumnName="id")
     */
    private $userUpdated;
    
      /**
     * @ORM\ManyToOne(targetEntity="SGroupStage")
     * @ORM\JoinColumn(name="s_group_stage_id", referencedColumnName="id")
     */
    private $groupe;
    
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->processus = new \Doctrine\Common\Collections\ArrayCollection();
        $this->obj_etudiant = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return PPatient
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return PPatient
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    
        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return PPatient
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return PPatient
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set ipp
     *
     * @param string $ipp
     *
     * @return PPatient
     */
    public function setIpp($ipp)
    {
        $this->ipp = $ipp;
    
        return $this;
    }

    /**
     * Get ipp
     *
     * @return string
     */
    public function getIpp()
    {
        return $this->ipp;
    }

    /**
     * Set imprimable
     *
     * @param string $imprimable
     *
     * @return PPatient
     */
    public function setImprimable($imprimable)
    {
        $this->imprimable = $imprimable;
    
        return $this;
    }

    /**
     * Get imprimable
     *
     * @return string
     */
    public function getImprimable()
    {
        return $this->imprimable;
    }

    /**
     * Set isPatient
     *
     * @param integer $isPatient
     *
     * @return PPatient
     */
    public function setIsPatient($isPatient)
    {
        $this->isPatient = $isPatient;
    
        return $this;
    }

    /**
     * Get isPatient
     *
     * @return integer
     */
    public function getIsPatient()
    {
        return $this->isPatient;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return PPatient
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set autre
     *
     * @param string $autre
     *
     * @return PPatient
     */
    public function setAutre($autre)
    {
        $this->autre = $autre;
    
        return $this;
    }

    /**
     * Get autre
     *
     * @return string
     */
    public function getAutre()
    {
        return $this->autre;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return PPatient
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return PPatient
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\PCategorie $categorie
     *
     * @return PPatient
     */
    public function setCategorie(\AppBundle\Entity\PCategorie $categorie = null)
    {
        $this->categorie = $categorie;
    
        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\PCategorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set inscription
     *
     * @param \AppBundle\Entity\TInscription $inscription
     *
     * @return PPatient
     */
    public function setInscription(\AppBundle\Entity\TInscription $inscription = null)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return \AppBundle\Entity\TInscription
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Add processus
     *
     * @param \AppBundle\Entity\TProcessus $processus
     *
     * @return PPatient
     */
    public function addProcessus(\AppBundle\Entity\TProcessus $processus)
    {
        $this->processus[] = $processus;
    
        return $this;
    }

    /**
     * Remove processus
     *
     * @param \AppBundle\Entity\TProcessus $processus
     */
    public function removeProcessus(\AppBundle\Entity\TProcessus $processus)
    {
        $this->processus->removeElement($processus);
    }

    /**
     * Get processus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProcessus()
    {
        return $this->processus;
    }

    /**
     * Add objEtudiant
     *
     * @param \AppBundle\Entity\TObjectifEtudiant $objEtudiant
     *
     * @return PPatient
     */
    public function addObjEtudiant(\AppBundle\Entity\TObjectifEtudiant $objEtudiant)
    {
        $this->obj_etudiant[] = $objEtudiant;
    
        return $this;
    }

    /**
     * Remove objEtudiant
     *
     * @param \AppBundle\Entity\TObjectifEtudiant $objEtudiant
     */
    public function removeObjEtudiant(\AppBundle\Entity\TObjectifEtudiant $objEtudiant)
    {
        $this->obj_etudiant->removeElement($objEtudiant);
    }

    /**
     * Get objEtudiant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjEtudiant()
    {
        return $this->obj_etudiant;
    }

    /**
     * Set encNiv1
     *
     * @param \AppBundle\Entity\PEncadrant $encNiv1
     *
     * @return PPatient
     */
    public function setEncNiv1(\AppBundle\Entity\PEncadrant $encNiv1 = null)
    {
        $this->encNiv1 = $encNiv1;
    
        return $this;
    }

    /**
     * Get encNiv1
     *
     * @return \AppBundle\Entity\PEncadrant
     */
    public function getEncNiv1()
    {
        return $this->encNiv1;
    }

    /**
     * Set encNiv2
     *
     * @param \AppBundle\Entity\PEncadrant $encNiv2
     *
     * @return PPatient
     */
    public function setEncNiv2(\AppBundle\Entity\PEncadrant $encNiv2 = null)
    {
        $this->encNiv2 = $encNiv2;
    
        return $this;
    }

    /**
     * Get encNiv2
     *
     * @return \AppBundle\Entity\PEncadrant
     */
    public function getEncNiv2()
    {
        return $this->encNiv2;
    }

    /**
     * Set encHop1
     *
     * @param \AppBundle\Entity\PEncadrant $encHop1
     *
     * @return PPatient
     */
    public function setEncHop1(\AppBundle\Entity\PEncadrant $encHop1 = null)
    {
        $this->encHop1 = $encHop1;
    
        return $this;
    }

    /**
     * Get encHop1
     *
     * @return \AppBundle\Entity\PEncadrant
     */
    public function getEncHop1()
    {
        return $this->encHop1;
    }

    /**
     * Set encHop2
     *
     * @param \AppBundle\Entity\PEncadrant $encHop2
     *
     * @return PPatient
     */
    public function setEncHop2(\AppBundle\Entity\PEncadrant $encHop2 = null)
    {
        $this->encHop2 = $encHop2;
    
        return $this;
    }

    /**
     * Get encHop2
     *
     * @return \AppBundle\Entity\PEncadrant
     */
    public function getEncHop2()
    {
        return $this->encHop2;
    }

    /**
     * Set encRes1
     *
     * @param \AppBundle\Entity\PEncadrant $encRes1
     *
     * @return PPatient
     */
    public function setEncRes1(\AppBundle\Entity\PEncadrant $encRes1 = null)
    {
        $this->encRes1 = $encRes1;
    
        return $this;
    }

    /**
     * Get encRes1
     *
     * @return \AppBundle\Entity\PEncadrant
     */
    public function getEncRes1()
    {
        return $this->encRes1;
    }

    /**
     * Set encRes2
     *
     * @param \AppBundle\Entity\PEncadrant $encRes2
     *
     * @return PPatient
     */
    public function setEncRes2(\AppBundle\Entity\PEncadrant $encRes2 = null)
    {
        $this->encRes2 = $encRes2;
    
        return $this;
    }

    /**
     * Get encRes2
     *
     * @return \AppBundle\Entity\PEncadrant
     */
    public function getEncRes2()
    {
        return $this->encRes2;
    }

    /**
     * Set userCreated
     *
     * @param \AppBundle\Entity\User $userCreated
     *
     * @return PPatient
     */
    public function setUserCreated(\AppBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userUpdated
     *
     * @param \AppBundle\Entity\User $userUpdated
     *
     * @return PPatient
     */
    public function setUserUpdated(\AppBundle\Entity\User $userUpdated = null)
    {
        $this->userUpdated = $userUpdated;
    
        return $this;
    }

    /**
     * Get userUpdated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }

    /**
     * Set groupe
     *
     * @param \AppBundle\Entity\SGroupStage $groupe
     *
     * @return PPatient
     */
    public function setGroupe(\AppBundle\Entity\SGroupStage $groupe = null)
    {
        $this->groupe = $groupe;
    
        return $this;
    }

    /**
     * Get groupe
     *
     * @return \AppBundle\Entity\SGroupStage
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
}
