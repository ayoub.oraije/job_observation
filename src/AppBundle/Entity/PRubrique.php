<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * PRubrique
 *
 * @ORM\Table(name="p_rubrique")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PRubriqueRepository")
 */
class PRubrique {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="abreviation", type="string", length=255, nullable=true)
     */
    private $abreviation;

    /**
     * @ORM\OneToMany(targetEntity="TProcessus", mappedBy="rubrique")
     */
    private $processus;

    /**
     * @ORM\ManyToMany(targetEntity="PCategorie" ,mappedBy="rubrique" )
     
     */
    private $categorie;

  
 
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->processus = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorie = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return PRubrique
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return PRubrique
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Add processus
     *
     * @param \AppBundle\Entity\TProcessus $processus
     *
     * @return PRubrique
     */
    public function addProcessus(\AppBundle\Entity\TProcessus $processus)
    {
        $this->processus[] = $processus;
    
        return $this;
    }

    /**
     * Remove processus
     *
     * @param \AppBundle\Entity\TProcessus $processus
     */
    public function removeProcessus(\AppBundle\Entity\TProcessus $processus)
    {
        $this->processus->removeElement($processus);
    }

    /**
     * Get processus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProcessus()
    {
        return $this->processus;
    }

    /**
     * Add categorie
     *
     * @param \AppBundle\Entity\PCategorie $categorie
     *
     * @return PRubrique
     */
    public function addCategorie(\AppBundle\Entity\PCategorie $categorie)
    {
        $this->categorie[] = $categorie;
    
        return $this;
    }

    /**
     * Remove categorie
     *
     * @param \AppBundle\Entity\PCategorie $categorie
     */
    public function removeCategorie(\AppBundle\Entity\PCategorie $categorie)
    {
        $this->categorie->removeElement($categorie);
    }

    /**
     * Get categorie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
