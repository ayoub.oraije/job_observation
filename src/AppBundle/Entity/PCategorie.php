<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PCategorie
 *
 * @ORM\Table(name="p_categorie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PCategorieRepository")
 */
class PCategorie {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="PModule", inversedBy="categories")
     * @ORM\JoinColumn(name="p_module_id",referencedColumnName="id")
     */
    private $module;

    /**
     * @var int
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="is_patient", type="integer", nullable=true)
     */
    private $isPatient;

    /**
     * 
     * @ORM\OneToMany(targetEntity="PPatient", mappedBy="categorie")
     */
    private $patients;

    /**
     * 
     * *@ORM\ManyToMany(targetEntity="PRubrique", inversedBy="categorie")
     * @ORM\JoinTable(name="rubrique_categorie")
     */
    private $rubrique;

  

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->patients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rubrique = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return PCategorie
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set isPatient
     *
     * @param integer $isPatient
     *
     * @return PCategorie
     */
    public function setIsPatient($isPatient)
    {
        $this->isPatient = $isPatient;
    
        return $this;
    }

    /**
     * Get isPatient
     *
     * @return integer
     */
    public function getIsPatient()
    {
        return $this->isPatient;
    }

    /**
     * Set module
     *
     * @param \AppBundle\Entity\PModule $module
     *
     * @return PCategorie
     */
    public function setModule(\AppBundle\Entity\PModule $module = null)
    {
        $this->module = $module;
    
        return $this;
    }

    /**
     * Get module
     *
     * @return \AppBundle\Entity\PModule
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Add patient
     *
     * @param \AppBundle\Entity\PPatient $patient
     *
     * @return PCategorie
     */
    public function addPatient(\AppBundle\Entity\PPatient $patient)
    {
        $this->patients[] = $patient;
    
        return $this;
    }

    /**
     * Remove patient
     *
     * @param \AppBundle\Entity\PPatient $patient
     */
    public function removePatient(\AppBundle\Entity\PPatient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }

    /**
     * Add rubrique
     *
     * @param \AppBundle\Entity\PRubrique $rubrique
     *
     * @return PCategorie
     */
    public function addRubrique(\AppBundle\Entity\PRubrique $rubrique)
    {
        $this->rubrique[] = $rubrique;
    
        return $this;
    }

    /**
     * Remove rubrique
     *
     * @param \AppBundle\Entity\PRubrique $rubrique
     */
    public function removeRubrique(\AppBundle\Entity\PRubrique $rubrique)
    {
        $this->rubrique->removeElement($rubrique);
    }

    /**
     * Get rubrique
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }
}
