<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * s_stages
 *
 * @ORM\Table(name="s_stages")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\s_stagesRepository")
 */
class s_stages {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="abreviation", type="string", length=255, nullable=true)
     */
    private $abreviation;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcPromotion", inversedBy="stages")
     * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id")
     * 
     */
    private $promotion;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcFormation", inversedBy="stages")
     * @ORM\JoinColumn(name="formation_id", referencedColumnName="id")
     */
    private $formation;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="stages")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="Specialite", inversedBy="stages")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id")
     */
    private $specialite;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="Responsable", inversedBy="stages")
     * @ORM\JoinColumn(name="responsable_id", referencedColumnName="id")
     */
    private $responsable;

    /**
     * @var string
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="fermer", type="string", length=10, nullable=true)
     */
    private $fermer;

    /**
     * @ORM\OneToMany(targetEntity="SObjniv1", mappedBy="stage")
     */
    private $objNiv1;

  
    
    
    
       /**
     * @ORM\ManyToMany(targetEntity="SGroupStage" ,mappedBy="stage")
     * @ORM\JoinTable(name="module_group")
     */
 
    private $module_stg_grp;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="enabled", type="integer" , nullable=true)
     * 
     */
    private $active = 1;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcEtablissement", inversedBy="stages")
     * @ORM\JoinColumn(name="ac_etablissement_id", referencedColumnName="id")
     */
    private $etablissement;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="stageUserCreated")
     * @ORM\JoinColumn(name="user_created", referencedColumnName="id")
     */
    private $userCreated;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="stageUserUpdated")
     * @ORM\JoinColumn(name="user_updated", referencedColumnName="id")
     */
    private $userUpdated;

 
 
 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->objNiv1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->module_stg_grp = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return s_stages
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return s_stages
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set fermer
     *
     * @param string $fermer
     *
     * @return s_stages
     */
    public function setFermer($fermer)
    {
        $this->fermer = $fermer;
    
        return $this;
    }

    /**
     * Get fermer
     *
     * @return string
     */
    public function getFermer()
    {
        return $this->fermer;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return s_stages
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return s_stages
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return s_stages
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     *
     * @return s_stages
     */
    public function setPromotion(\AppBundle\Entity\AcPromotion $promotion = null)
    {
        $this->promotion = $promotion;
    
        return $this;
    }

    /**
     * Get promotion
     *
     * @return \AppBundle\Entity\AcPromotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set formation
     *
     * @param \AppBundle\Entity\AcFormation $formation
     *
     * @return s_stages
     */
    public function setFormation(\AppBundle\Entity\AcFormation $formation = null)
    {
        $this->formation = $formation;
    
        return $this;
    }

    /**
     * Get formation
     *
     * @return \AppBundle\Entity\AcFormation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Set service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return s_stages
     */
    public function setService(\AppBundle\Entity\Service $service = null)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return \AppBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set specialite
     *
     * @param \AppBundle\Entity\Specialite $specialite
     *
     * @return s_stages
     */
    public function setSpecialite(\AppBundle\Entity\Specialite $specialite = null)
    {
        $this->specialite = $specialite;
    
        return $this;
    }

    /**
     * Get specialite
     *
     * @return \AppBundle\Entity\Specialite
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * Set responsable
     *
     * @param \AppBundle\Entity\Responsable $responsable
     *
     * @return s_stages
     */
    public function setResponsable(\AppBundle\Entity\Responsable $responsable = null)
    {
        $this->responsable = $responsable;
    
        return $this;
    }

    /**
     * Get responsable
     *
     * @return \AppBundle\Entity\Responsable
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Add objNiv1
     *
     * @param \AppBundle\Entity\SObjniv1 $objNiv1
     *
     * @return s_stages
     */
    public function addObjNiv1(\AppBundle\Entity\SObjniv1 $objNiv1)
    {
        $this->objNiv1[] = $objNiv1;
    
        return $this;
    }

    /**
     * Remove objNiv1
     *
     * @param \AppBundle\Entity\SObjniv1 $objNiv1
     */
    public function removeObjNiv1(\AppBundle\Entity\SObjniv1 $objNiv1)
    {
        $this->objNiv1->removeElement($objNiv1);
    }

    /**
     * Get objNiv1
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjNiv1()
    {
        return $this->objNiv1;
    }

    /**
     * Add moduleStgGrp
     *
     * @param \AppBundle\Entity\SGroupStage $moduleStgGrp
     *
     * @return s_stages
     */
    public function addModuleStgGrp(\AppBundle\Entity\SGroupStage $moduleStgGrp)
    {
        $this->module_stg_grp[] = $moduleStgGrp;
    
        return $this;
    }

    /**
     * Remove moduleStgGrp
     *
     * @param \AppBundle\Entity\SGroupStage $moduleStgGrp
     */
    public function removeModuleStgGrp(\AppBundle\Entity\SGroupStage $moduleStgGrp)
    {
        $this->module_stg_grp->removeElement($moduleStgGrp);
    }

    /**
     * Get moduleStgGrp
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleStgGrp()
    {
        return $this->module_stg_grp;
    }

    /**
     * Set etablissement
     *
     * @param \AppBundle\Entity\AcEtablissement $etablissement
     *
     * @return s_stages
     */
    public function setEtablissement(\AppBundle\Entity\AcEtablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;
    
        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \AppBundle\Entity\AcEtablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Set userCreated
     *
     * @param \AppBundle\Entity\User $userCreated
     *
     * @return s_stages
     */
    public function setUserCreated(\AppBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userUpdated
     *
     * @param \AppBundle\Entity\User $userUpdated
     *
     * @return s_stages
     */
    public function setUserUpdated(\AppBundle\Entity\User $userUpdated = null)
    {
        $this->userUpdated = $userUpdated;
    
        return $this;
    }

    /**
     * Get userUpdated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }
}
