<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TObjectifEtudiant
 *
 * @ORM\Table(name="t_objectif_etudiant")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TObjectifEtudiantRepository")
 */
class TObjectifEtudiant {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="TInscription", inversedBy="etudiant")
     * @ORM\JoinColumn(name="inscription_id", referencedColumnName="id")
     */
    private $inscription;

    /**
     * @ORM\ManyToOne(targetEntity="PPatient", inversedBy="obj_etudiant")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     */
    private $patients;

    /**
     * @ORM\ManyToOne(targetEntity="SObjniv3", inversedBy="obj_etudiant")
     * @ORM\JoinColumn(name="objN3_id", referencedColumnName="id")
     */
    private $objNiveau3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="date", nullable=true)
     */
    private $dateCreated;

    /**
     * @var int
     *
     * @ORM\Column(name="user_created", type="integer", nullable=true)
     */
    private $userCreated;


  


    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return TObjectifEtudiant
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set userCreated
     *
     * @param integer $userCreated
     *
     * @return TObjectifEtudiant
     */
    public function setUserCreated($userCreated)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return integer
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set inscription
     *
     * @param \AppBundle\Entity\TInscription $inscription
     *
     * @return TObjectifEtudiant
     */
    public function setInscription(\AppBundle\Entity\TInscription $inscription = null)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return \AppBundle\Entity\TInscription
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set patients
     *
     * @param \AppBundle\Entity\PPatient $patients
     *
     * @return TObjectifEtudiant
     */
    public function setPatients(\AppBundle\Entity\PPatient $patients = null)
    {
        $this->patients = $patients;
    
        return $this;
    }

    /**
     * Get patients
     *
     * @return \AppBundle\Entity\PPatient
     */
    public function getPatients()
    {
        return $this->patients;
    }

    /**
     * Set objNiveau3
     *
     * @param \AppBundle\Entity\SObjniv3 $objNiveau3
     *
     * @return TObjectifEtudiant
     */
    public function setObjNiveau3(\AppBundle\Entity\SObjniv3 $objNiveau3 = null)
    {
        $this->objNiveau3 = $objNiveau3;
    
        return $this;
    }

    /**
     * Get objNiveau3
     *
     * @return \AppBundle\Entity\SObjniv3
     */
    public function getObjNiveau3()
    {
        return $this->objNiveau3;
    }
}
