<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AcEtablissement
 *
 * @ORM\Table(name="ac_etablissement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcEtablissementRepository")

 */
class AcEtablissement {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, unique=true ,  nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=100, nullable=true)
     */
    private $abreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=100, nullable=true)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="doyen", type="string", length=100, nullable=true)
     */
    private $doyen;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=100, nullable=true)
     */
    private $nature;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '0';

    /**
     * @ORM\OneToMany(targetEntity="AcFormation", mappedBy="etablissement")
     */
    private $formations;

    /**
     * @ORM\OneToMany(targetEntity="s_stages", mappedBy="etablissement")
     */
    private $stages;

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->formations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->stages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return AcEtablissement
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return AcEtablissement
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return AcEtablissement
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return AcEtablissement
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    
        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set doyen
     *
     * @param string $doyen
     *
     * @return AcEtablissement
     */
    public function setDoyen($doyen)
    {
        $this->doyen = $doyen;
    
        return $this;
    }

    /**
     * Get doyen
     *
     * @return string
     */
    public function getDoyen()
    {
        return $this->doyen;
    }

    /**
     * Set nature
     *
     * @param string $nature
     *
     * @return AcEtablissement
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
    
        return $this;
    }

    /**
     * Get nature
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return AcEtablissement
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return AcEtablissement
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add formation
     *
     * @param \AppBundle\Entity\AcFormation $formation
     *
     * @return AcEtablissement
     */
    public function addFormation(\AppBundle\Entity\AcFormation $formation)
    {
        $this->formations[] = $formation;
    
        return $this;
    }

    /**
     * Remove formation
     *
     * @param \AppBundle\Entity\AcFormation $formation
     */
    public function removeFormation(\AppBundle\Entity\AcFormation $formation)
    {
        $this->formations->removeElement($formation);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormations()
    {
        return $this->formations;
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     *
     * @return AcEtablissement
     */
    public function addStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stages[] = $stage;
    
        return $this;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     */
    public function removeStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stages->removeElement($stage);
    }

    /**
     * Get stages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStages()
    {
        return $this->stages;
    }
}
