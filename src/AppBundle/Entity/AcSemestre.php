<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcSemestre
 *
 * @ORM\Table(name="ac_semestre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcSemestreRepository")
 */
class AcSemestre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true, unique=true)
     */
    private $code;

  

    /**
     * @var string
     *
     * @ORM\Column(name="codePromotion", type="string", length=100, nullable=true)
     */
    private $codePromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="validationExamen", type="string", length=3, nullable=true)
     */
    private $validationExamen;

    /**
     * @var string
     *
     * @ORM\Column(name="clotureExamen", type="string", length=3, nullable=true)
     */
    private $clotureExamen;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var int
     *
     * @ORM\Column(name="absence", type="integer", nullable=true)
     */
    private $absence;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficient", type="float", nullable=true)
     */
    private $coefficient;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficientAss", type="float", nullable=true)
     */
    private $coefficientAss;

   

    
       
     /**
     * @ORM\ManyToOne(targetEntity="AcPromotion")
     * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id")
     */
    private $promotion;

  

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return AcSemestre
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codePromotion
     *
     * @param string $codePromotion
     *
     * @return AcSemestre
     */
    public function setCodePromotion($codePromotion)
    {
        $this->codePromotion = $codePromotion;

        return $this;
    }

    /**
     * Get codePromotion
     *
     * @return string
     */
    public function getCodePromotion()
    {
        return $this->codePromotion;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return AcSemestre
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set validationExamen
     *
     * @param string $validationExamen
     *
     * @return AcSemestre
     */
    public function setValidationExamen($validationExamen)
    {
        $this->validationExamen = $validationExamen;

        return $this;
    }

    /**
     * Get validationExamen
     *
     * @return string
     */
    public function getValidationExamen()
    {
        return $this->validationExamen;
    }

    /**
     * Set clotureExamen
     *
     * @param string $clotureExamen
     *
     * @return AcSemestre
     */
    public function setClotureExamen($clotureExamen)
    {
        $this->clotureExamen = $clotureExamen;

        return $this;
    }

    /**
     * Get clotureExamen
     *
     * @return string
     */
    public function getClotureExamen()
    {
        return $this->clotureExamen;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return AcSemestre
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set absence
     *
     * @param integer $absence
     *
     * @return AcSemestre
     */
    public function setAbsence($absence)
    {
        $this->absence = $absence;

        return $this;
    }

    /**
     * Get absence
     *
     * @return integer
     */
    public function getAbsence()
    {
        return $this->absence;
    }

    /**
     * Set coefficient
     *
     * @param float $coefficient
     *
     * @return AcSemestre
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return float
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * Set coefficientAss
     *
     * @param float $coefficientAss
     *
     * @return AcSemestre
     */
    public function setCoefficientAss($coefficientAss)
    {
        $this->coefficientAss = $coefficientAss;

        return $this;
    }

    /**
     * Get coefficientAss
     *
     * @return float
     */
    public function getCoefficientAss()
    {
        return $this->coefficientAss;
    }

    /**
     * Set promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     *
     * @return AcSemestre
     */
    public function setPromotion(\AppBundle\Entity\AcPromotion $promotion = null)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \AppBundle\Entity\AcPromotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }
}
