<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * PStatut
 *
 * @ORM\Table(name="p_statut")
 * @ORM\Entity
 */
class PStatut {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=100, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=4, nullable=true)
     */
    private $abreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="table_0", type="string", length=100, nullable=true)
     */
    private $table0;

    /**
     * @var string
     *
     * @ORM\Column(name="phase_0", type="string", length=100, nullable=true)
     */
    private $phase0;

    /**
     * @var integer
     *
     * @ORM\Column(name="visible", type="integer", nullable=false)
     */
    private $visible = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="visible_admission", type="integer", nullable=true)
     */
    private $visibleAdmission;

    /**
     * @var string
     *
     * @ORM\Column(name="next", type="string", length=100, nullable=true)
     */
    private $next;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(name="annuler", type="smallint", nullable=false)
     */
    private $annuler;

    

    

    public function __construct() {

       
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return PStatut
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return PStatut
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return PStatut
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set table0
     *
     * @param string $table0
     *
     * @return PStatut
     */
    public function setTable0($table0)
    {
        $this->table0 = $table0;
    
        return $this;
    }

    /**
     * Get table0
     *
     * @return string
     */
    public function getTable0()
    {
        return $this->table0;
    }

    /**
     * Set phase0
     *
     * @param string $phase0
     *
     * @return PStatut
     */
    public function setPhase0($phase0)
    {
        $this->phase0 = $phase0;
    
        return $this;
    }

    /**
     * Get phase0
     *
     * @return string
     */
    public function getPhase0()
    {
        return $this->phase0;
    }

    /**
     * Set visible
     *
     * @param integer $visible
     *
     * @return PStatut
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return integer
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set visibleAdmission
     *
     * @param integer $visibleAdmission
     *
     * @return PStatut
     */
    public function setVisibleAdmission($visibleAdmission)
    {
        $this->visibleAdmission = $visibleAdmission;
    
        return $this;
    }

    /**
     * Get visibleAdmission
     *
     * @return integer
     */
    public function getVisibleAdmission()
    {
        return $this->visibleAdmission;
    }

    /**
     * Set next
     *
     * @param string $next
     *
     * @return PStatut
     */
    public function setNext($next)
    {
        $this->next = $next;
    
        return $this;
    }

    /**
     * Get next
     *
     * @return string
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return PStatut
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set annuler
     *
     * @param integer $annuler
     *
     * @return PStatut
     */
    public function setAnnuler($annuler)
    {
        $this->annuler = $annuler;
    
        return $this;
    }

    /**
     * Get annuler
     *
     * @return integer
     */
    public function getAnnuler()
    {
        return $this->annuler;
    }
}
