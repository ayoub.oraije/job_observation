<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SObjniv2
 *
 * @ORM\Table(name="s_objniv2")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SObjniv2Repository")
 */
class SObjniv2
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SObjniv1", inversedBy="objNiv2")
     * @ORM\JoinColumn(name="obj_niv1", referencedColumnName="id")
     */
    private $objNiv1;
    
    
    
      /**
     * @ORM\OneToMany(targetEntity="SObjniv3", mappedBy="objNiv2")
     */
    private $objNiv3;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=255, nullable=true)
     */
    private $abreviation;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;
  

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->objNiv3 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return SObjniv2
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return SObjniv2
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SObjniv2
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return SObjniv2
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set objNiv1
     *
     * @param \AppBundle\Entity\SObjniv1 $objNiv1
     *
     * @return SObjniv2
     */
    public function setObjNiv1(\AppBundle\Entity\SObjniv1 $objNiv1 = null)
    {
        $this->objNiv1 = $objNiv1;
    
        return $this;
    }

    /**
     * Get objNiv1
     *
     * @return \AppBundle\Entity\SObjniv1
     */
    public function getObjNiv1()
    {
        return $this->objNiv1;
    }

    /**
     * Add objNiv3
     *
     * @param \AppBundle\Entity\SObjniv3 $objNiv3
     *
     * @return SObjniv2
     */
    public function addObjNiv3(\AppBundle\Entity\SObjniv3 $objNiv3)
    {
        $this->objNiv3[] = $objNiv3;
    
        return $this;
    }

    /**
     * Remove objNiv3
     *
     * @param \AppBundle\Entity\SObjniv3 $objNiv3
     */
    public function removeObjNiv3(\AppBundle\Entity\SObjniv3 $objNiv3)
    {
        $this->objNiv3->removeElement($objNiv3);
    }

    /**
     * Get objNiv3
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjNiv3()
    {
        return $this->objNiv3;
    }

    /**
     * Set userCreated
     *
     * @param \AppBundle\Entity\User $userCreated
     *
     * @return SObjniv2
     */
    public function setUserCreated(\AppBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userUpdated
     *
     * @param \AppBundle\Entity\User $userUpdated
     *
     * @return SObjniv2
     */
    public function setUserUpdated(\AppBundle\Entity\User $userUpdated = null)
    {
        $this->userUpdated = $userUpdated;
    
        return $this;
    }

    /**
     * Get userUpdated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }
}
