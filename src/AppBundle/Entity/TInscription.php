<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TInscription
 *
 * @ORM\Table(name="t_inscription")
 * @ORM\Entity
 */
class TInscription {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="code_admission", type="string", length=100, nullable=true)
     */
    private $codeAdmission;

    /**
     * @var boolean
     *
     * @ORM\Column(name="statut", type="boolean", nullable=true)
     */
    private $st;

    /**
     * @var string
     *
     * @ORM\Column(name="code_promotion", type="string", length=100, nullable=true)
     */
    private $codePromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="code_annee", type="string", length=100, nullable=true)
     */
    private $codeAnnee;

    /**
     * @var string
     *
     * @ORM\Column(name="code_statut", type="string", length=10, nullable=true)
     */
    private $codeStatut;

    /**
     * @var integer
     *
     * @ORM\Column(name="anonymat", type="integer", nullable=true)
     */
    private $anonymat;

    /**
     * @var integer
     *
     * @ORM\Column(name="anonymatrat", type="integer", nullable=true)
     */
    private $anonymatrat;

    /**
     * @var string
     *
     * @ORM\Column(name="salle", type="string", length=50, nullable=true)
     */
    private $salle;

    /**
     * @var integer
     *
     * @ORM\Column(name="emplacement", type="integer", nullable=true)
     */
    private $emplacement;

    /**
     * @var string
     *
     * @ORM\Column(name="type_cand", type="string", length=50, nullable=true)
     */
    private $typeCand;

    /**
     * @ORM\ManyToOne(targetEntity="TAdmission")
     * @ORM\JoinColumn(name="t_admission_id", referencedColumnName="id")
     */
    private $admission;

    /**
     * @ORM\ManyToOne(targetEntity="PStatut")
     * @ORM\JoinColumn(name="p_statut_id", referencedColumnName="id")
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="AcAnnee")
     * @ORM\JoinColumn(name="ac_annee_id", referencedColumnName="id")
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="AcPromotion")
     * @ORM\JoinColumn(name="ac_promotion_id", referencedColumnName="id")
     */
    private $promotion;

   
    

   /**
     * @ORM\OneToMany(targetEntity="TObjectifEtudiant", mappedBy="inscription")
     */
    private $etudiant;
    
     /**
     * @ORM\OneToMany(targetEntity="SInsStg", mappedBy="inscription")
     */
    private $ins_stg;
    
    
 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->etudiant = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ins_stg = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TInscription
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeAdmission
     *
     * @param string $codeAdmission
     *
     * @return TInscription
     */
    public function setCodeAdmission($codeAdmission)
    {
        $this->codeAdmission = $codeAdmission;
    
        return $this;
    }

    /**
     * Get codeAdmission
     *
     * @return string
     */
    public function getCodeAdmission()
    {
        return $this->codeAdmission;
    }

    /**
     * Set st
     *
     * @param boolean $st
     *
     * @return TInscription
     */
    public function setSt($st)
    {
        $this->st = $st;
    
        return $this;
    }

    /**
     * Get st
     *
     * @return boolean
     */
    public function getSt()
    {
        return $this->st;
    }

    /**
     * Set codePromotion
     *
     * @param string $codePromotion
     *
     * @return TInscription
     */
    public function setCodePromotion($codePromotion)
    {
        $this->codePromotion = $codePromotion;
    
        return $this;
    }

    /**
     * Get codePromotion
     *
     * @return string
     */
    public function getCodePromotion()
    {
        return $this->codePromotion;
    }

    /**
     * Set codeAnnee
     *
     * @param string $codeAnnee
     *
     * @return TInscription
     */
    public function setCodeAnnee($codeAnnee)
    {
        $this->codeAnnee = $codeAnnee;
    
        return $this;
    }

    /**
     * Get codeAnnee
     *
     * @return string
     */
    public function getCodeAnnee()
    {
        return $this->codeAnnee;
    }

    /**
     * Set codeStatut
     *
     * @param string $codeStatut
     *
     * @return TInscription
     */
    public function setCodeStatut($codeStatut)
    {
        $this->codeStatut = $codeStatut;
    
        return $this;
    }

    /**
     * Get codeStatut
     *
     * @return string
     */
    public function getCodeStatut()
    {
        return $this->codeStatut;
    }

    /**
     * Set anonymat
     *
     * @param integer $anonymat
     *
     * @return TInscription
     */
    public function setAnonymat($anonymat)
    {
        $this->anonymat = $anonymat;
    
        return $this;
    }

    /**
     * Get anonymat
     *
     * @return integer
     */
    public function getAnonymat()
    {
        return $this->anonymat;
    }

    /**
     * Set anonymatrat
     *
     * @param integer $anonymatrat
     *
     * @return TInscription
     */
    public function setAnonymatrat($anonymatrat)
    {
        $this->anonymatrat = $anonymatrat;
    
        return $this;
    }

    /**
     * Get anonymatrat
     *
     * @return integer
     */
    public function getAnonymatrat()
    {
        return $this->anonymatrat;
    }

    /**
     * Set salle
     *
     * @param string $salle
     *
     * @return TInscription
     */
    public function setSalle($salle)
    {
        $this->salle = $salle;
    
        return $this;
    }

    /**
     * Get salle
     *
     * @return string
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * Set emplacement
     *
     * @param integer $emplacement
     *
     * @return TInscription
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;
    
        return $this;
    }

    /**
     * Get emplacement
     *
     * @return integer
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set typeCand
     *
     * @param string $typeCand
     *
     * @return TInscription
     */
    public function setTypeCand($typeCand)
    {
        $this->typeCand = $typeCand;
    
        return $this;
    }

    /**
     * Get typeCand
     *
     * @return string
     */
    public function getTypeCand()
    {
        return $this->typeCand;
    }

    /**
     * Set admission
     *
     * @param \AppBundle\Entity\TAdmission $admission
     *
     * @return TInscription
     */
    public function setAdmission(\AppBundle\Entity\TAdmission $admission = null)
    {
        $this->admission = $admission;
    
        return $this;
    }

    /**
     * Get admission
     *
     * @return \AppBundle\Entity\TAdmission
     */
    public function getAdmission()
    {
        return $this->admission;
    }

    /**
     * Set statut
     *
     * @param \AppBundle\Entity\PStatut $statut
     *
     * @return TInscription
     */
    public function setStatut(\AppBundle\Entity\PStatut $statut = null)
    {
        $this->statut = $statut;
    
        return $this;
    }

    /**
     * Get statut
     *
     * @return \AppBundle\Entity\PStatut
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set annee
     *
     * @param \AppBundle\Entity\AcAnnee $annee
     *
     * @return TInscription
     */
    public function setAnnee(\AppBundle\Entity\AcAnnee $annee = null)
    {
        $this->annee = $annee;
    
        return $this;
    }

    /**
     * Get annee
     *
     * @return \AppBundle\Entity\AcAnnee
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     *
     * @return TInscription
     */
    public function setPromotion(\AppBundle\Entity\AcPromotion $promotion = null)
    {
        $this->promotion = $promotion;
    
        return $this;
    }

    /**
     * Get promotion
     *
     * @return \AppBundle\Entity\AcPromotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Add etudiant
     *
     * @param \AppBundle\Entity\TObjectifEtudiant $etudiant
     *
     * @return TInscription
     */
    public function addEtudiant(\AppBundle\Entity\TObjectifEtudiant $etudiant)
    {
        $this->etudiant[] = $etudiant;
    
        return $this;
    }

    /**
     * Remove etudiant
     *
     * @param \AppBundle\Entity\TObjectifEtudiant $etudiant
     */
    public function removeEtudiant(\AppBundle\Entity\TObjectifEtudiant $etudiant)
    {
        $this->etudiant->removeElement($etudiant);
    }

    /**
     * Get etudiant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Add insStg
     *
     * @param \AppBundle\Entity\SInsStg $insStg
     *
     * @return TInscription
     */
    public function addInsStg(\AppBundle\Entity\SInsStg $insStg)
    {
        $this->ins_stg[] = $insStg;
    
        return $this;
    }

    /**
     * Remove insStg
     *
     * @param \AppBundle\Entity\SInsStg $insStg
     */
    public function removeInsStg(\AppBundle\Entity\SInsStg $insStg)
    {
        $this->ins_stg->removeElement($insStg);
    }

    /**
     * Get insStg
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInsStg()
    {
        return $this->ins_stg;
    }
}
