<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * s_ins_stg
 *
 * @ORM\Table(name="s_ins_stg")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\s_ins_stgRepository")
 */
class SInsStg {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   

    

 
         /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="stageUserCreated")
     * @ORM\JoinColumn(name="user_created", referencedColumnName="id")
     */
    private $userCreated;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="stageUserUpdated")
     * @ORM\JoinColumn(name="user_updated", referencedColumnName="id")
     */
    private $userUpdated;
    
    
    
       /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;
    

    /**
     * @var int
     *
     * @ORM\Column(name="statut", type="integer", nullable=true)
     */
    private $statut;
    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="TInscription", inversedBy="ins_stg")
     * @ORM\JoinColumn(name="t_inscription_id", referencedColumnName="id")
     */
    private $inscription;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="SGroupStage")
     * @ORM\JoinColumn(name="s_group_stage_id", referencedColumnName="id")
     */
    private $groupe;

 
    /**
     * @ORM\ManyToOne(targetEntity="AcAnnee")
     * @ORM\JoinColumn(name="ac_annee_id", referencedColumnName="id")
     */
    private $annee;

   
    /**
     * 
     * @ORM\ManyToOne(targetEntity="AcPromotion", inversedBy="ins_group")
     * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id")
     * 
     */
    private $promotion;


   


 
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SInsStg
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return SInsStg
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set statut
     *
     * @param integer $statut
     *
     * @return SInsStg
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    
        return $this;
    }

    /**
     * Get statut
     *
     * @return integer
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set userCreated
     *
     * @param \AppBundle\Entity\User $userCreated
     *
     * @return SInsStg
     */
    public function setUserCreated(\AppBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userUpdated
     *
     * @param \AppBundle\Entity\User $userUpdated
     *
     * @return SInsStg
     */
    public function setUserUpdated(\AppBundle\Entity\User $userUpdated = null)
    {
        $this->userUpdated = $userUpdated;
    
        return $this;
    }

    /**
     * Get userUpdated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }

    /**
     * Set inscription
     *
     * @param \AppBundle\Entity\TInscription $inscription
     *
     * @return SInsStg
     */
    public function setInscription(\AppBundle\Entity\TInscription $inscription = null)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return \AppBundle\Entity\TInscription
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set groupe
     *
     * @param \AppBundle\Entity\SGroupStage $groupe
     *
     * @return SInsStg
     */
    public function setGroupe(\AppBundle\Entity\SGroupStage $groupe = null)
    {
        $this->groupe = $groupe;
    
        return $this;
    }

    /**
     * Get groupe
     *
     * @return \AppBundle\Entity\SGroupStage
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * Set annee
     *
     * @param \AppBundle\Entity\AcAnnee $annee
     *
     * @return SInsStg
     */
    public function setAnnee(\AppBundle\Entity\AcAnnee $annee = null)
    {
        $this->annee = $annee;
    
        return $this;
    }

    /**
     * Get annee
     *
     * @return \AppBundle\Entity\AcAnnee
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     *
     * @return SInsStg
     */
    public function setPromotion(\AppBundle\Entity\AcPromotion $promotion = null)
    {
        $this->promotion = $promotion;
    
        return $this;
    }

    /**
     * Get promotion
     *
     * @return \AppBundle\Entity\AcPromotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }
}
