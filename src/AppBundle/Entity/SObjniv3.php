<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SObjniv3
 *
 * @ORM\Table(name="s_objniv3")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SObjniv3Repository")
 */
class SObjniv3 {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SObjniv2", inversedBy="objNiv3")
     * @ORM\JoinColumn(name="obj_niv2", referencedColumnName="id")
     */
    private $objNiv2;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="abreviation", type="string", length=255, nullable=true)
     */
    private $abreviation;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="objniv3User")
     * @ORM\JoinColumn(name="user_created", referencedColumnName="id")
     */
    private $userCreated;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="objniv3User")
     * @ORM\JoinColumn(name="user_updated", referencedColumnName="id")
     */
    private $userUpdated;

   
    
      /**
     * @ORM\OneToMany(targetEntity="TObjectifEtudiant", mappedBy="objNiveau3")
     */
    private $obj_etudiant;

  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->obj_etudiant = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return SObjniv3
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set abreviation
     *
     * @param string $abreviation
     *
     * @return SObjniv3
     */
    public function setAbreviation($abreviation)
    {
        $this->abreviation = $abreviation;
    
        return $this;
    }

    /**
     * Get abreviation
     *
     * @return string
     */
    public function getAbreviation()
    {
        return $this->abreviation;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SObjniv3
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return SObjniv3
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set objNiv2
     *
     * @param \AppBundle\Entity\SObjniv2 $objNiv2
     *
     * @return SObjniv3
     */
    public function setObjNiv2(\AppBundle\Entity\SObjniv2 $objNiv2 = null)
    {
        $this->objNiv2 = $objNiv2;
    
        return $this;
    }

    /**
     * Get objNiv2
     *
     * @return \AppBundle\Entity\SObjniv2
     */
    public function getObjNiv2()
    {
        return $this->objNiv2;
    }

    /**
     * Set userCreated
     *
     * @param \AppBundle\Entity\User $userCreated
     *
     * @return SObjniv3
     */
    public function setUserCreated(\AppBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userUpdated
     *
     * @param \AppBundle\Entity\User $userUpdated
     *
     * @return SObjniv3
     */
    public function setUserUpdated(\AppBundle\Entity\User $userUpdated = null)
    {
        $this->userUpdated = $userUpdated;
    
        return $this;
    }

    /**
     * Get userUpdated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }

    /**
     * Add objEtudiant
     *
     * @param \AppBundle\Entity\TObjectifEtudiant $objEtudiant
     *
     * @return SObjniv3
     */
    public function addObjEtudiant(\AppBundle\Entity\TObjectifEtudiant $objEtudiant)
    {
        $this->obj_etudiant[] = $objEtudiant;
    
        return $this;
    }

    /**
     * Remove objEtudiant
     *
     * @param \AppBundle\Entity\TObjectifEtudiant $objEtudiant
     */
    public function removeObjEtudiant(\AppBundle\Entity\TObjectifEtudiant $objEtudiant)
    {
        $this->obj_etudiant->removeElement($objEtudiant);
    }

    /**
     * Get objEtudiant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjEtudiant()
    {
        return $this->obj_etudiant;
    }
}
