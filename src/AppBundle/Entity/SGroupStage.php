<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * s_group_stage
 *
 * @ORM\Table(name="s_group_stage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\s_group_stageRepository")
 */
class SGroupStage {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="observation", type="string", length=255, nullable=true)
     */
    private $observation;

    /**
     * @var int
     *
     * @ORM\Column(name="fermer", type="integer", nullable=true)
     */
    private $fermer;

    /**
     * @ORM\ManyToMany(targetEntity="s_stages" ,inversedBy="module_stg_grp")
     * @ORM\JoinTable(name="module_group")
     */
    protected $stage;

    /**
     * @var \DateTime
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="date_debut", type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *@Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="enabled", type="integer" , nullable=true)
     * 
     */
    private $active = 1;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime" , nullable=true)
     * 
     */
    private $created;

    /**
     * 
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     * 
     */
    private $updated;

    /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcFormation")
     * @ORM\JoinColumn(name="ac_formation_id", referencedColumnName="id")
     */
    private $formation;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="stageUserCreated")
     * @ORM\JoinColumn(name="user_created", referencedColumnName="id")
     */
    private $userCreated;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="stageUserUpdated")
     * @ORM\JoinColumn(name="user_updated", referencedColumnName="id")
     */
    private $userUpdated;

      /**
     * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcPromotion", inversedBy="stages")
     * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id")
     * 
     */
    private $promotion;

   /**
    * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcEtablissement", inversedBy="stages")
     * @ORM\JoinColumn(name="ac_etablissement_id", referencedColumnName="id")
     */
    private $etablissement;
      /**
    * @Assert\NotBlank(message="Champ obligatoire.")
     * @ORM\ManyToOne(targetEntity="AcSemestre")
     * @ORM\JoinColumn(name="id_semestre", referencedColumnName="id")
     */
    private $semestre;
  
 
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return SGroupStage
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set fermer
     *
     * @param integer $fermer
     *
     * @return SGroupStage
     */
    public function setFermer($fermer)
    {
        $this->fermer = $fermer;

        return $this;
    }

    /**
     * Get fermer
     *
     * @return integer
     */
    public function getFermer()
    {
        return $this->fermer;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return SGroupStage
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return SGroupStage
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return SGroupStage
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SGroupStage
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return SGroupStage
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     *
     * @return SGroupStage
     */
    public function addStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stage[] = $stage;

        return $this;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\s_stages $stage
     */
    public function removeStage(\AppBundle\Entity\s_stages $stage)
    {
        $this->stage->removeElement($stage);
    }

    /**
     * Get stage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set formation
     *
     * @param \AppBundle\Entity\AcFormation $formation
     *
     * @return SGroupStage
     */
    public function setFormation(\AppBundle\Entity\AcFormation $formation = null)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \AppBundle\Entity\AcFormation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Set userCreated
     *
     * @param \AppBundle\Entity\User $userCreated
     *
     * @return SGroupStage
     */
    public function setUserCreated(\AppBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userUpdated
     *
     * @param \AppBundle\Entity\User $userUpdated
     *
     * @return SGroupStage
     */
    public function setUserUpdated(\AppBundle\Entity\User $userUpdated = null)
    {
        $this->userUpdated = $userUpdated;

        return $this;
    }

    /**
     * Get userUpdated
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }

    /**
     * Set promotion
     *
     * @param \AppBundle\Entity\AcPromotion $promotion
     *
     * @return SGroupStage
     */
    public function setPromotion(\AppBundle\Entity\AcPromotion $promotion = null)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \AppBundle\Entity\AcPromotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set etablissement
     *
     * @param \AppBundle\Entity\AcEtablissement $etablissement
     *
     * @return SGroupStage
     */
    public function setEtablissement(\AppBundle\Entity\AcEtablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \AppBundle\Entity\AcEtablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Set semestre
     *
     * @param \AppBundle\Entity\AcSemestre $semestre
     *
     * @return SGroupStage
     */
    public function setSemestre(\AppBundle\Entity\AcSemestre $semestre = null)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre
     *
     * @return \AppBundle\Entity\AcSemestre
     */
    public function getSemestre()
    {
        return $this->semestre;
    }
}
