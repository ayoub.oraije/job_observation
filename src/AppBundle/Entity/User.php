<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="default_p", type="string", length=100,nullable=true)
     */
    private $defaultP;

    /**
     * @var string
     *
     * @ORM\Column(name="notification", type="integer",nullable=true)
     */
    private $notification;

    /**
     * @var string
     *
     * @ORM\Column(name="first_login", type="integer",nullable=true)
     */
    private $firstLogin;

    /**
     * @ORM\OneToMany(targetEntity="s_stages", mappedBy="userCreated")
     */
    private $stageUserCreated;

    /**
     * @ORM\OneToMany(targetEntity="s_stages", mappedBy="userUpdated")
     */
    private $stageUserUpdated;

    /**
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * 
     * 
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="TEtudiant", inversedBy="user")
     * @ORM\JoinColumn(name="t_etudiant_id", referencedColumnName="id")
     */
    private $etudiant;

  

    /**
     * Set defaultP
     *
     * @param string $defaultP
     *
     * @return User
     */
    public function setDefaultP($defaultP)
    {
        $this->defaultP = $defaultP;

        return $this;
    }

    /**
     * Get defaultP
     *
     * @return string
     */
    public function getDefaultP()
    {
        return $this->defaultP;
    }

    /**
     * Set notification
     *
     * @param integer $notification
     *
     * @return User
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return integer
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set firstLogin
     *
     * @param integer $firstLogin
     *
     * @return User
     */
    public function setFirstLogin($firstLogin)
    {
        $this->firstLogin = $firstLogin;

        return $this;
    }

    /**
     * Get firstLogin
     *
     * @return integer
     */
    public function getFirstLogin()
    {
        return $this->firstLogin;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Add stageUserCreated
     *
     * @param \AppBundle\Entity\s_stages $stageUserCreated
     *
     * @return User
     */
    public function addStageUserCreated(\AppBundle\Entity\s_stages $stageUserCreated)
    {
        $this->stageUserCreated[] = $stageUserCreated;

        return $this;
    }

    /**
     * Remove stageUserCreated
     *
     * @param \AppBundle\Entity\s_stages $stageUserCreated
     */
    public function removeStageUserCreated(\AppBundle\Entity\s_stages $stageUserCreated)
    {
        $this->stageUserCreated->removeElement($stageUserCreated);
    }

    /**
     * Get stageUserCreated
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStageUserCreated()
    {
        return $this->stageUserCreated;
    }

    /**
     * Add stageUserUpdated
     *
     * @param \AppBundle\Entity\s_stages $stageUserUpdated
     *
     * @return User
     */
    public function addStageUserUpdated(\AppBundle\Entity\s_stages $stageUserUpdated)
    {
        $this->stageUserUpdated[] = $stageUserUpdated;

        return $this;
    }

    /**
     * Remove stageUserUpdated
     *
     * @param \AppBundle\Entity\s_stages $stageUserUpdated
     */
    public function removeStageUserUpdated(\AppBundle\Entity\s_stages $stageUserUpdated)
    {
        $this->stageUserUpdated->removeElement($stageUserUpdated);
    }

    /**
     * Get stageUserUpdated
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStageUserUpdated()
    {
        return $this->stageUserUpdated;
    }

    /**
     * Set etudiant
     *
     * @param \AppBundle\Entity\TEtudiant $etudiant
     *
     * @return User
     */
    public function setEtudiant(\AppBundle\Entity\TEtudiant $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \AppBundle\Entity\TEtudiant
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }
}
