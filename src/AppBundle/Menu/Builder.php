<?php

// src/AppBundle/Menu/Builder.php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface {

    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');

        $menu->addChild('Home', array('route' => 'homepage'));

        // access services from the container!
        $em = $this->container->get('doctrine')->getManager();
        // findMostRecent and Blog are just imaginary examples
        $blog = $em->getRepository('AppBundle:Blog')->findMostRecent();

        $menu->addChild('Latest Blog Post', array(
            'route' => 'blog_show',
            'routeParameters' => array('id' => $blog->getId())
        ));

        // create another menu item
        $menu->addChild('About Me', array('route' => 'about'));
        // you can also add sub level's to your menu's as follows
        $menu['About Me']->addChild('Edit profile', array('route' => 'edit_profile'));

        // ... add more children

        return $menu;
    }

//    public function etudiantHeaderMenu(FactoryInterface $factory, array $options) {
//        //  $menu = $factory->createItem('root');
//        $menu = $factory->createItem('root', array(
//            'childrenAttributes' => array(
//                'class' => 'nav nav-tabs padding-18 tab-size-bigger'),
//        ));
//
//
//
//
//        $menu->addChild('FINANCIER', array('route' => 'etudiant_financier'));
//        $menu->addChild('ASSIDUITE', array('route' => 'etudiant_bibliotheque'));
//        $menu->addChild('About Me', array('route' => 'etudiant_academique'));
//        
//
//
//        return $menu;
//    }

    
     public function BreadcrumbMenu(FactoryInterface $factory,array $options ) {
        $menu = $factory->createItem('root');

        $menu->addChild('Home', array('route' => 'etudiant_index'));

        
        
//
        $menu['Home']->addChild('Edit profile', array('route' => 'etudiant_financier'));

//        $request = $this->container->get('request');
//$routeName = $request->get('_route');
     //   var_dump($factory); 
    //    dump($this->container->get('request')->getRequestUri());
        

        return $menu;
    }
}
