<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcAnnee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Acannee controller.
 *
 * @Route("acannee")
 */
class AcAnneeController extends Controller
{
    /**
     * Lists all acAnnee entities.
     *
     * @Route("/", name="acannee_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acAnnees = $em->getRepository('AppBundle:AcAnnee')->findAll();

        return $this->render('acannee/index.html.twig', array(
            'acAnnees' => $acAnnees,
        ));
    }

    /**
     * Creates a new acAnnee entity.
     *
     * @Route("/new", name="acannee_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $acAnnee = new Acannee();
        $form = $this->createForm('AppBundle\Form\AcAnneeType', $acAnnee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acAnnee);
            $em->flush();

            return $this->redirectToRoute('acannee_show', array('id' => $acAnnee->getId()));
        }

        return $this->render('acannee/new.html.twig', array(
            'acAnnee' => $acAnnee,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a acAnnee entity.
     *
     * @Route("/{id}", name="acannee_show")
     * @Method("GET")
     */
    public function showAction(AcAnnee $acAnnee)
    {
        $deleteForm = $this->createDeleteForm($acAnnee);

        return $this->render('acannee/show.html.twig', array(
            'acAnnee' => $acAnnee,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing acAnnee entity.
     *
     * @Route("/{id}/edit", name="acannee_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AcAnnee $acAnnee)
    {
        $deleteForm = $this->createDeleteForm($acAnnee);
        $editForm = $this->createForm('AppBundle\Form\AcAnneeType', $acAnnee);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acannee_edit', array('id' => $acAnnee->getId()));
        }

        return $this->render('acannee/edit.html.twig', array(
            'acAnnee' => $acAnnee,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a acAnnee entity.
     *
     * @Route("/{id}", name="acannee_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AcAnnee $acAnnee)
    {
        $form = $this->createDeleteForm($acAnnee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($acAnnee);
            $em->flush();
        }

        return $this->redirectToRoute('acannee_index');
    }

    /**
     * Creates a form to delete a acAnnee entity.
     *
     * @param AcAnnee $acAnnee The acAnnee entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AcAnnee $acAnnee)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acannee_delete', array('id' => $acAnnee->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
