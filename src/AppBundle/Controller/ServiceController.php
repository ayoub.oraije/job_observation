<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service controller.
 *
 * @Route("service")
 */
class ServiceController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="service_list")
     * 
     */
    public function serviceListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $service = $em->getRepository('AppBundle:Service')->findAll();

        foreach ($service as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getDesignation();


            $url = $this->container->get('router')->generate('service_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('service_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";

            if ($value->getActive() == 1): $icon = "btn-success fa-unlock";
            else: $icon = "btn-danger fa-lock";
            endif;
            $nestedData[] = "<a class='disable_action' rel='" . $value->getId() . "' href='" . $value->getActive() . "'> <i class='btn btn-xs btn-warning ace-icon fa $icon bigger-120'></i></a>";

            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all service entities.
     *
     * @Route("/", name="service_index")
     * @Method("GET")
     */
    public function indexAction() {
        $lien = 1;
        $li = 1;
        $em = $this->getDoctrine()->getManager();

        $services = $em->getRepository('AppBundle:Service')->findAll();

        return $this->render('service/index.html.twig', array(
                    'services' => $services,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new service entity.
     *
     * @Route("/new", name="service_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $service = new Service();
        $form = $this->createForm('AppBundle\Form\ServiceType', $service);
        $form->handleRequest($request);
        $lien = 1;
        $li = 2;
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $service->setActive(1);
            $now = date_create('now');
            $service->setCreated($now);
            $service->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em->persist($service);
            $em->flush();

            $this->addFlash(
                    'notice', 'l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('service_index');
        }

        return $this->render('service/new.html.twig', array(
                    'service' => $service,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a service entity.
     *
     * @Route("/{id}", name="service_show")
     * @Method("GET")
     */
    public function showAction(Service $service) {
        $deleteForm = $this->createDeleteForm($service);
        $lien = 1;
        $li = 0;
        return $this->render('service/show.html.twig', array(
                    'service' => $service,
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing service entity.
     *
     * @Route("/{id}/edit", name="service_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Service $service) {
        $deleteForm = $this->createDeleteForm($service);
        $editForm = $this->createForm('AppBundle\Form\ServiceType', $service);
        $editForm->handleRequest($request);
        $lien = 1;
        $li = 0;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $now = date_create('now');
            $service->setUpdated($now);
            $service->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            $this->getDoctrine()->getManager()->flush();
            $flashbag = $this->get('session')->getFlashBag();

            // Set a flash message
            $this->addFlash(
                    'notice', ' la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('service_index');
        }

        return $this->render('service/edit.html.twig', array(
                    'service' => $service,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

//
//    /**
//     * Deletes a service entity.
//     *
//     * @Route("/{id}", name="service_delete")
//     * 
//     */
//    public function deleteAction(Request $request, Service $service) {
//        $form = $this->createDeleteForm($service);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($service);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('service_index');
//    }

    /**
     * Deletes a service entity.
     *
     * @Route("delete/{id}", options = { "expose" = true }  , name="service_delete")
     * 
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('AppBundle:Service')->findOneBy(array('id' => $id));
        $stage = $em->getRepository('AppBundle:s_stages')->findby(array('service' => $service));

        if ($stage) {
            $json_data = array(
                'data' => 'Suppression a echoué, le service "' . $service->getDesignation() . '"  affecter à un stage.',
                'a' => 0,
            );
        } else {
            $em->remove($service);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
                'a' => 1,
            );
        }

        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a service entity.
     *
     * @Route("/delete/{id}", options = { "expose" = true }  , name="service_delete1")
     *
     */
    public function deleteServiceAction($id) {

        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('AppBundle:Service')->find($id);
        $stage = $em->getRepository('AppBundle:s_stages')->findby(array('service' => $id));

        if ($stage) {
            $this->addFlash(
                    'error', 'Suppression a echoué.'
            );
        } else {
            $em->remove($service);
            $em->flush();
            $this->addFlash(
                    'notice', 'La suppression a été effectuée avec succès'
            );
        }

        return $this->redirectToRoute('service_index');
    }

    /**
     * Creates a form to delete a service entity.
     *
     * @param Service $service The service entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Service $service) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('service_delete', array('id' => $service->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * 
     *
     * @Route("/disable/{id}/{etat}" ,options = { "expose" = true } , name="service_disable")
     * 
     */
    public function DisableAction($id, $etat) {
        $text = "";
        if ($etat == 1) {
            $etat = 0;
            $text = "désactivé";
        } else {
            $etat = 1;
            $text = "activé";
        }

        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('AppBundle:Service')->find($id);

        $service->setActive($etat);
        $em->flush();

        $json_data = array(
            'data' => 'Le champ à été ' . $text . ' avec succés',
        );


        return new Response(json_encode($json_data));
    }

}
