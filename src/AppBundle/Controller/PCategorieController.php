<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PCategorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Pcategorie controller.
 *
 * @Route("pcategorie")
 */
class PCategorieController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="categorie_list")
     * 
     */
    public function categorieListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $service = $em->getRepository('AppBundle:PCategorie')->findAll();

        foreach ($service as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getDesignation();
            $nestedData[] = $value->getModule()->getDesignation();


            if ($value->getIsPatient() == 1) {
                $nestedData[] = 'OUI';
            } else {
                $nestedData[] = 'NON';
            }






            $url = $this->container->get('router')->generate('pcategorie_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('pcategorie_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";


            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all pCategorie entities.
     *
     * @Route("/", name="pcategorie_index")
     * @Method("GET")
     */
    public function indexAction() {
        $lien = 4;
        $li = 13;
        $em = $this->getDoctrine()->getManager();

        $pCategories = $em->getRepository('AppBundle:PCategorie')->findAll();

        return $this->render('pcategorie/index.html.twig', array(
                    'pCategories' => $pCategories,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new pCategorie entity.
     *
     * @Route("/new", name="pcategorie_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $pCategorie = new Pcategorie();
        $form = $this->createForm('AppBundle\Form\PCategorieType', $pCategorie);
        $form->handleRequest($request);
        $lien = 4;
        $li = 14;
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pCategorie);
            $em->flush();


            $this->addFlash(
                    'notice', 'l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('pcategorie_index');





         
        }

        return $this->render('pcategorie/new.html.twig', array(
                    'pCategorie' => $pCategorie,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a pCategorie entity.
     *
     * @Route("/{id}", name="pcategorie_show")
     * @Method("GET")
     */
    public function showAction(PCategorie $pCategorie) {
        $lien = 0;
        $li = 0;
        $deleteForm = $this->createDeleteForm($pCategorie);

        return $this->render('pcategorie/show.html.twig', array(
                    'pCategorie' => $pCategorie,
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing pCategorie entity.
     *
     * @Route("/{id}/edit", name="pcategorie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PCategorie $pCategorie) {
        $lien = 0;
        $li = 0;
        $deleteForm = $this->createDeleteForm($pCategorie);
        $editForm = $this->createForm('AppBundle\Form\PCategorieType', $pCategorie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            
                $this->addFlash(
                    'notice', ' la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('pcategorie_index');
            
            
            
            
           
        }

        return $this->render('pcategorie/edit.html.twig', array(
                    'pCategorie' => $pCategorie,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a form to delete a pCategorie entity.
     *
     * @param PCategorie $pCategorie The pCategorie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PCategorie $pCategorie) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pcategorie_delete', array('id' => $pCategorie->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * Deletes a service entity.
     *
     * @Route("delete/{id}", options = { "expose" = true }  , name="pcategorie_delete")
     * 
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $categrie = $em->getRepository('AppBundle:PCategorie')->findOneBy(array('id' => $id));
        $patient = $em->getRepository('AppBundle:PPatient')->findby(array('categorie' => $categrie));

        if ($patient) {
            $json_data = array(
                'data' => 'Suppression a echoué, le service "' . $categrie->getDesignation() . '"  affecter à un stage.',
                'a' => 0,
            );
        } else {
            $em->remove($categrie);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
                'a' => 1,
            );
        }

        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a service entity.
     *
     * @Route("/delete1/{id}", options = { "expose" = true }  , name="pcategorie_delete1")
     *
     */
    public function deleteCategorieAction($id) {

        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository('AppBundle:PCategorie')->find($id);
        $patient = $em->getRepository('AppBundle:PPatient')->findby(array('categorie' => $categorie));

        if ($patient) {
            $this->addFlash(
                    'error', 'Suppression a echoué.'
            );
        } else {
            $em->remove($categorie);
            $em->flush();
            $this->addFlash(
                    'notice', 'La suppression a été effectuée avec succès'
            );
        }

        return $this->redirectToRoute('pcategorie_index');
    }

}
