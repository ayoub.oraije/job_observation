<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * User controller.
 *
 * @Route("administrateur/user")
 */
class UserController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="utilisateur_list")
     * 
     */
    public function userListAction() {
        $data = array();
        //access user manager services 
//        $usermanager = $this->get('fos_user.user_manager');
//        $users = $usermanager->findUsers();

        $query = $this->getDoctrine()->getEntityManager()
                        ->createQuery(
                                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
                        )->setParameter('role', '%"ROLE_ADMIN"%');

        $users = $query->getResult();


        foreach ($users as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getLastname();
            $nestedData[] = $value->getFirstname();
            $nestedData[] = $value->getUsername();
            $nestedData[] = $value->getEmail();

            $url = $this->container->get('router')->generate('user_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('user_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";

            if ($value->isEnabled() == 1): $icon = "btn-success fa-unlock";
            else: $icon = "btn-danger fa-lock";
            endif;
            $nestedData[] = "<a class='disable_action' rel='" . $value->getId() . "' href='" . $value->isEnabled() . "'> <i class='btn btn-xs btn-warning ace-icon fa $icon bigger-120'></i></a>";

            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }

        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * 
     *
     * @Route("/etudiantslist",options = { "expose" = true } , name="etudiant_user_list")
     * 
     */
    public function etudiantuserListAction() {
        $data = array();
        //access user manager services 
//        $usermanager = $this->get('fos_user.user_manager');
//        $users = $usermanager->findUsers();

        $query = $this->getDoctrine()->getEntityManager()
                        ->createQuery(
                                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
                        )->setParameter('role', '%"ROLE_ETUDIANT"%');

        $users = $query->getResult();


        foreach ($users as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getLastname();
            $nestedData[] = $value->getFirstname();
            $nestedData[] = $value->getUsername();
            $nestedData[] = $value->getEmail();

            $url = $this->container->get('router')->generate('user_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('user_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";

            if ($value->isEnabled() == 1): $icon = "btn-success fa-unlock";
            else: $icon = "btn-danger fa-lock";
            endif;
            $nestedData[] = "<a class='disable_action' rel='" . $value->getId() . "' href='" . $value->isEnabled() . "'> <i class='btn btn-xs btn-warning ace-icon fa $icon bigger-120'></i></a>";

            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }

        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction() {
//        $usermanager = $this->get('fos_user.user_manager');
//        $users = $usermanager->findUsers();

        $query = $this->getDoctrine()->getEntityManager()
                        ->createQuery(
                                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
                        )->setParameter('role', '%"ROLE_SUPER_ADMIN"%');

        $users = $query->getResult();
        $lien = 0;
        $li = 1;
        return $this->render('user/index.html.twig', array(
                    'users' => $users,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Lists all user entities.
     *
     * @Route("/etudiants", name="etudiant_user_index")
     * @Method("GET")
     */
    public function indexetudiantAction() {
//        $usermanager = $this->get('fos_user.user_manager');
//        $users = $usermanager->findUsers();

        $query = $this->getDoctrine()->getEntityManager()
                        ->createQuery(
                                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
                        )->setParameter('role', '%"ROLE_ETUDIANT"%');

        $users = $query->getResult();

        return $this->render('etudiants/index.html.twig', array(
                    'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(true);
           // $user->setSuperAdmin(true);
            $user->addRole("ROLE_ADMIN");
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                    'notice', '  enregistrement a été effectué avec succès.'
            );

            return $this->redirectToRoute('user_index');
        }
        $lien = 6;
        $li = 9;
        return $this->render('user/new.html.twig', array(
                    'user' => $user,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new_admission", name="etudiant_user_new")
     * @Method({"GET", "POST"})
     */
    public function newetudiantAction(Request $request) {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(true);
//            $user->setSuperAdmin(true);
            $user->addRole("ROLE_ETUDIANT");
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                    'notice', '  enregistrement a été effectué avec succès.'
            );

            return $this->redirectToRoute('etudiant_user_index');
        }

        return $this->render('etudiants/new.html.twig', array(
                    'user' => $user,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user) {
        $deleteForm = $this->createDeleteForm($user);
        $lien = 6;
        $li = 9;
        return $this->render('user/show.html.twig', array(
                    'user' => $user,
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user) {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                    'notice', '  Modification a été effectué avec succès.'
            );

//            return $this->redirectToRoute('user_index');
            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }
        $lien = 6;
        $li = 9;
        return $this->render('user/edit.html.twig', array(
                    'user' => $user,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/delete/{id}", options = { "expose" = true }  , name="user_delete_")
     * 
     */
    public function deleteuserAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $em->remove($user);
        $em->flush();
        $this->addFlash(
                'notice', '  Suppression a été effectué avec succès.'
        );

        return $this->redirectToRoute('user_index');
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}/", options = { "expose" = true } , name="user_delete")
     * 
     */
    public function deleteAction($id) {
        $usermanager = $this->get('fos_user.user_manager');
        $user = $usermanager->findUserBy(array('id' => $id));
        $usermanager->deleteUser($user);

        $json_data = array(
            'data' => 'Suppression a été effectué avec succès.',
        );


        return new Response(json_encode($json_data));

//        $form = $this->createDeleteForm($user);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($user);
//            $em->flush();
//        }
//        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * 
     *
     * @Route("/disable/{id}/{etat}" ,options = { "expose" = true } , name="user_disable")
     * 
     */
    public function DisableAction($id, $etat) {
        $text = "";
        if ($etat == 1) {
            $etat = 0;
            $text = "désactivé";
        } else {
            $etat = 1;
            $text = "activé";
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        $user->setEnabled($etat);
        $em->flush();

        $json_data = array(
            'data' => 'L\'utilisateur ' . $user->getUsername() . ' a été ' . $text . ' avec succes',
        );


        return new Response(json_encode($json_data));
    }

}
