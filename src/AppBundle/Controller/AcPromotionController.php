<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcPromotion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Acpromotion controller.
 *
 * @Route("acpromotion")
 */
class AcPromotionController extends Controller
{
    /**
     * Lists all acPromotion entities.
     *
     * @Route("/", name="acpromotion_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acPromotions = $em->getRepository('AppBundle:AcPromotion')->findAll();

        return $this->render('acpromotion/index.html.twig', array(
            'acPromotions' => $acPromotions,
        ));
    }

    /**
     * Creates a new acPromotion entity.
     *
     * @Route("/new", name="acpromotion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $acPromotion = new Acpromotion();
        $form = $this->createForm('AppBundle\Form\AcPromotionType', $acPromotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acPromotion);
            $em->flush();

            return $this->redirectToRoute('acpromotion_show', array('id' => $acPromotion->getId()));
        }

        return $this->render('acpromotion/new.html.twig', array(
            'acPromotion' => $acPromotion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a acPromotion entity.
     *
     * @Route("/{id}", name="acpromotion_show")
     * @Method("GET")
     */
    public function showAction(AcPromotion $acPromotion)
    {
        $deleteForm = $this->createDeleteForm($acPromotion);

        return $this->render('acpromotion/show.html.twig', array(
            'acPromotion' => $acPromotion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing acPromotion entity.
     *
     * @Route("/{id}/edit", name="acpromotion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AcPromotion $acPromotion)
    {
        $deleteForm = $this->createDeleteForm($acPromotion);
        $editForm = $this->createForm('AppBundle\Form\AcPromotionType', $acPromotion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acpromotion_edit', array('id' => $acPromotion->getId()));
        }

        return $this->render('acpromotion/edit.html.twig', array(
            'acPromotion' => $acPromotion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a acPromotion entity.
     *
     * @Route("/{id}", name="acpromotion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AcPromotion $acPromotion)
    {
        $form = $this->createDeleteForm($acPromotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($acPromotion);
            $em->flush();
        }

        return $this->redirectToRoute('acpromotion_index');
    }

    /**
     * Creates a form to delete a acPromotion entity.
     *
     * @param AcPromotion $acPromotion The acPromotion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AcPromotion $acPromotion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acpromotion_delete', array('id' => $acPromotion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
