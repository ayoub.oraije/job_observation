<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Responsable;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Responsable controller.
 *
 * @Route("responsable")
 */
class ResponsableController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="responsable_list")
     * 
     */
    public function ResponsableListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $responsable = $em->getRepository('AppBundle:Responsable')->findAll();




        foreach ($responsable as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getDesignation();



            $url = $this->container->get('router')->generate('responsable_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('responsable_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";



            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all responsable entities.
     *
     * @Route("/", name="responsable_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $responsables = $em->getRepository('AppBundle:Responsable')->findAll();
        $lien = 3;
        $li = 5;
        return $this->render('responsable/index.html.twig', array(
                    'responsables' => $responsables,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new responsable entity.
     *
     * @Route("/new", name="responsable_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $responsable = new Responsable();
        $form = $this->createForm('AppBundle\Form\ResponsableType', $responsable);
        $form->handleRequest($request);
        $lien = 3;
        $li = 6;
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $now = date_create('now');
            $responsable->setCreated($now);
            $responsable->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em->persist($responsable);
            $em->flush();
            $this->addFlash(
                    'notice', 'l\'enregistrement a été effectué avec succès'
            );
            return $this->redirectToRoute('responsable_index');
        }

        return $this->render('responsable/new.html.twig', array(
                    'responsable' => $responsable,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a responsable entity.
     *
     * @Route("/{id}", name="responsable_show")
     * @Method("GET")
     */
    public function showAction(Responsable $responsable) {
        $deleteForm = $this->createDeleteForm($responsable);
           $lien = 3;
        $li = 0;
        return $this->render('responsable/show.html.twig', array(
                    'responsable' => $responsable,
                    'delete_form' => $deleteForm->createView(),
            'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing responsable entity.
     *
     * @Route("/{id}/edit", name="responsable_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Responsable $responsable) {
        $deleteForm = $this->createDeleteForm($responsable);
        $editForm = $this->createForm('AppBundle\Form\ResponsableType', $responsable);
        $editForm->handleRequest($request);
    $lien = 3;
        $li = 0;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $now = date_create('now');
            $responsable->setUpdated($now);
            $responsable->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', 'la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('responsable_index');
        }

        return $this->render('responsable/edit.html.twig', array(
                    'responsable' => $responsable,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
            'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Deletes a responsable entity.
     *
     * @Route("delete/{id}", options = { "expose" = true }  , name="responsable_delete")
     * 
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $responsable = $em->getRepository('AppBundle:Responsable')->findOneBy(array('id' => $id));
        $stage = $em->getRepository('AppBundle:s_stages')->findBy(array('responsable' => $responsable));

        if ($stage) {
            $json_data = array(
                'data' => 'Suppression a echoué, Ce Responsable "' . $responsable->getDesignation() . '" affecté à un ou plusieurs Stages.',
                'e' => 1,
            );
        } else {
            $em->remove($responsable);
            $em->flush();
            $json_data = array(
                'data' => 'La Suppression a été effectuée avec succès',
                'e' => 2,
            );
        }

        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a Responsable entity.
     *
     * @Route("/delete/{id}", options = { "expose" = true }  , name="responsable_delete1")
     *
     */
    public function deleteResponsableAction($id) {
        $em = $this->getDoctrine()->getManager();
        $responsable = $em->getRepository('AppBundle:Responsable')->find($id);
        $stage = $em->getRepository('AppBundle:s_stages')->findby(array('responsable' => $id));

        if ($stage) {
            $this->addFlash(
                    'error', 'Suppression a echoué, Ce Responsable "' . $responsable->getDesignation() . '" affecté à un ou plusieurs stages.'
            );
        } else {
            $em->remove($responsable);
            $em->flush();
            $this->addFlash(
                    'notice', 'La suppression a été effectuée avec succès'
            );
        }

        return $this->redirectToRoute('responsable_index');
    }

    /**
     * Creates a form to delete a responsable entity.
     *
     * @param Responsable $responsable The responsable entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Responsable $responsable) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('responsable_delete', array('id' => $responsable->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
