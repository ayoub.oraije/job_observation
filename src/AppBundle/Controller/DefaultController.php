<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
            {

        
        

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('administrateur_index');
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT')) {
            return $this->redirectToRoute('patient_index');
        }else {
            return $this->redirectToRoute('homepage');
        } 

        //return $this->render('patient_index');
    }

    /**
     * @Route("/tst", name="tst")
     */
    public function tstAction(Request $request) {

        $sql = " SELECT * FROM fos_user";
        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        //dump($result);
       // die();

// create a simple Response with a 200 status code (the default)
        $response = new Response('Hello ' . $name, Response::HTTP_OK);

        return $response;
    }
    
    

     

}
