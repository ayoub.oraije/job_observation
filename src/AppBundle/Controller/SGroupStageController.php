<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SGroupStage;
use AppBundle\Entity\s_stages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Doctrine\ORM\EntityRepository;

/**
 * Sgroupstage controller.
 *
 * @Route("sgroupstage")
 */
class SGroupStageController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="sgroup_list")
     * 
     */
    public function ResponsableListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $groupStage = $em->getRepository('AppBundle:SGroupStage')->findAll(array(), array('id' => 'DESC'));




        foreach ($groupStage as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getObservation();
            $nestedData[] = $value->getFormation()->getDesignation();
            $nestedData[] = $value->getPromotion()->getDesignation();
            /*if ($value->getFermer() == 1)
                $nestedData[] = 'Oui';
            else
                $nestedData[] = 'Non';*/
            //$nestedData[] = $value->getFermer();
            $serializer = new Serializer(array(new DateTimeNormalizer('Y/m/d')));

            $nestedData[] = $serializer->normalize($value->getDateDebut());
            $nestedData[] = $serializer->normalize($value->getDateFin());

            if ($value->getActive() == 1): $icon = "btn-success fa-unlock";
            else: $icon = "btn-danger fa-lock";
            endif;
            $nestedData[] = "<a class='disable_action' rel='" . $value->getId() . "' href='" . $value->getActive() . "'> <i class='btn btn-xs btn-warning ace-icon fa $icon bigger-120'></i></a>";

            //  if($value->getActive()==1) $nestedData[] ='Oui';else $nestedData[] ='Non';
            //$nestedData[] = $value->getActive();




            $url = $this->container->get('router')->generate('sgroupstage_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('sgroupstage_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";



            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all sGroupStage entities.
     *
     * @Route("/", name="sgroupstage_index")
     * @Method("GET")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();

        $sGroupStages = $em->getRepository('AppBundle:SGroupStage')->findAll();
        $lien = 4;
        $li = 7;
        return $this->render('sgroupstage/index.html.twig', array(
                    'sGroupStages' => $sGroupStages,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new sGroupStage entity.
     *
     * @Route("/new", name="sgroupstage_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $sGroupStage = new Sgroupstage();
        $form = $this->createForm('AppBundle\Form\SGroupStageType', $sGroupStage);
        $form->handleRequest($request);
        $lien = 4;
        $li = 8;
        // $var=$request->get('appbundle_sgroupstage[dateDebut]');

        if ($form->isSubmitted() && $form->isValid()) {

            // var_dump(); die();
            $date1 = $sGroupStage->getDateDebut()->format('Y-m-d');
            //\DateTime::createFromFormat("Y-m-d", $sGroupStage->getDateDebut());
            //die($date1);
            $date2 = $sGroupStage->getDateFin()->format('Y-m-d');
            //\DateTime::createFromFormat("Y-m-d", $request->get('dateFin'));
            if ($date1 < $date2) {
                if ($sGroupStage->getActive() == 1) {
                    $sGroupStage->setFermer(1);
                } else {
                    $sGroupStage->setFermer(0);
                }
                $em = $this->getDoctrine()->getManager();
                $sGroupStage->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
                $now = date_create('now');
                $sGroupStage->setCreated($now);
                $em->persist($sGroupStage);
                $em->flush();
                $this->addFlash('notice', ' l\'enregistrement a été effectué avec succès');
                return $this->redirectToRoute('sgroupstage_index');
            } else {

                $this->addFlash('error', 'La date de début depasse la date de fin');
                return $this->redirectToRoute('sgroupstage_new'
                );
            }
        }

        return $this->render('sgroupstage/new.html.twig', array(
                    'sGroupStage' => $sGroupStage,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a sGroupStage entity.
     *
     * @Route("/{id}", name="sgroupstage_show")
     * @Method("GET")
     */
    public function showAction(SGroupStage $sGroupStage) {
        $deleteForm = $this->createDeleteForm($sGroupStage);

        $diff = date_diff($sGroupStage->getDateDebut(), $sGroupStage->getDateFin());
        $lien = 4;
        $li = 0;
        return $this->render('sgroupstage/show.html.twig', array(
                    'sGroupStage' => $sGroupStage,
                    'dif' => $diff->days,
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds 
     *
     * @Route("/etab/{id}", options = { "expose" = true } , name="etab_grp")
     * @Method({"GET"})
     */
    public function etabliAction($id) {

        $em = $this->getDoctrine()->getManager();


        $forma = $em->getRepository('AppBundle:AcEtablissement')->GetEtablissement($id);

        $json_data = array('data' => $forma,);

        return new Response(json_encode($json_data));
    }

    /**
     * Finds 
     *
     * @Route("/forma/{id}", options = { "expose" = true } , name="forma_grp")
     * @Method({"GET"})
     */
    public function formationAction($id) {

        $em = $this->getDoctrine()->getManager();
        //$em = $this->getDoctrine()->getManager();
//$this->getEntityManager()->getRepository('AppBundle:s_objniv2')->findBy(array('objNiv1' => $obj1));
        $etablissement = $em->getRepository('AppBundle:AcEtablissement')->find($id);

        $forma = $em->getRepository('AppBundle:AcEtablissement')->GetFormation($etablissement, $id);

        $json_data = array('data' => $forma,);

        return new Response(json_encode($json_data));
    }

    /**
     * Displays a form to edit an existing sGroupStage entity.
     *
     * @Route("/{id}/edit", name="sgroupstage_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SGroupStage $sGroupStage) {
        $deleteForm = $this->createDeleteForm($sGroupStage);
        $editForm = $this->createForm('AppBundle\Form\SGroupStageType', $sGroupStage);
        $editForm->handleRequest($request);
        $lien = 4;
        $li = 0;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $date1 = $sGroupStage->getDateDebut()->format('Y-m-d');
            //\DateTime::createFromFormat("Y-m-d", $sGroupStage->getDateDebut());
            //die($date1);
            $date2 = $sGroupStage->getDateFin()->format('Y-m-d');
            if ($date1 < $date2) {
                if ($sGroupStage->getActive() == 1) {
                    $sGroupStage->setFermer(1);
                } else {
                    $sGroupStage->setFermer(0);
                }
                $sGroupStage->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
                $now = date_create('now');
                $sGroupStage->setUpdated($now);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash(
                        'notice', 'La modification a été effectué avec succès'
                );
                return $this->redirectToRoute('sgroupstage_index', array('id' => $sGroupStage->getId()));
            } else {
                $this->addFlash('error', 'La date de début depasse la date de fin');
                return $this->redirectToRoute('sgroupstage_edit', array('id' => $sGroupStage->getId())
                );
            }
        }

        return $this->render('sgroupstage/edit.html.twig', array(
                    'sGroupStage' => $sGroupStage,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing sGroupStage entity.
     *
     * @Route("/afecter/{id}", name="sgroupstage_affecter")
     * @Method({"GET", "POST"})
     */
    public function affecterAction(Request $request, SGroupStage $sGroupStage, $id) {





        // $stage = $this->getEntityManager()->getRepository('AppBundle:s_stage')->findBy(array('promotion' => $promotion));

        $em = $this->getDoctrine();
        $query = $em->getEntityManager()->getRepository('AppBundle:SGroupStage')->find($id);
       
        if(!$query->getPromotion())
            {
           
            $this->addFlash('error', 'veuillez renseigner la promotion de stage pour effectuer l\'opération');
            return $this->redirectToRoute('sgroupstage_index');
            }
        //var_dump($promotion);
        //die();
            else{
            $formation = $em->getEntityManager()->getRepository('AppBundle:AcFormation')->findOneBy(array('id' => $query->getFormation()->getId()));
        $lien = 4;
        $li = 0;
        $form = $this->createFormBuilder($sGroupStage)
                ->add('stage', EntityType::class, array(
                    'multiple' => 'multiple',
                    'label' => 'Stages :',
                    'attr' => array('multiple' => 'multiple', 'id' => 'duallist'),
                    'class' => 'AppBundle:s_stages',
                    'choice_label' => 'designation',
                    'query_builder' => function (EntityRepository $er ) use($formation) {
                        return $er->createQueryBuilder('e')
                                ->where('e.formation=:v')->setParameter('v', $formation);
                    },
                ))
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-sm btn-primary')))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary')))
                ->getForm();

        // var_dump($form);
        //die();

        $deleteForm = $this->createDeleteForm($sGroupStage);
        // $editForm = $this->createForm('AppBundle\Form\SGroupStageType', $sGroupStage);
        $form->handleRequest($request);
            }
        

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();


            $this->addFlash('notice', 'L\'affectation des Stages  a été  effectué avec succés');
            return $this->redirectToRoute('sgroupstage_index');
        }

        return $this->render('sgroupstage/affecter.html.twig', array(
                    'sGroupStage' => $sGroupStage,
                    'edit_form' => $form->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Deletes a responsable entity.
     *
     * @Route("delete/{id}", options = { "expose" = true }  , name="sgroupstage_delete")
     * 
     */
    public function delete1Action($id) {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $id));
        $ins = $em->getRepository('AppBundle:SInsStg')->findby(array('groupe' => $group));

        if ($ins) {
            $json_data = array(
                'data' => 'Suppression a echoué,',
                'e' => 1,
            );
        } else {
            $em->remove($group);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
                'e' => 2,
            );
        }

        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a sGroupStage entity.
     *
     * @Route("/{id}", name="sgroupstage_delete1")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SGroupStage $sGroupStage) {
        $form = $this->createDeleteForm($sGroupStage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sGroupStage);
            $em->flush();
        }

        return $this->redirectToRoute('sgroupstage_index');
    }

    /**
     * Creates a form to delete a sGroupStage entity.
     *
     * @param SGroupStage $sGroupStage The sGroupStage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SGroupStage $sGroupStage) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('sgroupstage_delete', array('id' => $sGroupStage->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * 
     *
     * @Route("/disable/{id}/{etat}" ,options = { "expose" = true } , name="group_disable")
     * 
     */
    public function DisableAction($id, $etat) {
        $text = "";
        if ($etat == 1) {
            $etat = 0;
            $text = "désactivé";
        } else {
            $etat = 1;
            $text = "activé";
        }

        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('AppBundle:SGroupStage')->find($id);

        $service->setActive($etat);
        $em->flush();

        $json_data = array(
            'data' => 'Le champ à été ' . $text . ' avec succés',
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/dup/{id}", options = { "expose" = true }  , name="stage_dup")
     *  @Method({"GET", "POST"})
     */
    public function groupAction(Request $request, $id) {
        /* var_dump($anne);
          var_dump($request->get('form-field-checkbox'));
          die(); */
        $em = $this->getDoctrine()->getManager();

        $group = $em->getRepository('AppBundle:SGroupStage')->find($id);






        return $this->render('sgroupstage/dupliquer.html.twig', array('group' => $group,));
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/dupp/", options = { "expose" = true }  , name="gr_dup")
     *  @Method({"GET", "POST"})
     */
    public function dupliqAction(Request $request) {
        /* var_dump($anne);
          var_dump($request->get('form-field-checkbox'));
          die(); */
        $em = $this->getDoctrine()->getManager();
        //$request->get('dateDebut');
        //  var_dump($request->get('dateFin')) ;
        // die();
        //$request->get('group');
        $date1 = \DateTime::createFromFormat("Y-m-d", $request->get('dateDebut'));
        $date2 = \DateTime::createFromFormat("Y-m-d", $request->get('dateFin'));

        if ($date1 < $date2) {
            $group = $em->getRepository('AppBundle:SGroupStage')->find($request->get('group'));
            $sGroupStage = new Sgroupstage();
            $sGroupStage->setObservation($group->getObservation());
            $sGroupStage->setFermer($group->getFermer());
            $sGroupStage->setDateDebut(\DateTime::createFromFormat("Y-m-d", $request->get('dateDebut')));
            $sGroupStage->setDateFin(\DateTime::createFromFormat("Y-m-d", $request->get('dateFin')));
            $sGroupStage->setActive($group->getActive());
            $sGroupStage->setFormation($group->getFormation());
            $sGroupStage->setPromotion($group->getPromotion());
            $sGroupStage->setEtablissement($group->getEtablissement());
            $sGroupStage->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            // $sGroupStage->addStage($group->getStage());
            $so = new s_stages();
            foreach ($group->getStage() as $value) {
                $st = $em->getRepository('AppBundle:s_stages')->find($value->getId());

                $sGroupStage->addStage($st);
            }

            $em->persist($sGroupStage);
            $em->flush();






            $json_data = array(
                'data' => 'Le Stage  à été Dupliqué avec succés',
                'e' => '1'
            );
        } else {
            $json_data = array(
                'data' => 'La date de début depasse la date de fin',
                'e' => '2'
            );
        }





        return new Response(json_encode($json_data));
    }

}
