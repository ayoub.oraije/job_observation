<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Specialite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Specialite controller.
 *
 * @Route("specialite")
 */
class SpecialiteController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="specialite_list")
     * 
     */
    public function SpecialiteListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $specialite = $em->getRepository('AppBundle:Specialite')->findAll();




        foreach ($specialite as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getDesignation();



            $url = $this->container->get('router')->generate('specialite_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('specialite_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";



            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all specialite entities.
     *
     * @Route("/", name="specialite_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $specialites = $em->getRepository('AppBundle:Specialite')->findAll();
        $lien = 2;
        $li = 3;

        return $this->render('specialite/index.html.twig', array(
                    'specialites' => $specialites,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new specialite entity.
     *
     * @Route("/new", name="specialite_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $specialite = new Specialite();
        $form = $this->createForm('AppBundle\Form\SpecialiteType', $specialite);
        $form->handleRequest($request);
        $lien = 2;
        $li = 4;
        if ($form->isSubmitted() && $form->isValid()) {
            $now = date_create('now');
            $specialite->setCreated($now);
            $specialite->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($specialite);
            $em->flush();

            $this->addFlash(
                    'notice', ' l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('specialite_index');
        }

        return $this->render('specialite/new.html.twig', array(
                    'specialite' => $specialite,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a specialite entity.
     *
     * @Route("/{id}", name="specialite_show")
     * @Method("GET")
     */
    public function showAction(Specialite $specialite) {
        $deleteForm = $this->createDeleteForm($specialite);
 $lien = 2;
        $li = 0;
        return $this->render('specialite/show.html.twig', array(
                    'specialite' => $specialite,
                    'delete_form' => $deleteForm->createView(),
              'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing specialite entity.
     *
     * @Route("/{id}/edit", name="specialite_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Specialite $specialite) {
        $deleteForm = $this->createDeleteForm($specialite);
        $editForm = $this->createForm('AppBundle\Form\SpecialiteType', $specialite);
        $editForm->handleRequest($request);
 $lien = 2;
        $li = 0;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $now = date_create('now');
            $specialite->setUpdated($now);
            $specialite->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', 'la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('specialite_index');
        }

        return $this->render('specialite/edit.html.twig', array(
                    'specialite' => $specialite,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
            'lien' => $lien,
                    'li' => $li,
        ));
    }

//    /**
//     * Deletes a specialite entity.
//     *
//     * @Route("/{id}", name="specialite_delete")
//     * @Method("DELETE")
//     */
//    public function deleteAction(Request $request, Specialite $specialite)
//    {
//        $form = $this->createDeleteForm($specialite);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($specialite);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('specialite_index');
//    }

    /**
     * Deletes a specialite entity.
     *
     * @Route("delete/{id}", options = { "expose" = true }  , name="specialite_delete")
     * 
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository('AppBundle:Specialite')->findOneBy(array('id' => $id));
        $stage = $em->getRepository('AppBundle:s_stages')->findby(array('specialite' => $specialite));

        if ($stage) {
            $json_data = array(
                'data' => 'Suppression a echoué, Cet Spécialité "' . $specialite->getDesignation() . '" affecté à un ou plusieurs Stages.',
                'e' => 1,
            );
        } else {
            $em->remove($specialite);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
                'e' => 2,
            );
        }

        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a Specialite entity.
     *
     * @Route("/delete/{id}", options = { "expose" = true }  , name="specialite_delete1")
     *
     */
    public function deleteSpecialiteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository('AppBundle:Specialite')->find($id);
        $stage = $em->getRepository('AppBundle:s_stages')->findby(array('specialite' => $id));

        if ($stage) {
            $this->addFlash(
                    'error', 'Suppression a echoué, Cet Spécialité "' . $specialite->getDesignation() . '" affecté à un ou plusieurs stages.'
            );
        } else {
            $em->remove($specialite);
            $em->flush();
            $this->addFlash(
                    'notice', 'Suppression "' . $specialite->getDesignation() . '" est fait avec succés'
            );
        }

        return $this->redirectToRoute('specialite_index');
    }

    /**
     * Creates a form to delete a specialite entity.
     *
     * @param Specialite $specialite The specialite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Specialite $specialite) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('specialite_delete', array('id' => $specialite->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
