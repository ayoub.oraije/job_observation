<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;
use AppBundle\Service\FileUploader3;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Acannee controller.
 *
 * @Route("management")
 */
class ManagerController extends Controller {

    /**
     * 
     *
     * @Route("/users/", name="management_users")
     * 
     */
    public function indexAction() {

        $lien = 0;
        $li = 0;
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $query = $repository->createQueryBuilder('u')
                ->where('u.etudiant IS NOT NULL ')
                ->getQuery();
        $users = $query->getResult();


        // dump($users); die();

        return $this->render('management/users.html.twig', array(
                    'users' => $users,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * 
     *
     * @Route("/users/list",options = { "expose" = true } , name="management_users_list")
     * 
     */
    public function UsersListAction() {
        /*  $data = array();

          // $em = $this->getDoctrine()->getManager();

          // $users = $em->getRepository('AppBundle:User')->findAll(array('etudiant'=> !Null));

          $repository = $this->getDoctrine()->getRepository('AppBundle:User');
          $query = $repository->createQueryBuilder('u')
          ->where('u.etudiant IS NOT NULL ')
          ->getQuery();
          $users = $query->getResult();
          //var_dump($users);
          // die('ok');
          //var_dump($users);

          foreach ($users as $key => $value) {
          $nestedData = array();
          $nestedData[] = ++$key;
          $nestedData[] = "<input type='checkbox' name = 'form-field-checkbox[]' value='" . $value->getId() . "' class='list_users cat'/>";
          $nestedData[] = $value->getEtudiant()->getNom();
          $nestedData[] = $value->getEtudiant()->getPrenom();
          $nestedData[] = $value->getUserName();
          $nestedData[] = $value->getEmail();
          if ($value->isEnabled() == 1) {
          //  $username     = $value->getUsername();
          // $nestedData[] = $value->getUser()->getUsername();

          $nestedData[] = "<a class = 'active_user' href='1' rel = '" . $value->getId() . "'>  <i class='btn btn-xs btn-success ace-icon fa fa-unlock bigger-120'></i></a>";
          } else {
          $nestedData[] = "<a class = 'active_user' href='0'  rel = '" . $value->getId() . "'><i class='btn btn-xs btn-danger ace-icon fa fa-lock bigger-120'></i></a>";
          }



          //            $url = $this->container->get('router')->generate('users_send_mail', array('id' => $value->getId()));
          //            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-info  ace-icon fa fa-send-o bigger-120'></i> </a>";





          $nestedData["DT_RowId"] = $value->getId();
          $data[] = $nestedData;
          }

          $json_data = array(
          "data" => $data
          );


          return new Response(json_encode($json_data));******** */
    }

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="users_list1")
     * @Method({"GET", "POST"})
     */
    public function listAction() {



        $sql = "SELECT fs.id, etu.nom, etu.prenom, fs.`username`,fs.`enabled`,fs.`email` FROM "
                . "`fos_user` fs INNER JOIN t_etudiant etu on fs.`t_etudiant_id`=etu.id  ";

        //$totalRows .= $sql;
        //  $sqlRequest .= $sql;


        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();


        $data = array();

        foreach ($result as $key => $row) {

            $nestedData = array();
            $nestedData[] = ++$key;



            $nestedData[] = "<input type='checkbox' name = 'form-field-checkbox[]' value='" . $row['id'] . "' class='list_users cat'/>";
            $cd = $row['id'];


            $nestedData[] = $row['nom'];
            $nestedData[] = $row['prenom'];
            $nestedData[] = $row['username'];

            $nestedData[] = $row['email'];
            if ($row['enabled'] == 1) {
                //  $username     = $value->getUsername();  
                // $nestedData[] = $value->getUser()->getUsername(); 

                $nestedData[] = "<a class = 'active_user' href='1' rel = '" . $row['id'] . "'>  <i class='btn btn-xs btn-success ace-icon fa fa-unlock bigger-120'></i></a>";
            } else {
                $nestedData[] = "<a class = 'active_user' href='0'  rel = '" . $row['id'] . "'><i class='btn btn-xs btn-danger ace-icon fa fa-lock bigger-120'></i></a>";
            }
            $nestedData["DT_RowId"] = $cd;

            $data[] = $nestedData;
        }

        $json_data = array(
            "data" => $data   // total data array
        );


        return new Response(json_encode($json_data));
    }

    /**
     * 
     *
     * @Route("/users/reset" ,options = { "expose" = true } , name="user_reset_password")
     * 
     */
    public function resetAccountUserAction(Request $request) {

        foreach ($request->request->get('form-field-checkbox') as $key => $value) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:User')->find($value);
            $user->setPassword('$2y$13$XiSyLqZT6h6XROHEBlfTR.ttPE5U3bs4QuFDDoBIwCuwQBMECa/ti');
            $em->persist($user);
        }
        $em->flush();


        $json_data = array(
            'data' => "L'enregistrement a été bien  effectué",
        );


        return new Response(json_encode($json_data));
    }

    /**
     * 
     *
     * @Route("/users/disable/{id}/{etat}" ,options = { "expose" = true } , name="user_disable_account")
     * 
     */
    public function DisableAccountUserAction($id, $etat) {
        $data = array();
        $text = "";
        if ($etat == 1) {
            $etat = 0;
            $text = "désactivé";
        } else {
            $etat = 1;
            $text = "activé";
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        $user->setEnabled($etat);
        $em->flush();



        $json_data = array(
            'data' => 'Le Compte ' . $user->getUsername() . ' a été ' . $text . ' avec succes',
        );


        return new Response(json_encode($json_data));
    }

}
