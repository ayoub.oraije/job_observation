<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PRubrique;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Prubrique controller.
 *
 * @Route("prubrique")
 */
class PRubriqueController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="rubrique_list")
     * 
     */
    public function rubriqueListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $service = $em->getRepository('AppBundle:PRubrique')->findAll();

        foreach ($service as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = $value->getDesignation();
            $nestedData[] = $value->getAbreviation();


            $url = $this->container->get('router')->generate('prubrique_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('prubrique_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";


            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all pRubrique entities.
     *
     * @Route("/", name="prubrique_index")
     * @Method("GET")
     */
    public function indexAction() {
        $lien = 5;
        $li = 15;
        $em = $this->getDoctrine()->getManager();

        $pRubriques = $em->getRepository('AppBundle:PRubrique')->findAll();

        return $this->render('prubrique/index.html.twig', array(
                    'pRubriques' => $pRubriques,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new pRubrique entity.
     *
     * @Route("/new", name="prubrique_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {

        $lien = 5;
        $li = 16;
        $pRubrique = new Prubrique();
        $form = $this->createForm('AppBundle\Form\PRubriqueType', $pRubrique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pRubrique);
            $em->flush();
            $this->addFlash(
                    'notice', 'l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('prubrique_index');
        }

        return $this->render('prubrique/new.html.twig', array(
                    'pRubrique' => $pRubrique,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a pRubrique entity.
     *
     * @Route("/{id}", name="prubrique_show")
     * @Method("GET")
     */
    public function showAction(PRubrique $pRubrique) {
        $lien = 0;
        $li = 0;
        $deleteForm = $this->createDeleteForm($pRubrique);

        return $this->render('prubrique/show.html.twig', array(
                    'pRubrique' => $pRubrique,
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing pRubrique entity.
     *
     * @Route("/{id}/edit", name="prubrique_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PRubrique $pRubrique) {
        $lien = 0;
        $li = 0;
        $deleteForm = $this->createDeleteForm($pRubrique);
        $editForm = $this->createForm('AppBundle\Form\PRubriqueType', $pRubrique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', ' la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('prubrique_index');
        }

        return $this->render('prubrique/edit.html.twig', array(
                    'pRubrique' => $pRubrique,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Deletes a service entity.
     *
     * @Route("delete/{id}", options = { "expose" = true }  , name="prubrique_delete")
     * 
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $rubrique = $em->getRepository('AppBundle:PRubrique')->findOneBy(array('id' => $id));
      
        //$categorie = $em->getRepository('AppBundle:PCategorie')->findBy(array('rubrique' => $rubrique));
       // var_dump($rubrique->getCategorie());
       // die();
        $cat=0;
         foreach ($rubrique->getCategorie() as $value) {
           $cat=1;
                     break;
                    // var_dump($value);
                    // die();
         } 
        
        if ($cat==1) {
            
            $json_data = array(
                'data' => 'Suppression a echoué,"' . $rubrique->getDesignation() . '"  affecter à une Categorie.',
                'a' => 0,
            );
        } else {
           //  die('xx');
            $em->remove($rubrique);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
                'a' => 1,
            );
        }

        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a rub entity.
     *
     * @Route("/delete1/{id}", options = { "expose" = true }  , name="prubrique_delete1")
     *
     */
    public function deleteRubriqueAction($id) {

        $em = $this->getDoctrine()->getManager();
        $rubrique = $em->getRepository('AppBundle:PRubrique')->findOneBy(array('id' => $id));

        $cat=0;
         foreach ($rubrique->getCategorie() as $value) {
           $cat=1;
                     break;
                    // var_dump($value);
                    // die();
         } 
        //var_dump($categorie);
        //die();
        if ($cat==1) {
            $this->addFlash(
                    'error', 'Suppression a echoué.'
            );
        } else {
            $em->remove($rubrique);
            $em->flush();
            $this->addFlash(
                    'notice', 'La suppression a été effectuée avec succès'
            );
        }

        return $this->redirectToRoute('prubrique_index');
    }

    /**
     * Creates a form to delete a pRubrique entity.
     *
     * @param PRubrique $pRubrique The pRubrique entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PRubrique $pRubrique) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('prubrique_delete', array('id' => $pRubrique->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
