<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PPatient;
use AppBundle\Entity\TObjectifEtudiant;
use AppBundle\Entity\TProcessus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\File;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

/**
 * PPatient controller.
 *
 * @Route("patient")
 */
class PPatientController extends Controller {

    /**
     * Lists all patient entities.
     *
     * @Route("/",options = { "expose" = true } , name="patient_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        //dump($this->container->get('security.token_storage')->getToken()->getUser()->getEtudiant()->getId()); die();



        $inscription = $em->getRepository('AppBundle:TEtudiant')->GetInsCriptionActiveAnnee($this->container->get('security.token_storage')->getToken()->getUser()->getEtudiant()->getId());
        $admission = $em->getRepository('AppBundle:TAdmission')->findByCode($this->container->get('security.token_storage')->getToken()->getUserName());
        $inscription = $em->getRepository('AppBundle:TInscription')->findByAdmission($admission);

        if (empty($inscription)) {
            $this->addFlash(
                    'error', utf8_decode('INSCRIPTION NON TROUVÉ')
            );
            return $this->redirectToRoute('fos_user_security_login');
        } else {
            $ins = null;

            foreach ($inscription as $key => $value) {
                if ($value->getAnnee()->getValidationAcademique() == 'non') {
                    $ins = $key;
                    break;
                }
            }
            //  $patients = $em->getRepository('AppBundle:PPatient')->findOneBy(array('inscription' => $inscription));
            // $SInsStg = $em->getRepository('AppBundle:SInsStg')->findOneByInscription($inscription[$ins]);
            $SInsStg = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('inscription' => $inscription[$ins], 'statut' => 0));
//           
//            dump($SInsStg);
//            die();
            if (empty($SInsStg)) {
                $this->addFlash(
                        'error', 'VOUS N\'AVAEZ AUCUN STAGE POUR LE MOMENT'
                );
                return $this->redirectToRoute('fos_user_security_login');
            } else {
                $now = new \DateTime("now");

//dump($now);
// dump($SInsStg->getGroupId()->getDateFin());

                $interval = date_diff($SInsStg->getGroupe()->getDateFin(), $now);
                if (intval($interval->format('%R%a')) > 0) {

                    $this->addFlash(
                            'error', 'Vo'
                            . ''
                            . 'tre période de stage de ' . date_format($SInsStg->getGroupe()->getDateDebut(), 'd/m/Y') . ' A ' . date_format($SInsStg->getGroupe()->getDateFin(), 'd/m/Y') . ' est terminé'
                    );
                    return $this->redirectToRoute('fos_user_security_login');
                }
            }
        }
        $satge = $SInsStg->getGroupe();
        $session = new Session();
        $session->set('stage', $satge);
        $session = new Session();
        $session->set('inscription', $inscription[$ins]);
        $insc = $session->get('inscription');
        $patients = $em->getRepository('AppBundle:PPatient')->findByInscription($session->get('inscription')->getId());

        return $this->render('ppatient/index.html.twig', array(
                    'patients' => $patients,
                    'ins' => $insc,
        ));
    }

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="patient_list")
     * 
     */
    public function patientListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();
        $session = new Session();
        $patient = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $session->get('inscription'), 'groupe' => $session->get('stage')));


        // $patient = $em->getRepository('AppBundle:PPatient')->findByInscription($session->get('inscription'));

        foreach ($patient as $key => $value) {
            $nestedData = array();
            $nestedData[] = "<center>" . ++$key . "</center>";
            $nestedData[] = $value->getDesignation();
            $nestedData[] = $value->getCategorie()->getModule()->getDesignation();
            $nestedData[] = $value->getCategorie()->getDesignation();


//            $url = $this->container->get('router')->generate('patient_edit', array('id' => $value->getId()));
//            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('patient_show', array('id' => $value->getId()));
            $nestedData[] = "<center><a class='' href='" . $url . "'> <i class='btn btn-xs btn-success ace-icon fa fa-eye bigger-120'></i> </a></center>";

            //$url = $this->container->get('router')->generate('patient_edit', array('id' => $value->getId()));
            $nestedData[] = "<center><a class='modif_action'  href1='" . $value->getActive() . "' rel1='" . $value->getId() . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a></center>";

            if ($value->getActive() == 1): $icon = "btn-success fa-unlock";
            else: $icon = "btn-danger fa-lock";
            endif;

            $nestedData[] = "<center><a class='disable_action' rel='" . $value->getId() . "' href='" . $value->getActive() . "'> <i class='btn btn-xs btn-success ace-icon fa $icon bigger-120'></i></a></center>";


            $url2 = $this->container->get('router')->generate('etu_print', array('id' => $value->getId()));
            $nestedData[] = "<center><a class='' href='" . $url2 . "' target='_blank'> <i class='btn btn-xs btn-success ace-icon fa fa-print  bigger-120'></i> </a></center>";


            $nestedData[] = "<center><a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-success  ace-icon fa fa-trash-o bigger-120'></i></a></center>";





//            if ($value->getActive() == 1): $icon = "btn-success fa-unlock";
//            else: $icon = "btn-danger fa-lock";
//            endif;
//            $nestedData[] = "<a class='disable_action' rel='" . $value->getId() . "' href='" . $value->getActive() . "'> <i class='btn btn-xs btn-warning ace-icon fa $icon bigger-120'></i></a>";

            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Creates a new patient entity.
     *
     * @Route("/new_patient",options = { "expose" = true } , name="patient_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {

        $patient = new PPatient();
        $formpatient = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $formpatient->handleRequest($request);
        $formnotpatient = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $formnotpatient->handleRequest($request);
        $formsimulation = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $formsimulation->handleRequest($request);
        $formrecherche = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $formrecherche->handleRequest($request);
        $session = new Session();
//        dump($session->get('stage'));die();
        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->find($session->get('inscription')->getId());
        $stage = $em->getRepository('AppBundle:SGroupStage')->find($session->get('stage')->getId());

        if ($formnotpatient->isSubmitted() && $formnotpatient->isValid()) {

//            dump($formnotpatient->get('cat')->getViewData());
//            die();
            $em = $this->getDoctrine()->getManager();
            $now = date_create('now');
            $patient->setCreated($now);
            $patient->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $patient->setInscription($inscription);
            $categorie = $em->getRepository('AppBundle:PCategorie')->find($formnotpatient->get('cat')->getViewData());
            $patient->setCategorie($categorie);
            $patient->setActive(1);

            $patient->setGroupe($stage);
            $em->persist($patient);
            $em->flush();
            $this->addFlash(
                    'notice', '  l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_show', array('id' => $patient->getId()));
        }
        if ($formsimulation->isSubmitted() && $formsimulation->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $patient->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $patient->setInscription($inscription);
            $categorie = $em->getRepository('AppBundle:PCategorie')->find($formnotpatient->get('cat')->getViewData());
            $patient->setCategorie($categorie);
            $patient->setActive(1);

            $patient->setGroupe($stage);
            $em->persist($patient);
            $em->flush();
            $this->addFlash(
                    'notice', '  l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_show', array('id' => $patient->getId()));
        }
        if ($formrecherche->isSubmitted() && $formrecherche->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $patient->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $patient->setInscription($inscription);
            $categorie = $em->getRepository('AppBundle:PCategorie')->find($formnotpatient->get('cat')->getViewData());
            $patient->setCategorie($categorie);
            $patient->setActive(1);

            $patient->setGroupe($stage);
            $em->persist($patient);
            $em->flush();
            $this->addFlash(
                    'notice', '  l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_show', array('id' => $patient->getId()));
        }

        if ($formpatient->isSubmitted() && $formpatient->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $patient->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $patient->setInscription($inscription);
            $categorie = $em->getRepository('AppBundle:PCategorie')->find($formnotpatient->get('cat')->getViewData());
            $patient->setCategorie($categorie);
            $patient->setActive(1);

            $patient->setGroupe($stage);


            $em->persist($patient);
            $em->flush();
            $this->addFlash(
                    'notice', '  l\'enregistrement a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_show', array('id' => $patient->getId()));
        }

        return $this->render('ppatient/new.html.twig', array(
                    'patient' => $patient,
                    'form1' => $formpatient->createView(),
                    'form2' => $formnotpatient->createView(),
                    'form3' => $formsimulation->createView(),
                    'form4' => $formrecherche->createView(),
        ));
    }

    /**
     *
     * @Route("/{id}",options = { "expose" = true } , name="patient_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, PPatient $patient) {
        $session = new Session();
        $session->set('patient', $patient);

//        dump( $session->get('patient'));die();
        $deleteForm = $this->createDeleteForm($patient);

        $em = $this->getDoctrine()->getManager();
        $modules = $em->getRepository('AppBundle:PModule')->findAll();
        $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('userCreated' => $this->container->get('security.token_storage')->getToken()->getUser()));

        $categories = $em->getRepository('AppBundle:PCategorie')->findAll();
        $enc_form = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $enc_form->handleRequest($request);

        if ($enc_form->isSubmitted() && $enc_form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('patient_show', array('id' => $patient->getId()));
        } else {
            return $this->redirectToRoute('processus_rubrique', array('id' => 1));
        }


        return $this->render('ppatient/show.html.twig', array(
                    'modules' => $modules,
                    'categories' => $categories,
                    'patients' => $patients,
                    'patient' => $patient,
                    'delete_form' => $deleteForm->createView(),
                    'encform' => $enc_form->createView(),
        ));
    }

    /**
     *
     * @Route("/rub/{id}",options = { "expose" = true } ,name="processus_rubrique")
     * @Method({"GET", "POST"})
     */
    public function processusParRubriqueAction(Request $request, $id) {
        $session = new Session();
        $processus = new TProcessus();
//        dump( $session->get('patient'));die();
         libxml_use_internal_errors(true);

        $em = $this->getDoctrine()->getManager();
        $rubri = $em->getRepository('AppBundle:PRubrique')->find($id);
        $modules = $em->getRepository('AppBundle:PModule')->findAll();
        //  $patients = $em->getRepository('AppBundle:PPatient')->findByInscription(array('userCreated' => $this->container->get('security.token_storage')->getToken()->getUser()));
//$session->get('stage');
        //$patients = $em->getRepository('AppBundle:PPatient')->findByInscription($session->get('inscription')->getId());
 $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $session->get('inscription'), 'groupe' => $session->get('stage')));
        $categories = $em->getRepository('AppBundle:PCategorie')->findAll();
        $patient = $em->getRepository('AppBundle:PPatient')->find($session->get('patient')->getId());

        $form = $this->createForm('AppBundle\Form\TProcessusType', $processus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
             $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);
             $doc->loadHTML(mb_convert_encoding($processus->getText(), 'HTML-ENTITIES', 'UTF-8'));
           
             foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $data = "";
                if (strpos($img, 'data:image') !== false) {
                    // die('xxxxxxxxxxxxx');
                    $img = str_replace('data:image/gif;base64,', '', $img);
                    $img = str_replace('data:image/png;base64,', '', $img);
                    $img = str_replace('data:image/jpeg;base64,', '', $img);
                    $data = base64_decode($img);
                } else {

                    $files = $img;
                    $file_headers = @get_headers($files);
                    if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'http/1.1 403 forbidden') {
                        //$exists = false;
                    } else {
                        $data = file_get_contents($img);
                    }




                    // copy($file, 'images/bar/test' . $var . '.png');
                    // delete unlink($file);
                }
                $var = uniqid();

                $file = 'images/bar/test' . $var . '.png';
                 $file2 = '../../../images/bar/test' . $var . '.png';
                $success = file_put_contents($file, $data);
                $image->setAttribute("src", $file2);
            }
              $doc->removeChild($doc->doctype);
               $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
            $processus->setText($content);
             
             $processus->setPatient($patient);
            $rub = $em->getRepository('AppBundle:PRubrique')->find($id);
            $processus->setRubrique($rub);
            $now = date_create('now');
            $processus->setCreated($now);
            $processus->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em->persist($processus);
            $em->flush();
            $this->addFlash(
                    'notice', ' l\'enregistrement a été effectué avec succès'
            );
            // $json_data = array('data' => 'L\'enregistrement est fait avec succés',);
            // return new Response(json_encode($json_data));
            return $this->redirectToRoute('processus_rubrique', array('id' => $id));
        }

        $textprocessus = $em->getRepository('AppBundle:TProcessus')->findBy(array('patient' => $session->get('patient')->getId(), 'rubrique' => $id));
//        dump($textprocessus);die();
        $enc_patient = $em->getRepository('AppBundle:PPatient')->find($session->get('patient')->getId());
        $enc_form = $this->createForm('AppBundle\Form\PPatientType', $enc_patient);
        $enc_form->handleRequest($request);

        if ($enc_form->isSubmitted() && $enc_form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($request->getUri());
//            return $this->redirectToRoute('patient_show', array('id' => $session->get('patient')->getId()));
        }

        return $this->render('ppatient/edit.html.twig', array(
                    'form' => $form->createView(),
                    'encform' => $enc_form->createView(),
                    'modules' => $modules,
                    'categories' => $categories,
                    'patients' => $patients,
                    'patient' => $patient,
                    'textprocessus' => $textprocessus,
                    'rubri' => $rubri
        ));
    }

    /**
     * 
     *
     * @Route("/procc/{id}",options = { "expose" = true } , name="proc_list")
     * @Method({"GET"})
     */
    public function procListAction($id) {

        $data = array();
        $em = $this->getDoctrine()->getManager();


        $textprocessus = $em->getRepository('AppBundle:TProcessus')->findBy(array('patient' => $session->get('patient')->getId(), 'rubrique' => $id));
        foreach ($textprocessus as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
            $nestedData[] = "<p> " . $value->getText() . "</p>";



            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    public function allTextAction($rub_id) {
        $session = new Session();

        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository('AppBundle:TProcessus')->findBy(array('patient' => $session->get('patient'), 'rubrique' => $rub_id));

        return $this->render('ppatient/show.html.twig', array(
                    'text' => $text,
        ));
    }

    /**
     * Displays a form to edit an existing patient entity.
     *
     * @Route("/{id}/edit",options = { "expose" = true } , name="patient_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PPatient $patient) {
        $deleteForm = $this->createDeleteForm($patient);
        $editForm = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $now = date_create('now');
            $patient->setUpdated($now);
            $patient->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                    'notice', 'la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_edit', array('id' => $patient->getId()));
        }

        return $this->render('ppatient/edit.html.twig', array(
                    'patient' => $patient,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

//    /**
//     * Deletes a patient entity.
//     *
//     * @Route("/{id}", name="patient_delete")
//     * @Method("DELETE")
//     */
//    public function deleteAction(Request $request, PPatient $patient)
//    {
//        $form = $this->createDeleteForm($patient);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($patient);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('patient_index');
//    }

    /**
     * Deletes a patient entity.
     *
     * @Route("/delete/{id}", options = { "expose" = true }  , name="patient_delete")
     *  @Method({"GET", "POST"})
     */
    public function deleteAction($id) {
        $json_data = array('ffff' => 'gffdg');
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository('AppBundle:PPatient')->findOneBy(['id' => $id]);
        $obP = $em->getRepository('AppBundle:TObjectifEtudiant')->findOneBy(['patients' => $patient]);
        $proc = $em->getRepository('AppBundle:TProcessus')->findOneBy(['patient' => $patient]);
        //dump($patient);die();

        if ($patient) {
            // var_dump($obP);
            //  var_dump($patient);
            // die();
            if ($proc) {
                $em->remove($proc);
                $em->flush();
            }
            if ($obP) {
                $em->remove($obP);
                $em->flush();
            }

            $em->remove($patient);
            $em->flush();
            $json_data = array(
                'data' => 'L\'enregistrement "' . $patient->getDesignation() . '"à été  supprimé avec succés',
                'e' => 2,
            );
        } else {
            $json_data = array(
                'data' => 'Suppression a echoué ',
                'e' => 1,
            );
        }

        return new Response(json_encode($json_data));
    }

//    /**
//     * Deletes a patient entity.
//     *
//     * @Route("/delete/{id}", options = { "expose" = true }  , name="patient_delete1")
//     *
//     */
//    public function deletePPatientAction($id) {
//
//        $em = $this->getDoctrine()->getManager();
//        $patient = $em->getRepository('AppBundle:PPatient')->find($id);
//
//        $em->remove($patient);
//        $em->flush();
//        $this->addFlash(
//                'notice', 'Suppression est fait avec succÃ©e'
//        );
//
//        return $this->redirectToRoute('patient_index');
//    }

    /**
     * Creates a form to delete a patient entity.
     *
     * @param PPatient $patient The patient entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PPatient $patient) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('patient_delete', array('id' => $patient->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * Deletes a patient entity.
     *
     * @Route("/objectif3/{id}", options = { "expose" = true }  , name="patient_objectif")
     *  @Method({"GET", "POST"})
     */
    public function objectif3Action($id) {
//    
        $session = new Session();

        $em = $this->getDoctrine()->getManager();
        $sInsStage = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('inscription' => $session->get('inscription'),'statut'=>0));
       // $sInsStage = $em->getRepository('AppBundle:SInsStg')->findOneByInscription($session->get('inscription'));
        $patient = $em->getRepository('AppBundle:PPatient')->findBy(array('id' => $session->get('patient')->getId()));
        $etuObj = $em->getRepository('AppBundle:TObjectifEtudiant')->findBy(array('patients' => $patient));
        $objniveau3;
        // dump($sInsStage); 

        if (!empty($sInsStage)) {

            //  dump($sInsStage->getGroupe()->getId());
            foreach ($sInsStage->getGroupe()->getStage() as $key => $module_stage) {

                // dump($module_stage);

                foreach ($module_stage->getObjNiv1() as $key2 => $objniv1) {

                    foreach ($objniv1->getObjNiv2() as $key3 => $objniv2) {
                        foreach ($objniv2->getObjNiv3() as $key4 => $objniv3)


                        //dump($objniv3);
                            $objniveau3[] = $objniv3;
                    }
                }
            }
        }






        //var_dump($objniveau3[0]->getDesignation());
        //  die();
        //return new Response('ppatient/objectif.html.twig', array('name' => $objniveau3));
        return $this->render('ppatient/objectif.html.twig', array('stage' => $sInsStage, 'obj' => $etuObj));
    }

    /**
     * Deletes a patient entity.
     *
     * @Route("/rapportmenue/", options = { "expose" = true }  , name="rapport_menue")
     *  @Method({"GET", "POST"})
     */
    public function RapportMenueAction() {
//    
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $patients = $em->getRepository('AppBundle:PPatient')->findOneBy(array('id' => $session->get('patient')->getId()));






        //var_dump($objniveau3[0]->getDesignation());
        //  die();
        //return new Response('ppatient/objectif.html.twig', array('name' => $objniveau3));
        return $this->render('ppatient/rap_menue.html.twig', array('patient' => $patients));
    }

    /**
     * @Route("/print/{id}",options = { "expose" = true } , name="etu_print")
     */
    public function printAction($id) {


        $em = $this->getDoctrine()->getManager();
        $session = new Session();
$html="";
        $patients = $em->getRepository('AppBundle:PPatient')->findOneBy(array('id' => $id, 'inscription' => $session->get('inscription')));
        /* if(isset($patients->getObjEtudiant())!=null)

          {
          $objectifs=$patients->getObjEtudiant();
          } */


        /*  foreach ($patients->getCategorie()->getRubrique() as $key => $row) 
          {


          var_dump($row->getDesignation());


          }
          die(); */

        if (!empty($patients)) {
            $html = $this->renderView('rapport/rapo.html.twig', array(
                'patient' => $patients,
            ));
        } else {

            $html = utf8_encode('interdit d\'accéder à ce patient');
        }

  $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
           $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));


        foreach ($doc->getElementsByTagName('img') as $key => $image) {


            $img = $image->getAttribute('src');
            $data = "";
            if (strpos($img, '../../../images/bar/') !== false) {
                // die('xxxxxxxxxxxxx');
                $img = str_replace('../../../images/bar/', 'images/bar/', $img);
            }



            $image->setAttribute("src", $img);
        }
        $doc->removeChild($doc->doctype);
        $content = $doc->saveHTML($doc);
        $content = str_replace('<html><body>', '', $content);
        $content = str_replace('</body></html>', '', $content);


        $mpdf = new \mPDF();
        $mpdf->SetTitle('RAPPORT DU DOSSIER AVEC OBJECTIF');
        $mpdf->WriteHTML($content);
        $mpdf->Output();
    }

    /**
     * @Route("/printSansObjectif/{id}",options = { "expose" = true } , name="sansObj_print")
     */
    public function printSansObjectifAction($id) {


        $em = $this->getDoctrine()->getManager();
        $session = new Session();
        $html="";

        $patients = $em->getRepository('AppBundle:PPatient')->findOneBy(array('id' => $id, 'inscription' => $session->get('inscription')));
        /* if(isset($patients->getObjEtudiant())!=null)

          {
          $objectifs=$patients->getObjEtudiant();
          } */


        /*  foreach ($patients->getCategorie()->getRubrique() as $key => $row) 
          {


          var_dump($row->getDesignation());


          }
          die(); */

        if (!empty($patients)) {
            $html = $this->renderView('rapport/sansOb.html.twig', array(
                'patient' => $patients,
            ));
        } else {

            $html = utf8_encode('interdit d\'accéder à ce patient');
        }
   $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);

        // $doc->loadHTML($value->getText());

        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
           foreach ($doc->getElementsByTagName('img') as $key => $image) {


            $img = $image->getAttribute('src');
            $data = "";
            if (strpos($img, '../../../images/bar/') !== false) {
                // die('xxxxxxxxxxxxx');
                $img = str_replace('../../../images/bar/', 'images/bar/', $img);
            }



            $image->setAttribute("src", $img);
        }
        $doc->removeChild($doc->doctype);
        $content = $doc->saveHTML($doc);
        $content = str_replace('<html><body>', '', $content);
        $content = str_replace('</body></html>', '', $content);



        $mpdf = new \mPDF();
        $mpdf->SetTitle('RAPPORT DU DOSSIER SANS OBJECTIF');
        $mpdf->WriteHTML($content);
        $mpdf->Output();
    }

    /**
     * Deletes a patient entity.
     *
     * @Route("/niv3/{id}", options = { "expose" = true }  , name="patient_niv3")
     *  @Method({"GET", "POST"})
     */
    public function objAction($id) {

        $session = new Session();
        //var_dump($session->get('patient'));
        //die();

        $json_data = array('data' => 'gffdg', 'e' => 3,);
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository('AppBundle:PPatient')->find($session->get('patient')->getId());
        $inscription = $em->getRepository('AppBundle:TInscription')->find($session->get('inscription')->getId());
        $etuObj = $em->getRepository('AppBundle:TObjectifEtudiant')->findOneBy(array('objNiveau3' => $id, 'patients' => $patient));

        //dump($patient);die();

        if (empty($etuObj)) {



            $objn = $em->getRepository('AppBundle:SObjniv3')->find($id);

            $objEtu = new TObjectifEtudiant();

            $objEtu->setPatients($patient);
            $objEtu->setInscription($inscription);
            $objEtu->setObjNiveau3($objn);

            //  var_dump($objEtu);
            //  die();

            $em = $this->getDoctrine()->getManager();
            $em->persist($objEtu);
            $em->flush();
            $json_data = array(
                'data' => 'l\'enregistrement a été effectué avec succès',
            );
        } else {



            $em->remove($etuObj);
            $em->flush();




            $json_data = array(
                'data' => 'L\'objectif  \'' . $etuObj->getObjNiveau3()->getDesignation() . ' \' est retirée avec succés',
            );
        }
        //$json_data = utf8_encode($json_data);
        return new Response(json_encode($json_data));
    }

    /**
     * @Route("/sansobj_rapports/{id}", name="sansObjectif_rapports")
     */
    public function RapportsSansObjectifAction($id) {
        $session = new Session();
        //  $session->get('inscription');
        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $session->get('inscription')->getId()));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $session->get('stage')->getId()));

        $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));

        // $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));

        $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
            'patients' => $patients,
            'inscription' => $inscription,
            'pic' => 'p'
        ));
           $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
        
        
          $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($doc->getElementsByTagName('img') as $key => $image) {


            $img = $image->getAttribute('src');
            $data = "";
            if (strpos($img, '../../../images/bar/') !== false) {
                // die('xxxxxxxxxxxxx');
                $img = str_replace('../../../images/bar/', 'images/bar/', $img);
            }



            $image->setAttribute("src", $img);
        }
          $doc->removeChild($doc->doctype);
        $content = $doc->saveHTML($doc);
        $content = str_replace('<html><body>', '', $content);
        $content = str_replace('</body></html>', '', $content);
         error_reporting(0);
        $mpdf = new \mPDF();
        $mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
         $mpdf->WriteHTML($content);
        $mpdf->Output();
    }

    /**
     * @Route("/objectif_rapports/{id}", name="objectif_rapports")
     */
    public function etudiantRapportsAction($id) {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $session->get('inscription')->getId()));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $session->get('stage')->getId()));

        $patients = $em->getRepository('AppBundle:PPatient')->findOneBy(array('inscription' => $inscription, 'groupe' => $stage));
        // $objectif = $em->getRepository('AppBundle:TObjectifEtudiant')->findBy(array('inscription' => $inscription));
        // $qb = $em->createQueryBuilder() ->select('u') ->distinct() ->from('TObjectifEtudiant', 'objNiveau3');

        $patients2 = $em->getRepository('AppBundle:PPatient')->GetPatients2($inscription->getId(), $stage->getId());





        // $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));

         $html = $this->renderView('rapport/etudiant1.html.twig', array(
          'patient' => $patients,
          'inscription' => $inscription,
          'res'=>$patients2,
          )); 
    /*   return $this->render('rapport/etudiant1.html.twig', array(
                    'patient' => $patients,
                    'inscription' => $inscription,
                    'res' => $patients2,
        ));*/
 error_reporting(0);

         $mpdf = new \mPDF();
        $mpdf->SetDisplayMode('fullwidth');
       
          $mpdf->SetTitle('RAPPORT GLOBAL AVEC OBJECTIF');
          $mpdf->WriteHTML($html);
          error_reporting(0);
          $mpdf->Output(); 
          //ob_end_flush();
          return 1;
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/editproc/{id}", options = { "expose" = true }  , name="editproc")
     * @Method("GET")
     */
    public function editpAction($id) {
        $em = $this->getDoctrine()->getManager();
        $processus = $em->getRepository('AppBundle:TProcessus')->findOneBy(array('id' => $id));
        return $this->render('ppatient/editP.html.twig', array('processus' => $processus)
        );
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/editProcessus/", options = { "expose" = true }  , name="edit_processus1")
     *  @Method({"GET", "POST"})
     */
    public function ProcessusEditAction(Request $request) {
        $json_data = array('data' => 'gffdg');
         $dt=$request->request->get('frmPro', 'prcTxt');
         $dt1=$request->request->get('frmPro', 'prcTxt');
        $em = $this->getDoctrine()->getManager();

        $processus = $em->getRepository('AppBundle:TProcessus')->findOneBy(array('id' => $dt['procId']));

        if (!empty($processus)) {
            
              $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);
            
              $doc->loadHTML(mb_convert_encoding($dt['prcTxt'], 'HTML-ENTITIES', 'UTF-8'));
            
                    foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                //$d=strpos($img,'sed');
                //dump($d);
                //dump($img);
                // die();
                if (strpos($img, '../../../images/bar/') !== 0) {
                    $data = "";
                    if (strpos($img, 'data:image') !== false) {
                        // die('xxxxxxxxxxxxx');
                        $img = str_replace('data:image/gif;base64,', '', $img);
                        $img = str_replace('data:image/png;base64,', '', $img);
                        $img = str_replace('data:image/jpeg;base64,', '', $img);
                        $data = base64_decode($img);
                    } else {

                        $files = $img;
                        $file_headers = @get_headers($files);
                        if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'http/1.1 403 forbidden') {
                            //$exists = false;
                        } else {
                            $data = file_get_contents($img);
                        }




                        // copy($file, 'images/bar/test' . $var . '.png');
                        // delete unlink($file);
                    }
                    $var = uniqid();

                    $file = 'images/bar/test' . $var . '.png';
                    $file2 = '../../../images/bar/test' . $var . '.png';
                    $success = file_put_contents($file, $data);
                    $image->setAttribute("src", $file2);
                }
            }
            
              $doc->removeChild($doc->doctype);
              
                $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
            $processus->setText($content);
            
            
           
            $em->flush();
            $this->addFlash(
                    'notice', ' la modification a été effectué avec succès'
            );
            $json_data = array('data' => 1,);
        } else {
            $json_data = array('data' => 0,);
        }



        return new Response(json_encode($json_data));
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/supproc/{id}", options = { "expose" = true }  , name="supproc")
     * @Method("GET")
     */
    public function SuppAction($id) {
        $json_data = array('data' => 'gffdg', 'e' => 3,);
        $em = $this->getDoctrine()->getManager();
        $processus = $em->getRepository('AppBundle:TProcessus')->findOneBy(array('id' => $id));
        if (!empty($processus)) {
            $em->remove($processus);
            $em->flush();
            $json_data = array('data' => 1,);
        } else {
            $json_data = array('data' => 0,);
        }


        return new Response(json_encode($json_data));
    }

    /**
     * 
     *
     * @Route("/disable/{id}/{etat}" ,options = { "expose" = true } , name="patient_disable")
     * 
     */
    public function DisableAction($id, $etat) {
        $text = "";
        if ($etat == 1) {
            $etat = 0;
            $text = "désactivé";
        } else {
            $etat = 1;
            $text = "activé";
        }

        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository('AppBundle:PPatient')->find($id);

        $patient->setActive($etat);
        $em->flush();

        $json_data = array(
            'data' => 'Le champ a été ' . $text . ' avec succés',
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Displays a form to edit an existing responsable entity.
     *
     * @Route("/{id}/edit2", name="patient_edit")
     * @Method({"GET", "POST"})
     */
    public function PatienteditAction(Request $request, PPatient $patient) {
        $deleteForm = $this->createDeleteForm($patient);
        $editForm = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $editForm->handleRequest($request);

        $editForm = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $editForm->handleRequest($request);
        $editForm2 = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $editForm2->handleRequest($request);
        $editForm3 = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $editForm3->handleRequest($request);
        $editForm4 = $this->createForm('AppBundle\Form\PPatientType', $patient);
        $editForm4->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $now = date_create('now');
            $patient->setUpdated($now);
            $patient->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', 'la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_index');
        }
        /* if ($editForm->isSubmitted() && $editForm->isValid()) {

          //            dump($formnotpatient->get('cat')->getViewData());
          //            die();
          $em = $this->getDoctrine()->getManager();
          $now = date_create('now');
          $patient->setCreated($now);
          $patient->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());

          $categorie = $em->getRepository('AppBundle:PCategorie')->find($editForm->get('cat')->getViewData());
          $patient->setCategorie($categorie);

          $this->getDoctrine()->getManager()->flush();

          // Set a flash message
          $this->addFlash(
          'notice', 'Modification est fait avec succés'
          );

          return $this->redirectToRoute('patient_index');
          } */
        if ($editForm2->isSubmitted() && $editForm2->isValid()) {

//            dump($formnotpatient->get('cat')->getViewData());
//            die();
            $em = $this->getDoctrine()->getManager();
            $now = date_create('now');
            $patient->setUpdated($now);
            $patient->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());

            $categorie = $em->getRepository('AppBundle:PCategorie')->find($editForm2->get('cat')->getViewData());
            $patient->setCategorie($categorie);

            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', 'Modification est fait avec succés'
            );

            return $this->redirectToRoute('patient_index');
        }
        if ($editForm3->isSubmitted() && $editForm3->isValid()) {

//            dump($formnotpatient->get('cat')->getViewData());
//            die();
            $em = $this->getDoctrine()->getManager();
            $now = date_create('now');
            $patient->setUpdated($now);
            $patient->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());

            $categorie = $em->getRepository('AppBundle:PCategorie')->find($editForm3->get('cat')->getViewData());
            $patient->setCategorie($categorie);

            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', 'la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_index');
        }
        if ($editForm4->isSubmitted() && $editForm4->isValid()) {

//            dump($formnotpatient->get('cat')->getViewData());
//            die();
            $em = $this->getDoctrine()->getManager();
            $now = date_create('now');
            $patient->setUpdated($now);
            $patient->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            //$editForm3->get('cat') = $request->get('categorie');
            $categorie = $em->getRepository('AppBundle:PCategorie')->find($request->get('categorie'));
            //  var_dump($categorie);
            //die();
            $categorie = $em->getRepository('AppBundle:PCategorie')->findOneBy(array('id' => $request->get('categorie')));

            $patient->setCategorie($categorie);

            $this->getDoctrine()->getManager()->flush();

            // Set a flash message
            $this->addFlash(
                    'notice', 'la modification a été effectué avec succès'
            );

            return $this->redirectToRoute('patient_index');
        }

        return $this->render('ppatient/mod.html.twig', array(
                    'patient' => $patient,
                    'edit_form' => $editForm->createView(),
                    //'edit_form1' => $editForm1->createView(),
                    'edit_form2' => $editForm2->createView(),
                    'edit_form3' => $editForm3->createView(),
                    'edit_form4' => $editForm4->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }
           /**
     * @Route("/sansobj_rapports_simulation/{id}", name="sansObjectif_rapports_simulation")
     */
    public function RapportsSansObjectifSimCAction($id) {
        $session = new Session();
        //  $session->get('inscription');
        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $session->get('inscription')->getId()));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $session->get('stage')->getId()));
          $clinique = $em->getRepository('AppBundle:PModule')->findOneBy(array('id' => 2));
 $patients = $em->getRepository('AppBundle:PPatient')->GetPatientsByModule($inscription,$stage,$clinique);
       // $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));

        // $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));

        $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
            'patients' => $patients,
            'inscription' => $inscription,
            'pic' => 'p'
        ));

    error_reporting(0);
            $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);

            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $data = "";
                if (strpos($img, '../../../images/bar/') !== false) {
                    // die('xxxxxxxxxxxxx');
                    $img = str_replace('../../../images/bar/', 'images/bar/', $img);
                }



                $image->setAttribute("src", $img);
            }
                   $doc->removeChild($doc->doctype);
          $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
             $mpdf = new \mPDF();
            $mpdf->SetDisplayMode('fullwidth');
            $mpdf->SetTitle('RAPPORT GLOBAL Simulation');
            $mpdf->setFooter('<center>page : {PAGENO}</center>');
            $mpdf->WriteHTML($content);
            $mpdf->Output();
  return 1;
        //$mpdf = new \mPDF();
        //$mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
        //$mpdf->WriteHTML($html);
       // $mpdf->Output();
    }
    
             /**
     * @Route("/sansobj_rapports_recherche/{id}", name="sansObjectif_rapports_recherche")
     */
    public function RapportsSansObjectifrechCAction($id) {
        $session = new Session();
        //  $session->get('inscription');
        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $session->get('inscription')->getId()));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $session->get('stage')->getId()));
          $clinique = $em->getRepository('AppBundle:PModule')->findOneBy(array('id' => 3));
       $patients = $em->getRepository('AppBundle:PPatient')->GetPatientsByModule($inscription,$stage,$clinique);
       // $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));

        // $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));

        $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
            'patients' => $patients,
            'inscription' => $inscription,
            'pic' => 'p'
        ));

               error_reporting(0);
            $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);

            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $data = "";
                if (strpos($img, '../../../images/bar/') !== false) {
                    // die('xxxxxxxxxxxxx');
                    $img = str_replace('../../../images/bar/', 'images/bar/', $img);
                }



                $image->setAttribute("src", $img);
            }
                   $doc->removeChild($doc->doctype);
          $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
             $mpdf = new \mPDF();
            $mpdf->SetDisplayMode('fullwidth');
            $mpdf->SetTitle('RAPPORT GLOBAL Recherche');
            $mpdf->setFooter('<center>page : {PAGENO}</center>');
            $mpdf->WriteHTML($content);
            $mpdf->Output();
  return 1;
        //$mpdf = new \mPDF();
        //$mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
        //$mpdf->WriteHTML($html);
      //  $mpdf->Output();
    }
         /**
     * @Route("/sansobj_rapports_clinique/", name="sansObjectif_rapports_clinique")
     */
    public function RapportsSansObjectifCAction() {
        $session = new Session();
        //  $session->get('inscription');
        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $session->get('inscription')->getId()));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $session->get('stage')->getId()));
          $clinique = $em->getRepository('AppBundle:PModule')->findOneBy(array('id' => 1));
 $patients = $em->getRepository('AppBundle:PPatient')->GetPatientsByModule($inscription,$stage,$clinique);
 
     //   $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));

        // $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));

        $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
            'patients' => $patients,
            'inscription' => $inscription,
            'pic' => 'p'
        ));
        
        
           error_reporting(0);
            $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);

            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $data = "";
                if (strpos($img, '../../../images/bar/') !== false) {
                    // die('xxxxxxxxxxxxx');
                    $img = str_replace('../../../images/bar/', 'images/bar/', $img);
                }



                $image->setAttribute("src", $img);
            }
                   $doc->removeChild($doc->doctype);
          $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
             $mpdf = new \mPDF();
            $mpdf->SetDisplayMode('fullwidth');
            $mpdf->SetTitle('RAPPORT GLOBAL Clinique');
            $mpdf->setFooter('<center>page : {PAGENO}</center>');
            $mpdf->WriteHTML($content);
            $mpdf->Output();



     //   $mpdf = new \mPDF();
       // $mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
        //$mpdf->WriteHTML($html);
        //$mpdf->Output();
        return 1;
    }

}
