<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unirest\Request as Req;

/**
 * Etudiant controller.
 *
 * @Route("api/")
 */
class ApiController extends Controller {

    /**
     * Displays api index
     *
     * @Route("index" ,name="api_index")
     * 
     */
    public function ApiIndexAction(Request $request) {
        
          $lien = 13;
        $li = 13;
        
        return $this->render('management/api.html.twig',['lien' => $lien,'li' => $li]);
    }

    /**
     * @Route("syn/etablissement" , options = { "expose" = true } , name="syn_ac_etablissement")
     */
    public function SynchnisationEtablissementAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_etablissement ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/etablissement/' . $max_id, $headers);
//
//$response->code;        // HTTP Status code
//$response->headers;     // Headers
//$response->body;        // Parsed body
//$response->raw_body;    // Unparsed body
//dump($response->code);
//dump($response->headers);

        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_etablissement` (`id`, `code`, `designation`, `abreviation`, `statut`, `doyen`, `nature`, `date`, `active`) VALUES (:id, :code, :designation, :abreviation, :statut, :doyen, :nature, :date, :active);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', $value->id_etablissement);
                    $stmt->bindValue('code', $value->code);
                    $stmt->bindValue('designation', $value->designation);
                    $stmt->bindValue('abreviation', $value->abreviation);
                    $stmt->bindValue('statut', $value->statut);
                    $stmt->bindValue('doyen', $value->doyen);
                    $stmt->bindValue('nature', $value->nature);
                    $stmt->bindValue('nature', $value->nature);
                    $stmt->bindValue('date', $value->date);
                    $stmt->bindValue('active', $value->active);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/formation" , options = { "expose" = true } , name="syn_ac_formation")
     */
    public function SynchnisationFormationAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_formation ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
                              
        $response = Req::get($this->getParameter('api_link').'/api/formation/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                // dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_formation` (`id`, `ac_etablissement_id`,`code` ,`code_etablissement` ,`code_departement`,`designation`, `abreviation`, `nbr_annee`, `seuil`, `date`, `active`) VALUES (:id,:ac_etablissement_id,:code, :code_etablissement, :code_departement, :designation, :abreviation, :nbr_annee, :seuil, :date, :active);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', $value->id_formation);
                    $stmt->bindValue('ac_etablissement_id', $value->etablissement_id);
                    $stmt->bindValue('code', $value->code);
                    $stmt->bindValue('code_etablissement', $value->code_etablissement);
                    $stmt->bindValue('code_departement', isset($value->code_departement) ? $value->code_departement : NULL);
                    $stmt->bindValue('designation', $value->designation);
                    $stmt->bindValue('abreviation', $value->abreviation);
                    $stmt->bindValue('nbr_annee', $value->nbr_annee);
                    $stmt->bindValue('seuil', isset($value->seuil) ? $value->seuil : NULL);
                    $stmt->bindValue('date', $value->date);
                    $stmt->bindValue('active', $value->active);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/annee" , options = { "expose" = true } , name="syn_ac_annee")
     */
    public function SynchnisationAnneeAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_annee ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/annee/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                // dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_annee`(`id`, `code`, `code_etablissement`, `code_formation`, `code_promotion`, `designation`, `validation_academique`, `cloture_academique`, `ac_etablissement_id`, `ac_formation_id`) 
                            VALUES (:id, :code, :code_etablissement, :code_formation, :code_promotion, :designation, :validation_academique, :cloture_academique, :etablissement_id, :formation_id)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', $value->id);
                    $stmt->bindValue('code', $value->code);
                    $stmt->bindValue('code_etablissement', isset($value->code_etablissement) ? $value->code_etablissement : NULL);
                    $stmt->bindValue('code_formation', isset($value->code_formation) ? $value->code_formation : NULL);
                    $stmt->bindValue('code_promotion', isset($value->code_promotion) ? $value->code_promotion : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('validation_academique', isset($value->validation_academique) ? $value->validation_academique : NULL);
                    $stmt->bindValue('cloture_academique', isset($value->cloture_academique) ? $value->cloture_academique : NULL);
                    $stmt->bindValue('etablissement_id', isset($value->etablissement_id) ? $value->etablissement_id : NULL);
                    $stmt->bindValue('formation_id', isset($value->formation_id) ? $value->formation_id : NULL);


                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/promotion" , options = { "expose" = true } , name="syn_ac_promotion")
     */
    public function SynchnisationPromotionAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_promotion ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/promotion/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                //   dump($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_promotion` (`id`, `formation_id`,`code` ,`code_formation`,`designation`, `ordre`, `validation_academique`, `cloture_academique`, `active`) VALUES (:id, :formation_id,:code ,:code_formation,:designation, :ordre, :validation_academique, :cloture_academique, :active);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('formation_id', isset($value->formation_id) ? $value->formation_id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_formation', isset($value->code_formation) ? $value->code_formation : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('ordre', isset($value->ordre) ? $value->ordre : NULL);
                    $stmt->bindValue('validation_academique', isset($value->validation_academique) ? $value->validation_academique : NULL);
                    $stmt->bindValue('cloture_academique', isset($value->cloture_academique) ? $value->cloture_academique : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/semestre" , options = { "expose" = true } , name="syn_ac_semestre")
     */
    public function SynchnisationSemestreAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_semestre ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/semestre/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                //   dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_semestre` (`id`, `code` , `code_annee`  ,`promotion_id`,`code_promotion`,`designation`,  `validation_examen`, `cloture_examen`, `active`) VALUES (:id,:code ,:code_annee ,:promotion_id, :code_promotion, :designation,:validation_examen, :cloture_examen, :active);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_annee', isset($value->code_annee) ? $value->code_annee : NULL);
                    $stmt->bindValue('promotion_id', isset($value->promotion_id) ? $value->promotion_id : NULL);
                    $stmt->bindValue('code_promotion', isset($value->code_promotion) ? $value->code_promotion : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('validation_examen', isset($value->validation_examen) ? $value->validation_examen : NULL);
                    $stmt->bindValue('cloture_examen', isset($value->cloture_examen) ? $value->cloture_examen : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/module" , options = { "expose" = true } , name="syn_ac_module")
     */
    public function SynchnisationModuleAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_module ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/module/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                //   dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_module` (`id`, `code` , `code_semestre`  ,`semestre_id`,`designation`,  `couleur`, `coefficient`,`type`, `active`) VALUES (:id,:code ,:code_semestre ,:semestre_id,:designation,:couleur, :coefficient , :type, :active);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_semestre', isset($value->code_semestre) ? $value->code_semestre : NULL);
                    $stmt->bindValue('semestre_id', isset($value->semestre_id) ? $value->semestre_id : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('couleur', isset($value->couleur) ? $value->couleur : NULL);
                    $stmt->bindValue('coefficient', isset($value->coefficient) ? $value->coefficient : NULL);
                    $stmt->bindValue('type', isset($value->type) ? $value->type : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/element" , options = { "expose" = true } , name="syn_ac_element")
     */
    public function SynchnisationElementAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_element ";

        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/element/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_element` (`id`, `code` , `module_id`  ,`code_module`,`designation`,`type`,  `nature`, `active`,`coefficient`, `coefficient_epreuve`,`cours_document`) VALUES (:id, :code, :module_id ,:code_module,:designation,:type,  :nature, :active,:coefficient, :coefficient_epreuve,:cours_document);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('module_id', isset($value->module_id) ? $value->module_id : NULL);
                    $stmt->bindValue('code_module', isset($value->code_module) ? $value->code_module : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('type', isset($value->type) ? $value->type : NULL);
                    $stmt->bindValue('nature', isset($value->nature) ? $value->nature : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $stmt->bindValue('coefficient', isset($value->coefficient) ? $value->coefficient : NULL);
                    $stmt->bindValue('coefficient_epreuve', isset($value->coefficient_epreuve) ? $value->coefficient_epreuve : NULL);
                    $stmt->bindValue('cours_document', isset($value->cours_document) ? $value->cours_document : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/enseignant" , options = { "expose" = true } , name="syn_p_enseignant")
     */
    public function SynchnisationEnseignantAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from p_enseignant ";

        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/enseignant/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `p_enseignant` (`id`, `code` , `qualite`  ,`nom`,`prenom`,`code_grade`,  `nature`, `date`) VALUES (:id, :code , :qualite  ,:nom,:prenom,:code_grade,  :nature, :date);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id_enseignant) ? $value->id_enseignant : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('qualite', isset($value->qualite) ? $value->qualite : NULL);
                    $stmt->bindValue('nom', isset($value->nom) ? $value->nom : NULL);
                    $stmt->bindValue('prenom', isset($value->prenom) ? $value->prenom : NULL);
                    $stmt->bindValue('code_grade', isset($value->code_grade) ? $value->code_grade : NULL);
                    $stmt->bindValue('nature', isset($value->nature) ? $value->nature : NULL);
                    $stmt->bindValue('date', isset($value->date) ? $value->date : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/natureepreuve" , options = { "expose" = true } , name="syn_pr_nature_epreuve")
     */
    public function SynchnisationNatureEpreuveAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from pr_nature_epreuve ";

        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/natureepreuve/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `pr_nature_epreuve` (`id`, `code` , `designation`  ,`abreviation`,`type`,`nature`,  `examen`, `mapped`) VALUES (:id, :code , :designation  ,:abreviation,:type,:nature, :examen ,:mapped);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id_nature_epreuve) ? $value->id_nature_epreuve : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('type', isset($value->type) ? $value->type : NULL);
                    $stmt->bindValue('nature', isset($value->nature) ? $value->nature : NULL);
                    $stmt->bindValue('examen', isset($value->examen) ? $value->examen : NULL);
                    $stmt->bindValue('mapped', isset($value->mapped) ? $value->mapped : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/epreuve" , options = { "expose" = true } , name="syn_ac_epreuve")
     */
    public function SynchnisationEpreuveAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ac_epreuve ";

        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/epreuve/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ac_epreuve`(`id`, `code`, `coefficient`, `date_epreuve`, `observation`, `anonymat`, `natureanonymat`, `desig_statut`, `statut`, `user`, `date_creation`, `active`, `id_nature_epreuve`, `ac_annee_id`, `ac_element_id`, `p_enseignant_id`) VALUES 
                                                    (:id, :code, :coefficient, :date_epreuve, :observation, :anonymat, :natureanonymat, :desig_statut, :statut, :user, :date_creation, :active, :id_nature_epreuve, :ac_annee_id, :ac_element_id, :p_enseignant_id)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('coefficient', isset($value->coefficient) ? $value->coefficient : NULL);
                    $stmt->bindValue('date_epreuve', isset($value->date_epreuve) ? $value->date_epreuve : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('anonymat', isset($value->anonymat) ? $value->anonymat : NULL);
                    $stmt->bindValue('natureanonymat', isset($value->natureanonymat) ? $value->natureanonymat : NULL);
                    $stmt->bindValue('desig_statut', isset($value->desig_statut) ? $value->desig_statut : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $stmt->bindValue('id_nature_epreuve', isset($value->id_nature_epreuve) ? $value->id_nature_epreuve : NULL);
                    $stmt->bindValue('ac_annee_id', isset($value->ac_annee_id) ? $value->ac_annee_id : NULL);
                    $stmt->bindValue('ac_element_id', isset($value->ac_element_id) ? $value->ac_element_id : NULL);
                    $stmt->bindValue('p_enseignant_id', isset($value->p_enseignant_id) ? $value->p_enseignant_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/typebac" , options = { "expose" = true } , name="syn_x_type_bac")
     */
    public function SynchnisationTypeBacAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from x_type_bac ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/typebac/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                //   dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `x_type_bac`(`id`, `code`, `designation`)  VALUES (:id,:code ,:designation);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/modalite" , options = { "expose" = true } , name="syn_x_modalites")
     */
    public function SynchnisationModaliteAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from x_modalites ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/modalite/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                //   dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `x_modalites`(`id`, `code`, `designation`)  VALUES (:id,:code ,:designation);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/langue" , options = { "expose" = true } , name="syn_x_langues")
     */
    public function SynchnisationLangueAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from x_langues ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/langue/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                //   dump($response->body);

                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `x_langues`(`id`, `code`, `designation`, `abr`)  VALUES (:id,:code ,:designation ,:abr);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abr', isset($value->abr) ? $value->abr : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/filiere" , options = { "expose" = true } , name="syn_x_filiere")
     */
    public function SynchnisationFiliereAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from x_filiere ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/filiere/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `x_filiere`(`id`, `code`, `designation`, `abr`)  VALUES (:id,:code ,:designation ,:abr);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abr', isset($value->abr) ? $value->abr : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/banque" , options = { "expose" = true } , name="syn_x_banque")
     */
    public function SynchnisationBanqueAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from x_banque ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/banque/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `x_banque`(`id`, `code`, `designation`, `abr`)  VALUES (:id,:code ,:designation ,:abr);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abr', isset($value->abr) ? $value->abr : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/academie" , options = { "expose" = true } , name="syn_x_academie")
     */
    public function SynchnisationAcademieAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from x_academie ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/academie/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `x_academie`(`id`, `code`, `designation`, `abreviation`)  VALUES (:id,:code ,:designation ,:abreviation);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/naturedemande" , options = { "expose" = true } , name="syn_nature_demande")
     */
    public function SynchnisationNatureDemandeAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from nature_demande ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/naturedemande/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `nature_demande`(`id`, `code`, `designation`, `abreviation`, `concours`)  VALUES (:id,:code ,:designation ,:abreviation,:concours);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('concours', isset($value->concours) ? $value->concours : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/statut" , options = { "expose" = true } , name="syn_p_statut")
     */
    public function SynchnisationStatutAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from p_statut ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/statut/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `p_statut`(`id`, `code`, `designation`, `abreviation`, `table_0`, `phase_0`, `visible`, `visible_admission`, `next`, `active`, `annuler`)  VALUES (:id, :code, :designation, :abreviation, :table_0, :phase_0, :visible, :visible_admission,:next, :active, :annuler);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('table_0', isset($value->table_0) ? $value->table_0 : NULL);
                    $stmt->bindValue('phase_0', isset($value->phase_0) ? $value->phase_0 : NULL);
                    $stmt->bindValue('visible', isset($value->visible) ? $value->visible : NULL);
                    $stmt->bindValue('visible_admission', isset($value->visible_admission) ? $value->visible_admission : NULL);
                    $stmt->bindValue('next', isset($value->next) ? $value->next : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $stmt->bindValue('annuler', isset($value->annuler) ? $value->annuler : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/salle" , options = { "expose" = true } , name="syn_p_salles")
     */
    public function SynchnisationSallesAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from p_salles ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/salle/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `p_salles`(`id`, `code`, `designation`, `abreviation`, `code_etage`, `xIP`, `EtatPC`, `Attente`)  VALUES (:id,:code ,:designation ,:abreviation,:code_etage,:xIP ,:EtatPC,:Attente);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('code_etage', isset($value->code_etage) ? $value->code_etage : NULL);
                    $stmt->bindValue('xIP', isset($value->xIP) ? $value->xIP : NULL);
                    $stmt->bindValue('EtatPC', isset($value->EtatPC) ? $value->EtatPC : NULL);
                    $stmt->bindValue('Attente', isset($value->Attente) ? $value->Attente : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/organisme" , options = { "expose" = true } , name="syn_p_organisme")
     */
    public function SynchnisationOrganismeAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from p_organisme ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/organisme/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `p_organisme`(`id`, `code`, `designation`, `abreviation`, `active`)  VALUES (:id,:code ,:designation ,:abreviation,:active);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/frais" , options = { "expose" = true } , name="syn_p_frais")
     */
    public function SynchnisationFraisAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from p_frais ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/frais/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `p_frais`(`id`, `code`, `designation`, `categorie`, `montant`, `code_etablissement`, `code_formation`, `active`, `Rubrique`, `Nature`)  VALUES (:id,:code ,:designation ,:categorie, :montant, :code_etablissement, :code_formation, :active, :Rubrique, :Nature);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('categorie', isset($value->categorie) ? $value->categorie : NULL);
                    $stmt->bindValue('montant', isset($value->montant) ? $value->montant : NULL);
                    $stmt->bindValue('code_etablissement', isset($value->code_etablissement) ? $value->code_etablissement : NULL);
                    $stmt->bindValue('code_formation', isset($value->code_formation) ? $value->code_formation : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $stmt->bindValue('Rubrique', isset($value->Rubrique) ? $value->Rubrique : NULL);
                    $stmt->bindValue('Nature', isset($value->Nature) ? $value->Nature : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/estatut" , options = { "expose" = true } , name="syn_p_estatut")
     */
    public function SynchnisationEstatutAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from p_estatut ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/estatut/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `p_estatut`(`id`, `code`, `designation`, `abreviation`, `mine`, `maxs`, `active`, `definitif`)  VALUES (:id,:code ,:designation , :abreviation, :mine, :maxs, :active, :definitif);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('designation', isset($value->designation) ? $value->designation : NULL);
                    $stmt->bindValue('abreviation', isset($value->abreviation) ? $value->abreviation : NULL);
                    $stmt->bindValue('mine', isset($value->mine) ? $value->mine : NULL);
                    $stmt->bindValue('maxs', isset($value->maxs) ? $value->maxs : NULL);
                    $stmt->bindValue('active', isset($value->active) ? $value->active : NULL);
                    $stmt->bindValue('definitif', isset($value->definitif) ? $value->definitif : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/etudiant" , options = { "expose" = true } , name="syn_t_etudiant")
     */
    public function SynchnisationTEtudiantAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from t_etudiant ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/etudiant/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_etudiant`(
    `id`,
    `code`,
    `inscription_valide`,
    `nom`,
    `prenom`,
    `url_image`,
    `titre`,
    `date_naissance`,
    `lieu_naissance`,
    `sexe`,
    `st_famille`,
    `st_famille_parent`,
    `nationalite`,
    `cin`,
    `passeport`,
    `adresse`,
    `ville`,
    `tel1`,
    `tel2`,
    `tel3`,
    `mail1`,
    `mail2`,
    `nom_p`,
    `prenom_p`,
    `nationalite_p`,
    `profession_p`,
    `employe_p`,
    `categorie_p`,
    `tel_p`,
    `mail_p`,
    `salaire_p`,
    `nom_m`,
    `prenom_m`,
    `nationalite_m`,
    `profession_m`,
    `employe_m`,
    `categorie_m`,
    `tel_m`,
    `mail_m`,
    `salaire_m`,
    `cne`,
    `id_academie`,
    `etablissement`,
    `id_filiere`,
    `id_type_bac`,
    `annee_bac`,
    `moyenne_bac`,
    `concours_medbup`,
    `obs`,
    `langue_concours`,
    `categorie_preinscription`,
    `frais_preinscription`,
    `bourse`,
    `logement`,
    `parking`,
    `statut`,
    `actif`,
    `id_nature_demande`,
    `code_organisme`,
    `nombre_enfants`,
    `categorie_liste`,
    `admission_liste`,
    `tele_liste`,
    `statut_deliberation`,
    `date_creation`,
    `utilisateur`,
    `nature_demande_id`,
    `x_type_bac_id`,
    `x_academie_id`,
    `x_langues_id`,
    `x_filiere_id`
)
VALUES(:id,
:code,
:inscription_valide,
:nom,
:prenom,
:url_image,
:titre,
:date_naissance,
:lieu_naissance,
:sexe,
:st_famille,
:st_famille_parent,
:nationalite,
:cin,
:passeport,
:adresse,
:ville,
:tel1,
:tel2,
:tel3,
:mail1,
:mail2,
:nom_p,
:prenom_p,
:nationalite_p,
:profession_p,
:employe_p,
:categorie_p,
:tel_p,
:mail_p,
:salaire_p,
:nom_m,
:prenom_m,
:nationalite_m,
:profession_m,
:employe_m,
:categorie_m,
:tel_m,
:mail_m,
:salaire_m,
:cne,
:id_academie,
:etablissement,
:id_filiere,
:id_type_bac,
:annee_bac,
:moyenne_bac,
:concours_medbup,
:obs,
:langue_concours,
:categorie_preinscription,
:frais_preinscription,
:bourse,
:logement,
:parking,
:statut,
:actif,
:id_nature_demande,
:code_organisme,
:nombre_enfants,
:categorie_liste,
:admission_liste,
:tele_liste,
:statut_deliberation,
:date_creation,
:utilisateur,
:nature_demande_id,
:x_type_bac_id,
:x_academie_id,
:x_langues_id,
:x_filiere_id)";




                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('inscription_valide', isset($value->inscription_valide) ? $value->inscription_valide : NULL);
                    $stmt->bindValue('nom', isset($value->nom) ? $value->nom : NULL);
                    $stmt->bindValue('prenom', isset($value->prenom) ? $value->prenom : NULL);
                    $stmt->bindValue('url_image', isset($value->url_image) ? $value->url_image : NULL);
                    $stmt->bindValue('titre', isset($value->titre) ? $value->titre : NULL);
                    $stmt->bindValue('date_naissance', isset($value->date_naissance) ? $value->date_naissance : NULL);
                    $stmt->bindValue('lieu_naissance', isset($value->lieu_naissance) ? $value->lieu_naissance : NULL);
                    $stmt->bindValue('sexe', isset($value->sexe) ? $value->sexe : NULL);
                    $stmt->bindValue('st_famille', isset($value->st_famille) ? $value->st_famille : NULL);
                    $stmt->bindValue('st_famille_parent', isset($value->st_famille_parent) ? $value->st_famille_parent : NULL);
                    $stmt->bindValue('nationalite', isset($value->nationalite) ? $value->nationalite : NULL);
                    $stmt->bindValue('cin', isset($value->cin) ? $value->cin : NULL);
                    $stmt->bindValue('passeport', isset($value->passeport) ? $value->passeport : NULL);
                    $stmt->bindValue('adresse', isset($value->adresse) ? $value->adresse : NULL);
                    $stmt->bindValue('ville', isset($value->ville) ? $value->ville : NULL);
                    $stmt->bindValue('tel1', isset($value->tel1) ? $value->tel1 : NULL);
                    $stmt->bindValue('tel2', isset($value->tel2) ? $value->tel2 : NULL);
                    $stmt->bindValue('tel3', isset($value->tel3) ? $value->tel3 : NULL);
                    $stmt->bindValue('mail1', isset($value->mail1) ? $value->mail1 : NULL);
                    $stmt->bindValue('mail2', isset($value->mail2) ? $value->mail2 : NULL);
                    $stmt->bindValue('nom_p', isset($value->nom_p) ? $value->nom_p : NULL);
                    $stmt->bindValue('prenom_p', isset($value->prenom_p) ? $value->prenom_p : NULL);
                    $stmt->bindValue('nationalite_p', isset($value->nationalite_p) ? $value->nationalite_p : NULL);
                    $stmt->bindValue('profession_p', isset($value->profession_p) ? $value->profession_p : NULL);
                    $stmt->bindValue('employe_p', isset($value->employe_p) ? $value->employe_p : NULL);
                    $stmt->bindValue('categorie_p', isset($value->categorie_p) ? $value->categorie_p : NULL);
                    $stmt->bindValue('tel_p', isset($value->tel_p) ? $value->tel_p : NULL);
                    $stmt->bindValue('mail_p', isset($value->mail_p) ? $value->mail_p : NULL);
                    $stmt->bindValue('salaire_p', isset($value->salaire_p) ? $value->salaire_p : NULL);
                    $stmt->bindValue('nom_m', isset($value->nom_m) ? $value->nom_m : NULL);
                    $stmt->bindValue('prenom_m', isset($value->prenom_m) ? $value->prenom_m : NULL);
                    $stmt->bindValue('nationalite_m', isset($value->nationalite_m) ? $value->nationalite_m : NULL);
                    $stmt->bindValue('profession_m', isset($value->profession_m) ? $value->profession_m : NULL);
                    $stmt->bindValue('employe_m', isset($value->employe_m) ? $value->employe_m : NULL);
                    $stmt->bindValue('categorie_m', isset($value->categorie_m) ? $value->categorie_m : NULL);
                    $stmt->bindValue('tel_m', isset($value->tel_m) ? $value->tel_m : NULL);
                    $stmt->bindValue('mail_m', isset($value->mail_m) ? $value->mail_m : NULL);
                    $stmt->bindValue('salaire_m', isset($value->salaire_m) ? $value->salaire_m : NULL);
                    $stmt->bindValue('cne', isset($value->cne) ? $value->cne : NULL);
                    $stmt->bindValue('id_academie', isset($value->id_academie) ? $value->id_academie : NULL);
                    $stmt->bindValue('etablissement', isset($value->etablissement) ? $value->etablissement : NULL);
                    $stmt->bindValue('id_filiere', isset($value->id_filiere) ? $value->id_filiere : NULL);
                    $stmt->bindValue('id_type_bac', isset($value->id_type_bac) ? $value->id_type_bac : NULL);
                    $stmt->bindValue('annee_bac', isset($value->annee_bac) ? $value->annee_bac : NULL);
                    $stmt->bindValue('moyenne_bac', isset($value->moyenne_bac) ? $value->moyenne_bac : NULL);
                    $stmt->bindValue('concours_medbup', isset($value->concours_medbup) ? $value->concours_medbup : NULL);
                    $stmt->bindValue('obs', isset($value->obs) ? $value->obs : NULL);
                    $stmt->bindValue('langue_concours', isset($value->langue_concours) ? $value->langue_concours : NULL);
                    $stmt->bindValue('categorie_preinscription', isset($value->categorie_preinscription) ? $value->categorie_preinscription : NULL);
                    $stmt->bindValue('frais_preinscription', isset($value->frais_preinscription) ? $value->frais_preinscription : NULL);
                    $stmt->bindValue('bourse', isset($value->bourse) ? $value->bourse : NULL);
                    $stmt->bindValue('logement', isset($value->logement) ? $value->logement : NULL);
                    $stmt->bindValue('parking', isset($value->parking) ? $value->parking : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('actif', isset($value->actif) ? $value->actif : NULL);
                    $stmt->bindValue('id_nature_demande', isset($value->id_nature_demande) ? $value->id_nature_demande : NULL);
                    $stmt->bindValue('code_organisme', isset($value->code_organisme) ? $value->code_organisme : NULL);
                    $stmt->bindValue('nombre_enfants', isset($value->nombre_enfants) ? $value->nombre_enfants : NULL);
                    $stmt->bindValue('categorie_liste', isset($value->categorie_liste) ? $value->categorie_liste : NULL);
                    $stmt->bindValue('admission_liste', isset($value->admission_liste) ? $value->admission_liste : NULL);
                    $stmt->bindValue('tele_liste', isset($value->tele_liste) ? $value->tele_liste : NULL);
                    $stmt->bindValue('statut_deliberation', isset($value->statut_deliberation) ? $value->statut_deliberation : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('utilisateur', isset($value->utilisateur) ? $value->utilisateur : NULL);
                    $stmt->bindValue('nature_demande_id', isset($value->nature_demande_id) ? $value->nature_demande_id : NULL);
                    $stmt->bindValue('x_type_bac_id', isset($value->x_type_bac_id) ? $value->x_type_bac_id : NULL);
                    $stmt->bindValue('x_type_bac_id', isset($value->x_type_bac_id) ? $value->x_type_bac_id : NULL);
                    $stmt->bindValue('x_academie_id', isset($value->x_academie_id) ? $value->x_academie_id : NULL);
                    $stmt->bindValue('x_langues_id', isset($value->x_langues_id) ? $value->x_langues_id : NULL);
                    $stmt->bindValue('x_filiere_id', isset($value->x_filiere_id) ? $value->x_filiere_id : NULL);


                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/preinscription" , options = { "expose" = true } , name="syn_t_preinscription")
     */
    public function SynchnisationTPreinscriptionAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from t_preinscription ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/preinscription/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_preinscription`(
    `id`,
    `code`,
    `inscription_valide`,
    `rang_p`,
    `rang_s`,
    `statut_deliberation`,
    `date_creation`,
    `utilisateur`,
    `code_cab`,
    `code_etudiant`,
    `categorie_liste`,
    `admission_liste`,
    `tele_liste`,
    `t_etudiant_id`,
    `t_preinscriptioncab_id`,
    `p_statut_categorie_id`,
    `p_statut_admission_id`,
    `p_statut_tele_id`
)
VALUES(
:id,
:code,
:inscription_valide,
:rang_p,
:rang_s,
:statut_deliberation,
:date_creation,
:utilisateur,
:code_cab,
:code_etudiant,
:categorie_liste,
:admission_liste,
:tele_liste,
:t_etudiant_id,
:t_preinscriptioncab_id,
:p_statut_categorie_id,
:p_statut_admission_id,
:p_statut_tele_id
);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('inscription_valide', isset($value->inscription_valide) ? $value->inscription_valide : NULL);
                    $stmt->bindValue('rang_p', isset($value->rang_p) ? $value->rang_p : NULL);
                    $stmt->bindValue('rang_s', isset($value->rang_s) ? $value->rang_s : NULL);
                    $stmt->bindValue('statut_deliberation', isset($value->statut_deliberation) ? $value->statut_deliberation : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('utilisateur', isset($value->utilisateur) ? $value->utilisateur : NULL);
                    $stmt->bindValue('code_cab', isset($value->code_cab) ? $value->code_cab : NULL);
                    $stmt->bindValue('code_etudiant', isset($value->code_etudiant) ? $value->code_etudiant : NULL);
                    $stmt->bindValue('categorie_liste', isset($value->categorie_liste) ? $value->categorie_liste : NULL);
                    $stmt->bindValue('admission_liste', isset($value->admission_liste) ? $value->admission_liste : NULL);
                    $stmt->bindValue('tele_liste', isset($value->tele_liste) ? $value->tele_liste : NULL);
                    $stmt->bindValue('t_etudiant_id', isset($value->t_etudiant_id) ? $value->t_etudiant_id : NULL);
                    $stmt->bindValue('t_preinscriptioncab_id', NULL);
                    $stmt->bindValue('p_statut_categorie_id', isset($value->p_statut_categorie_id) ? $value->p_statut_categorie_id : NULL);
                    $stmt->bindValue('p_statut_admission_id', isset($value->p_statut_admission_id) ? $value->p_statut_admission_id : NULL);
                    $stmt->bindValue('p_statut_tele_id', isset($value->p_statut_tele_id) ? $value->p_statut_tele_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/admission" , options = { "expose" = true } , name="syn_t_admission")
     */
    public function SynchnisationTAdmissionAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from t_admission ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/admission/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_admission`(
    `id`,
    `code`,
    `code_preinscription`,
    `code_cab`,
    `code_organisme`,
    `statut`,
    `fermer`,
    `code_statut`,
    `date_creation`,
    `t_preinscription_id`,
    `p_organisme_id`,
    `p_statut_id`
)
VALUES(
    :id,
    :code,
    :code_preinscription,
    :code_cab,
    :code_organisme,
    :statut,
    :fermer,
    :code_statut,
    :date_creation,
    :t_preinscription_id,
    :p_organisme_id,
    :p_statut_id);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_preinscription', isset($value->code_preinscription) ? $value->code_preinscription : NULL);
                    $stmt->bindValue('code_cab', isset($value->code_cab) ? $value->code_cab : NULL);
                    $stmt->bindValue('code_organisme', isset($value->code_organisme) ? $value->code_organisme : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('fermer', isset($value->fermer) ? $value->fermer : NULL);
                    $stmt->bindValue('code_statut', isset($value->code_statut) ? $value->code_statut : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('t_preinscription_id', isset($value->t_preinscription_id) ? $value->t_preinscription_id : NULL);
                    $stmt->bindValue('p_organisme_id', isset($value->p_organisme_id) ? $value->p_organisme_id : NULL);
                    $stmt->bindValue('p_statut_id', isset($value->p_statut_id) ? $value->p_statut_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/inscription" , options = { "expose" = true } , name="syn_t_inscription")
     */
    public function SynchnisationTInscriptionAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from t_inscription ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/inscription/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_inscription`(
    `id`,
    `code`,
    `code_admission`,
    `statut`,
    `code_promotion`,
    `code_annee`,
    `code_statut`,
    `anonymat`,
    `anonymatrat`,
    `salle`,
    `emplacement`,
    `type_cand`,
    `t_admission_id`,
    `p_statut_id`,
    `ac_annee_id`,
    `ac_promotion_id`
)
VALUES(
 :id,
 :code,
 :code_admission,
 :statut,
 :code_promotion,
 :code_annee,
 :code_statut,
 :anonymat,
 :anonymatrat,
 :salle,
 :emplacement,
 :type_cand,
 :t_admission_id,
 :p_statut_id,
 :ac_annee_id,
 :ac_promotion_id
);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_admission', isset($value->code_admission) ? $value->code_admission : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('code_promotion', isset($value->code_promotion) ? $value->code_promotion : NULL);
                    $stmt->bindValue('code_annee', isset($value->code_annee) ? $value->code_annee : NULL);
                    $stmt->bindValue('code_statut', isset($value->code_statut) ? $value->code_statut : NULL);
                    $stmt->bindValue('anonymat', isset($value->anonymat) ? $value->anonymat : NULL);
                    $stmt->bindValue('anonymatrat', isset($value->anonymatrat) ? $value->anonymatrat : NULL);
                    $stmt->bindValue('salle', isset($value->salle) ? $value->salle : NULL);
                    $stmt->bindValue('emplacement', isset($value->emplacement) ? $value->emplacement : NULL);
                    $stmt->bindValue('type_cand', isset($value->type_cand) ? $value->type_cand : NULL);
                    $stmt->bindValue('t_admission_id', isset($value->t_admission_id) ? $value->t_admission_id : NULL);
                    $stmt->bindValue('p_statut_id', isset($value->p_statut_id) ? $value->p_statut_id : NULL);
                    $stmt->bindValue('ac_annee_id', isset($value->ac_annee_id) ? $value->ac_annee_id : NULL);
                    $stmt->bindValue('ac_promotion_id', isset($value->ac_promotion_id) ? $value->ac_promotion_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/operationcab" , options = { "expose" = true } , name="syn_t_operationcab")
     */
    public function SynchnisationTOperationcabAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from t_operationcab ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/operationcab/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_operationcab`(`id`, `code`, `categorie`, `code_organisme`, `observation`, `utilisateur`, `date_creation`, `code_anc`, `t_etudiant_id`, `ac_annee_id`) 
                                                  VALUES (:id, :code, :categorie, :code_organisme, :observation, :utilisateur, :date_creation, :code_anc, :t_etudiant_id, :ac_annee_id);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('categorie', isset($value->categorie) ? $value->categorie : NULL);
                    $stmt->bindValue('code_organisme', isset($value->code_organisme) ? $value->code_organisme : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('utilisateur', isset($value->utilisateur) ? $value->utilisateur : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('code_anc', isset($value->code_anc) ? $value->code_anc : NULL);
                    $stmt->bindValue('t_etudiant_id', isset($value->t_etudiant_id) ? $value->t_etudiant_id : NULL);
                    $stmt->bindValue('ac_annee_id', isset($value->ac_annee_id) ? $value->ac_annee_id : NULL);

                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/operationdet" , options = { "expose" = true } , name="syn_t_operationdet")
     */
    public function SynchnisationTOperationDetAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id_operationdet)  id from t_operationdet ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/operationdet/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_operationdet`(`id_operationdet`, `code`, `code_cab`, `t_operationcab_id`, `code_frais`, `montant`, `remise`, `descriptif`, `p_frais_id`)
                                                  VALUES (:id_operationdet, :code, :code_cab, :t_operationcab_id, :code_frais, :montant, :remise, :descriptif, :p_frais_id);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id_operationdet', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_cab', isset($value->code_cab) ? $value->code_cab : NULL);
                    $stmt->bindValue('t_operationcab_id', isset($value->t_operationcab_id) ? $value->t_operationcab_id : NULL);
                    $stmt->bindValue('code_frais', isset($value->code_frais) ? $value->code_frais : NULL);
                    $stmt->bindValue('montant', isset($value->montant) ? $value->montant : NULL);
                    $stmt->bindValue('remise', isset($value->remise) ? $value->remise : NULL);
                    $stmt->bindValue('descriptif', isset($value->descriptif) ? $value->descriptif : NULL);
                    $stmt->bindValue('p_frais_id', isset($value->p_frais_id) ? $value->p_frais_id : NULL);


                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/regelement" , options = { "expose" = true } , name="syn_t_regelement")
     */
    public function SynchnisationRegelementAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from t_regelement ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/regelement/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `t_regelement`(`id`, `code`, `montant`, `remise`, `impayer`, `date_reglement`, `periode`, `duree`, `debut`, `fin`, `sens`, `paiement`, `banque`, `reference`, `brd`, `date_creation`, `x_banque_id`, `x_modalite_id`, `t_operationcab_id`)
                                               VALUES (:id, :code, :montant, :remise, :impayer, :date_reglement, :periode, :duree, :debut,:fin,:sens,:paiement,:banque,:reference,:brd,:date_creation,:x_banque_id,:x_modalite_id,:t_operationcab_id);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('montant', isset($value->montant) ? $value->montant : NULL);
                    $stmt->bindValue('remise', isset($value->remise) ? $value->remise : NULL);
                    $stmt->bindValue('impayer', isset($value->impayer) ? $value->impayer : NULL);
                    $stmt->bindValue('date_reglement', isset($value->date_reglement) ? $value->date_reglement : NULL);
                    $stmt->bindValue('periode', isset($value->periode) ? $value->periode : NULL);
                    $stmt->bindValue('duree', isset($value->duree) ? $value->duree : NULL);
                    $stmt->bindValue('debut', isset($value->debut) ? $value->debut : NULL);
                    $stmt->bindValue('fin', isset($value->fin) ? $value->fin : NULL);
                    $stmt->bindValue('sens', isset($value->sens) ? $value->sens : NULL);
                    $stmt->bindValue('paiement', isset($value->paiement) ? $value->paiement : NULL);
                    $stmt->bindValue('banque', isset($value->banque) ? $value->banque : NULL);
                    $stmt->bindValue('reference', isset($value->reference) ? $value->reference : NULL);
                    $stmt->bindValue('brd', isset($value->brd) ? $value->brd : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('banque', isset($value->banque) ? $value->banque : NULL);
                    $stmt->bindValue('x_banque_id', isset($value->x_banque_id) ? $value->x_banque_id : NULL);
                    $stmt->bindValue('x_modalite_id', isset($value->x_modalite_id) ? $value->x_modalite_id : NULL);
                    $stmt->bindValue('t_operationcab_id', isset($value->t_operationcab_id) ? $value->t_operationcab_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/gnote" , options = { "expose" = true } , name="syn_ex_gnotes")
     */
    public function SynchnisationExGnotesAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ex_gnotes ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/gnote/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ex_gnotes`(`id`, `statut`, `note`, `user`, `date_creation`, `anonymat`, `absent`, `observation`, `ac_epreuve_id`, `t_inscription_id`) 
                            VALUES                 (:id, :statut,:note, :user, :date_creation, :anonymat, :absent, :observation, :ac_epreuve_id, :t_inscription_id);";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('note', isset($value->note) ? $value->note : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('anonymat', isset($value->anonymat) ? $value->anonymat : NULL);
                    $stmt->bindValue('absent', isset($value->absent) ? $value->absent : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('ac_epreuve_id', isset($value->ac_epreuve_id) ? $value->ac_epreuve_id : NULL);
                    $stmt->bindValue('t_inscription_id', isset($value->t_inscription_id) ? $value->t_inscription_id : NULL);

                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/enote" , options = { "expose" = true } , name="syn_ex_enotes")
     */
    public function SynchnisationExEnotesAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ex_enotes ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/enote/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ex_enotes`(`id`, `m_cc`, `m_tp`, `m_ef`, `cc_r`, `tp_r`, `ef_r`, `ex`, `note`, `note_ini`, `note_rat`, `note_rachat`, `flag`, `statut`, `observation`, `user`, `date_creation`, `cc_rachat`, `tp_rachat`, `ef_rachat`, `statut_s1`, `statut_s2`, `statut_def`, `ac_element_id`, `t_inscription_id`) 
                            VALUES (:id, :m_cc, :m_tp, :m_ef, :cc_r, :tp_r, :ef_r, :ex,:note,:note_ini, :note_rat, :note_rachat, :flag, :statut, :observation, :user, :date_creation, :cc_rachat, :tp_rachat, :ef_rachat, :statut_s1, :statut_s2, :statut_def, :ac_element_id, :t_inscription_id)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('m_cc', isset($value->m_cc) ? $value->m_cc : NULL);
                    $stmt->bindValue('m_tp', isset($value->m_tp) ? $value->m_tp : NULL);
                    $stmt->bindValue('m_ef', isset($value->m_ef) ? $value->m_ef : NULL);
                    $stmt->bindValue('cc_r', isset($value->cc_r) ? $value->cc_r : NULL);
                    $stmt->bindValue('tp_r', isset($value->tp_r) ? $value->tp_r : NULL);
                    $stmt->bindValue('ef_r', isset($value->ef_r) ? $value->ef_r : NULL);
                    $stmt->bindValue('ex', isset($value->ex) ? $value->ex : NULL);
                    $stmt->bindValue('note', isset($value->note) ? $value->note : NULL);
                    $stmt->bindValue('note_ini', isset($value->note_ini) ? $value->note_ini : NULL);
                    $stmt->bindValue('note_rat', isset($value->note_rat) ? $value->note_rat : NULL);
                    $stmt->bindValue('note_rachat', isset($value->note_rachat) ? $value->note_rachat : NULL);
                    $stmt->bindValue('flag', isset($value->flag) ? $value->flag : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('cc_rachat', isset($value->cc_rachat) ? $value->cc_rachat : NULL);
                    $stmt->bindValue('tp_rachat', isset($value->tp_rachat) ? $value->tp_rachat : NULL);
                    $stmt->bindValue('ef_rachat', isset($value->ef_rachat) ? $value->ef_rachat : NULL);
                    $stmt->bindValue('statut_s1', isset($value->statut_s1) ? $value->statut_s1 : NULL);
                    $stmt->bindValue('statut_s2', isset($value->statut_s2) ? $value->statut_s2 : NULL);
                    $stmt->bindValue('statut_def', isset($value->statut_def) ? $value->statut_def : NULL);
                    $stmt->bindValue('ac_element_id', isset($value->ac_element_id) ? $value->ac_element_id : NULL);
                    $stmt->bindValue('t_inscription_id', isset($value->t_inscription_id) ? $value->t_inscription_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/mnote" , options = { "expose" = true } , name="syn_ex_mnotes")
     */
    public function SynchnisationExMnotesAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ex_mnotes ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/mnote/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ex_mnotes`(`id`, `t_inscription_id`, `ac_module_id`, `note`, `note_ini`, `note_rat`, `note_rachat`, `flag`, `statut`, `observation`, `user`, `date_creation`, `statut_s2`, `statut_def`, `statut_aff`, `observation_delib`)
                                          VALUES (:id, :t_inscription_id, :ac_module_id, :note, :note_ini, :note_rat, :note_rachat, :flag, :statut, :observation, :user, :date_creation, :statut_s2, :statut_def, :statut_aff, :observation_delib)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('t_inscription_id', isset($value->t_inscription_id) ? $value->t_inscription_id : NULL);
                    $stmt->bindValue('ac_module_id', isset($value->ac_module_id) ? $value->ac_module_id : NULL);
                    $stmt->bindValue('note', isset($value->note) ? $value->note : NULL);
                    $stmt->bindValue('note_ini', isset($value->note_ini) ? $value->note_ini : NULL);
                    $stmt->bindValue('note_rat', isset($value->note_rat) ? $value->note_rat : NULL);
                    $stmt->bindValue('note_rachat', isset($value->note_rachat) ? $value->note_rachat : NULL);
                    $stmt->bindValue('flag', isset($value->flag) ? $value->flag : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('statut_s2', isset($value->statut_s2) ? $value->statut_s2 : NULL);
                    $stmt->bindValue('statut_def', isset($value->statut_def) ? $value->statut_def : NULL);
                    $stmt->bindValue('statut_aff', isset($value->statut_aff) ? $value->statut_aff : NULL);
                    $stmt->bindValue('observation_delib', isset($value->observation_delib) ? $value->observation_delib : NULL);

                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/snote" , options = { "expose" = true } , name="syn_ex_snotes")
     */
    public function SynchnisationSxMnotesAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ex_snotes ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/snote/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ex_snotes`(`id`, `t_inscription_id`, `ac_semestre_id`, `note`, `note_sec`, `note_rachat`, `statut`, `observation`, `user`, `date_creation`, `statut_s2`, `statut_def`, `statut_aff`, `categorie`, `categorie_ad`, `categorie_aff`)
                            VALUES (:id, :t_inscription_id, :ac_semestre_id, :note, :note_sec, :note_rachat, :statut, :observation, :user, :date_creation, :statut_s2, :statut_def, :statut_aff, :categorie, :categorie_ad, :categorie_aff)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('t_inscription_id', isset($value->t_inscription_id) ? $value->t_inscription_id : NULL);
                    $stmt->bindValue('ac_semestre_id', isset($value->ac_semestre_id) ? $value->ac_semestre_id : NULL);
                    $stmt->bindValue('note', isset($value->note) ? $value->note : NULL);
                    $stmt->bindValue('note_sec', isset($value->note_sec) ? $value->note_sec : NULL);
                    $stmt->bindValue('note_rachat', isset($value->note_rachat) ? $value->note_rachat : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('statut_s2', isset($value->statut_s2) ? $value->statut_s2 : NULL);
                    $stmt->bindValue('statut_def', isset($value->statut_def) ? $value->statut_def : NULL);
                    $stmt->bindValue('statut_aff', isset($value->statut_aff) ? $value->statut_aff : NULL);
                    $stmt->bindValue('categorie', isset($value->categorie) ? $value->categorie : NULL);
                    $stmt->bindValue('categorie_ad', isset($value->categorie_ad) ? $value->categorie_ad : NULL);
                    $stmt->bindValue('categorie_aff', isset($value->categorie_aff) ? $value->categorie_aff : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/anote" , options = { "expose" = true } , name="syn_ex_anotes")
     */
    public function SynchnisationAxMnotesAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from ex_anotes ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/anote/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `ex_anotes`(`id`, `note`, `note_sec`, `statut`, `statut_s2`, `statut_def`, `observation`, `user`, `date_creation`, `ac_annee_id`, `t_inscription_id`)
                            VALUES (:id, :note, :note_sec, :statut, :statut_s2, :statut_def, :observation, :user, :date_creation, :ac_annee_id, :t_inscription_id)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('note', isset($value->note) ? $value->note : NULL);
                    $stmt->bindValue('note_sec', isset($value->note_sec) ? $value->note_sec : NULL);
                    $stmt->bindValue('statut', isset($value->statut) ? $value->statut : NULL);
                    $stmt->bindValue('statut_s2', isset($value->statut_s2) ? $value->statut_s2 : NULL);
                    $stmt->bindValue('statut_def', isset($value->statut_def) ? $value->statut_def : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('ac_annee_id', isset($value->ac_annee_id) ? $value->ac_annee_id : NULL);
                    $stmt->bindValue('t_inscription_id', isset($value->t_inscription_id) ? $value->t_inscription_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    /**
     * @Route("syn/programme" , options = { "expose" = true } , name="syn_pr_programmation")
     */
    public function SynchnisationPrProgrammationAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from pr_programmation ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/programme/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `pr_programmation`(
    `id`,
    `code`,
    `id_etablissement`,
    `id_formation`,
    `id_annee`,
    `id_promotion`,
    `id_semestre`,
    `id_module`,
    `id_element`,
    `volume`,
    `observation`,
    `date_creation`,
    `user`,
    `modified`,
    `regroupe`,
    `categorie`,
    `id_nature_epreuve`,
    `ac_element_id`,
    `ac_annee_id`
)
VALUES(
:id,
:code,
:id_etablissement,
:id_formation,
:id_annee,
:id_promotion,
:id_semestre,
:id_module,
:id_element,
:volume,
:observation,
:date_creation,
:user,
:modified,
:regroupe,
:categorie,
:id_nature_epreuve,
:ac_element_id,
:ac_annee_id
)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('id_etablissement', isset($value->id_etablissement) ? $value->id_etablissement : NULL);
                    $stmt->bindValue('id_formation', isset($value->id_formation) ? $value->id_formation : NULL);
                    $stmt->bindValue('id_annee', isset($value->id_annee) ? $value->id_annee : NULL);
                    $stmt->bindValue('id_promotion', isset($value->id_promotion) ? $value->id_promotion : NULL);
                    $stmt->bindValue('id_semestre', isset($value->id_semestre) ? $value->id_semestre : NULL);
                    $stmt->bindValue('id_module', isset($value->id_module) ? $value->id_module : NULL);
                    $stmt->bindValue('id_element', isset($value->id_element) ? $value->id_element : NULL);
                    $stmt->bindValue('volume', isset($value->volume) ? $value->volume : NULL);
                    $stmt->bindValue('observation', isset($value->observation) ? $value->observation : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('modified', isset($value->modified) ? $value->modified : NULL);
                    $stmt->bindValue('regroupe', isset($value->regroupe) ? $value->regroupe : NULL);
                    $stmt->bindValue('categorie', isset($value->categorie) ? $value->categorie : NULL);
                    $stmt->bindValue('id_nature_epreuve', isset($value->id_nature_epreuve) ? $value->id_nature_epreuve : NULL);
                    $stmt->bindValue('ac_element_id', isset($value->ac_element_id) ? $value->ac_element_id : NULL);
                    $stmt->bindValue('ac_annee_id', isset($value->ac_annee_id) ? $value->ac_annee_id : NULL);
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }

    
    
    
        /**
     * @Route("syn/emptime" , options = { "expose" = true } , name="syn_pl_emptime")
     */
    public function SynchnisationPlEmptimeAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from pl_emptime ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/emptime/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `pl_emptime`(
    `id`,
    `niv_1`,
    `niv_2`,
    `niv_3`,
    `code`,
    `code_type`,
    `description`,
    `code_prog`,
    `semaine`,
    `datemp`,
    `start`,
    `end`,
    `heur_db`,
    `heur_fin`,
    `code_salle`,
    `valider`,
    `generer`,
    `annuler`,
    `motif_anuler`,
    `color`,
    `idmodule`,
    `idelement`,
    `idnatureepreuve`,
    `user`,
    `date_creation`,
    `date_mod`,
    `user_mod`,
    `pr_programmation_id`,
    `p_salles_id`
)
VALUES(
:id,
:niv_1,
:niv_2,
:niv_3,
:code,
:code_type,
:description,
:code_prog,
:semaine,
:datemp,
:start,
:end,
:heur_db,
:heur_fin,
:code_salle,
:valider,
:generer,
:annuler,
:motif_anuler,
:color,
:idmodule,
:idelement,
:idnatureepreuve,
:user,
:date_creation,
:date_mod,
:user_mod,
:pr_programmation_id,
:p_salles_id
)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('niv_1', isset($value->niv_1) ? $value->niv_1 : NULL);
                    $stmt->bindValue('niv_2', isset($value->niv_2) ? $value->niv_2 : NULL);
                    $stmt->bindValue('niv_3', isset($value->niv_3) ? $value->niv_3 : NULL);
                    $stmt->bindValue('code', isset($value->code) ? $value->code : NULL);
                    $stmt->bindValue('code_type', isset($value->code_type) ? $value->code_type : NULL);
                    $stmt->bindValue('description', isset($value->description) ? $value->description : NULL);
                    $stmt->bindValue('code_prog', isset($value->code_prog) ? $value->code_prog : NULL);
                    $stmt->bindValue('semaine', isset($value->semaine) ? $value->semaine : NULL);
                    $stmt->bindValue('datemp', isset($value->datemp) ? $value->datemp : NULL);
                    $stmt->bindValue('start', isset($value->start) ? $value->start : NULL);
                    $stmt->bindValue('end', isset($value->end) ? $value->end : NULL);
                    $stmt->bindValue('heur_db', isset($value->heur_db) ? $value->heur_db : NULL);
                    $stmt->bindValue('heur_fin', isset($value->heur_fin) ? $value->heur_fin : NULL);
                    $stmt->bindValue('code_salle', isset($value->code_salle) ? $value->code_salle : NULL);
                    $stmt->bindValue('valider', isset($value->valider) ? $value->valider : NULL);
                    $stmt->bindValue('generer', isset($value->generer) ? $value->generer : NULL);
                    $stmt->bindValue('annuler', isset($value->annuler) ? $value->annuler : NULL);
                    $stmt->bindValue('motif_anuler', isset($value->motif_anuler) ? $value->motif_anuler : NULL);
                    $stmt->bindValue('color', isset($value->color) ? $value->color : NULL);
                    $stmt->bindValue('idmodule', isset($value->idmodule) ? $value->idmodule : NULL);
                    $stmt->bindValue('idelement', isset($value->idelement) ? $value->idelement : NULL);
                    $stmt->bindValue('idnatureepreuve', isset($value->idnatureepreuve) ? $value->idnatureepreuve : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('date_mod', isset($value->date_mod) ? $value->date_mod : NULL);
                    $stmt->bindValue('user_mod', isset($value->user_mod) ? $value->user_mod : NULL);
                    $stmt->bindValue('pr_programmation_id', isset($value->pr_programmation_id) ? $value->pr_programmation_id : NULL);
                    $stmt->bindValue('p_salles_id', isset($value->p_salles_id) ? $value->p_salles_id : NULL);

                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }
    
    
    
    
    
            /**
     * @Route("syn/emptimens" , options = { "expose" = true } , name="syn_pl_emptimens")
     */
    public function SynchnisationPlEmptimensAction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from pl_emptimens ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/emptimens/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `pl_emptimens`(
    `id`,
    `code_seance`,
    `code_enseignant`,
    `generer`,
    `date_creation`,
    `user`,
    `pl_emptime_id`,
    `p_enseignant_id`
)
VALUES(
:id,
:code_seance,
:code_enseignant,
:generer,
:date_creation,
:user,
:pl_emptime_id,
:p_enseignant_id
)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('code_seance', isset($value->code_seance) ? $value->code_seance : NULL);
                    $stmt->bindValue('code_enseignant', isset($value->code_enseignant) ? $value->code_enseignant : NULL);
                    $stmt->bindValue('generer', isset($value->generer) ? $value->generer : NULL);
                    $stmt->bindValue('date_creation', isset($value->date_creation) ? $value->date_creation : NULL);
                    $stmt->bindValue('user', isset($value->user) ? $value->user : NULL);
                    $stmt->bindValue('pl_emptime_id', isset($value->pl_emptime_id) ? $value->pl_emptime_id : NULL);
                    $stmt->bindValue('p_enseignant_id', isset($value->p_enseignant_id) ? $value->p_enseignant_id : NULL);
                  
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }
    
    
    
    
                /**
     * @Route("syn/absence" , options = { "expose" = true } , name="syn_xseance_absences")
     */
    public function SynchnisationXSeanceAbsences(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from xseance_absences ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/absence/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `xseance_absences`(
    `id`,
    `ID_Admission`,
    `ID_Seance`,
    `ID_User`,
    `Nom`,
    `Prenom`,
    `ID_Groupe_Stage`,
    `Date_Pointage`,
    `Heure_Pointage`,
    `Retard`,
    `Categorie`,
    `Categorie_Enseig`,
    `Justifier`,
    `ID_Justif`,
    `Motif`,
    `Comptabilise`,
    `Date_Sys`,
    `Obs`
)
VALUES(
:id,
:ID_Admission,
:ID_Seance,
:ID_User,
:Nom,
:Prenom,
:ID_Groupe_Stage,
:Date_Pointage,
:Heure_Pointage,
:Retard,
:Categorie,
:Categorie_Enseig,
:Justifier,
:ID_Justif,
:Motif,
:Comptabilise,
:Date_Sys,
:Obs
)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('ID_Admission', isset($value->ID_Admission) ? $value->ID_Admission : NULL);
                    $stmt->bindValue('ID_Seance', isset($value->ID_Seance) ? $value->ID_Seance : NULL);
                    $stmt->bindValue('ID_User', isset($value->ID_User) ? $value->ID_User : NULL);
                    $stmt->bindValue('Nom', isset($value->Nom) ? $value->Nom : NULL);
                    $stmt->bindValue('Prenom', isset($value->Prenom) ? $value->Prenom : NULL);
                    $stmt->bindValue('ID_Groupe_Stage', isset($value->ID_Groupe_Stage) ? $value->ID_Groupe_Stage : NULL);
                    $stmt->bindValue('Date_Pointage', isset($value->Date_Pointage) ? $value->Date_Pointage : NULL);
                    $stmt->bindValue('Heure_Pointage', isset($value->Heure_Pointage) ? $value->Heure_Pointage : NULL);
                    $stmt->bindValue('Retard', isset($value->Retard) ? $value->Retard : NULL);
                    $stmt->bindValue('Categorie', isset($value->Categorie) ? $value->Categorie : NULL);
                    $stmt->bindValue('Categorie_Enseig', isset($value->Categorie_Enseig) ? $value->Categorie_Enseig : NULL);
                    $stmt->bindValue('Justifier', isset($value->Justifier) ? $value->Justifier : NULL);
                    $stmt->bindValue('ID_Justif', isset($value->ID_Justif) ? $value->ID_Justif : NULL);
                    $stmt->bindValue('Motif', isset($value->Motif) ? $value->Motif : NULL);
                    $stmt->bindValue('Comptabilise', isset($value->Comptabilise) ? $value->Comptabilise : NULL);
                    $stmt->bindValue('Date_Sys', isset($value->Date_Sys) ? $value->Date_Sys : NULL);
                    $stmt->bindValue('Obs', isset($value->Obs) ? $value->Obs : NULL);
                    
                    
                  
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }
    
    
    
    
    
     /**
     * @Route("syn/sanction" , options = { "expose" = true } , name="syn_xseance_sanction")
     */
    public function SynchnisationXSeanceSanction(Request $request) {
        //selectionner le max id de notre base de données
        $sql = "select max(id)  id from xseance_sanction ";
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $max_id = 0;
        if ($result['id']) {
            $max_id = $result['id'];
        }
        $headers = array('Accept' => 'application/json');
        $response = Req::get($this->getParameter('api_link').'/api/sanction/' . $max_id, $headers);
        $detail = array('code' => 0, 'row' => 0);
        if ($response->code == 200) {
            $detail['code'] = 200;
            if ($response->body) {
                $detail['row'] = count($response->body);
                foreach ($response->body as $key => $value) {
                    $sql = "INSERT INTO `xseance_sanction`(
    `id`,
    `Tranche_Sem`,
    `ID_Promotion`,
    `ID_Semestre`,
    `ID_Admission`,
    `Nom`,
    `Num_S1`,
    `DateD_S1`,
    `DateF_S1`,
    `VHA_S1`,
    `VHR_S1`,
    `Note_S1`,
    `Tranche_S1`,
    `Num_S2`,
    `DateD_S2`,
    `DateF_S2`,
    `VHA_S2`,
    `VHR_S2`,
    `Note_S2`,
    `Tranche_S2`,
    `Num_S3`,
    `DateD_S3`,
    `DateF_S3`,
    `VHA_S3`,
    `VHR_S3`,
    `Note_S3`,
    `Tranche_S3`,
    `Num_S4`,
    `DateD_S4`,
    `DateF_S4`,
    `VHA_S4`,
    `VHR_S4`,
    `Note_S4`,
    `Tranche_S4`,
    `VHA_G1`,
    `VHR_G1`,
    `Note_G1`,
    `Tranche_G1`,
    `VHA_Autorise`,
    `VHA_G2`,
    `VHR_G2`,
    `Note_G2`,
    `Tranche_G2`,
    `Appliquer`,
    `Debut_App`,
    `Fin_App`,
    `Motif`,
    `ID_Annee`,
    `Annee_Lib`,
    `Prenom`,
    `Decision_S1`,
    `Decision_S2`,
    `Decision_S3`,
    `Decision_S4`,
    `Decision_G1`,
    `Decision_G2`
)
VALUES(
:id,
:Tranche_Sem,
:ID_Promotion,
:ID_Semestre,
:ID_Admission,
:Nom,
:Num_S1,
:DateD_S1,
:DateF_S1,
:VHA_S1,
:VHR_S1,
:Note_S1,
:Tranche_S1,
:Num_S2,
:DateD_S2,
:DateF_S2,
:VHA_S2,
:VHR_S2,
:Note_S2,
:Tranche_S2,
:Num_S3,
:DateD_S3,
:DateF_S3,
:VHA_S3,
:VHR_S3,
:Note_S3,
:Tranche_S3,
:Num_S4,
:DateD_S4,
:DateF_S4,
:VHA_S4,
:VHR_S4,
:Note_S4,
:Tranche_S4,
:VHA_G1,
:VHR_G1,
:Note_G1,
:Tranche_G1,
:VHA_Autorise,
:VHA_G2,
:VHR_G2,
:Note_G2,
:Tranche_G2,
:Appliquer,
:Debut_App,
:Fin_App,
:Motif,
:ID_Annee,
:Annee_Lib,
:Prenom,
:Decision_S1,
:Decision_S2,
:Decision_S3,
:Decision_S4,
:Decision_G1,
:Decision_G2
)";
                    
 
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('id', isset($value->id) ? $value->id : NULL);
                    $stmt->bindValue('Tranche_Sem', isset($value->Tranche_Sem) ? $value->Tranche_Sem : NULL);
                    $stmt->bindValue('ID_Promotion', isset($value->ID_Promotion) ? $value->ID_Promotion : NULL);
                    $stmt->bindValue('ID_Semestre', isset($value->ID_Semestre) ? $value->ID_Semestre : NULL);
                    $stmt->bindValue('ID_Admission', isset($value->ID_Admission) ? $value->ID_Admission : NULL);
                    $stmt->bindValue('Nom', isset($value->Nom) ? $value->Nom : NULL);
                    $stmt->bindValue('Num_S1', isset($value->Num_S1) ? $value->Num_S1 : NULL);
                    $stmt->bindValue('DateD_S1', isset($value->DateD_S1) ? $value->DateD_S1 : NULL);
                    $stmt->bindValue('DateF_S1', isset($value->DateF_S1) ? $value->DateF_S1 : NULL);
                    $stmt->bindValue('VHA_S1', isset($value->VHA_S1) ? $value->VHA_S1 : NULL);
                    $stmt->bindValue('VHR_S1', isset($value->VHR_S1) ? $value->VHR_S1 : NULL);
                    $stmt->bindValue('Note_S1', isset($value->Note_S1) ? $value->Note_S1 : NULL);
                    $stmt->bindValue('Tranche_S1', isset($value->Tranche_S1) ? $value->Tranche_S1 : NULL);
                    $stmt->bindValue('Num_S2', isset($value->Num_S2) ? $value->Num_S2 : NULL);
                    $stmt->bindValue('DateD_S2', isset($value->DateD_S2) ? $value->DateD_S2 : NULL);
                    $stmt->bindValue('DateF_S2', isset($value->DateF_S2) ? $value->DateF_S2 : NULL);
                    $stmt->bindValue('VHA_S2', isset($value->VHA_S2) ? $value->VHA_S2 : NULL);
                    $stmt->bindValue('VHR_S2', isset($value->VHR_S2) ? $value->VHR_S2 : NULL);
                    $stmt->bindValue('Note_S2', isset($value->Note_S2) ? $value->Note_S2 : NULL);
                    $stmt->bindValue('Tranche_S2', isset($value->Tranche_S2) ? $value->Tranche_S2 : NULL);
                    
                    $stmt->bindValue('Num_S3', isset($value->Num_S3) ? $value->Num_S3 : NULL);
                    $stmt->bindValue('DateD_S3', isset($value->DateD_S3) ? $value->DateD_S3 : NULL);
                    $stmt->bindValue('DateF_S3', isset($value->DateF_S3) ? $value->DateF_S3 : NULL);
                    $stmt->bindValue('VHA_S3', isset($value->VHA_S3) ? $value->VHA_S3 : NULL);
                    $stmt->bindValue('VHR_S3', isset($value->VHR_S3) ? $value->VHR_S3 : NULL);
                    $stmt->bindValue('Note_S3', isset($value->Note_S3) ? $value->Note_S3 : NULL);
                    $stmt->bindValue('Tranche_S3', isset($value->Tranche_S3) ? $value->Tranche_S3 : NULL);
                    $stmt->bindValue('Num_S4', isset($value->Num_S4) ? $value->Num_S4 : NULL);
                    $stmt->bindValue('DateD_S4', isset($value->DateD_S4) ? $value->DateD_S4 : NULL);
                    $stmt->bindValue('DateF_S4', isset($value->DateF_S4) ? $value->DateF_S4 : NULL);
                   
                    
                    $stmt->bindValue('VHA_S4', isset($value->VHA_S4) ? $value->VHA_S4 : NULL);
                    $stmt->bindValue('VHR_S4', isset($value->VHR_S4) ? $value->VHR_S4 : NULL);
                    $stmt->bindValue('Note_S4', isset($value->Note_S4) ? $value->Note_S4 : NULL);
                    $stmt->bindValue('Tranche_S4', isset($value->Tranche_S4) ? $value->Tranche_S4 : NULL);
                    $stmt->bindValue('VHA_G1', isset($value->VHA_G1) ? $value->VHA_G1 : NULL);
                    $stmt->bindValue('VHR_G1', isset($value->VHR_G1) ? $value->VHR_G1 : NULL);
                    $stmt->bindValue('Note_G1', isset($value->Note_G1) ? $value->Note_G1 : NULL);
                    $stmt->bindValue('Tranche_G1', isset($value->Tranche_G1) ? $value->Tranche_G1 : NULL);
                    $stmt->bindValue('VHA_Autorise', isset($value->VHA_Autorise) ? $value->VHA_Autorise : NULL);
                    $stmt->bindValue('VHA_G2', isset($value->VHA_G2) ? $value->VHA_G2 : NULL);
                    $stmt->bindValue('VHR_G2', isset($value->VHR_G2) ? $value->VHR_G2 : NULL);
                    $stmt->bindValue('Note_G2', isset($value->Note_G2) ? $value->Note_G2 : NULL);
                    $stmt->bindValue('Tranche_G2', isset($value->Tranche_G2) ? $value->Tranche_G2 : NULL);
                    $stmt->bindValue('Appliquer', isset($value->Appliquer) ? $value->Appliquer : NULL);
                    $stmt->bindValue('Debut_App', isset($value->Debut_App) ? $value->Debut_App : NULL);
                    $stmt->bindValue('Fin_App', isset($value->Fin_App) ? $value->Fin_App : NULL);
                    $stmt->bindValue('Motif', isset($value->Motif) ? $value->Motif : NULL);
                    $stmt->bindValue('ID_Annee', isset($value->ID_Annee) ? $value->ID_Annee : NULL);
                    $stmt->bindValue('Annee_Lib', isset($value->Annee_Lib) ? $value->Annee_Lib : NULL);
                    $stmt->bindValue('Prenom', isset($value->Prenom) ? $value->Prenom : NULL);
                   
                    
                    $stmt->bindValue('Decision_S1', isset($value->Decision_S1) ? $value->Decision_S1 : NULL);
                    $stmt->bindValue('Decision_S2', isset($value->Decision_S2) ? $value->Decision_S2 : NULL);
                    $stmt->bindValue('Decision_S3', isset($value->Decision_S3) ? $value->Decision_S3 : NULL);
                    $stmt->bindValue('Decision_S4', isset($value->Decision_S4) ? $value->Decision_S4 : NULL);
                    $stmt->bindValue('Decision_G1', isset($value->Decision_G1) ? $value->Decision_G1 : NULL);
                    $stmt->bindValue('Decision_G2', isset($value->Decision_G2) ? $value->Decision_G2 : NULL);
                    
                   // dump($stmt) ; die();
                    $rs = $stmt->execute();
                }
            }
        }
        return new Response(json_encode($detail));
    }
}
