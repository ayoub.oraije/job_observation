<?php

namespace AppBundle\Controller;


/**
 * Etudiant controller.
 *
 * @Route("univ")
 */
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\AcEtablissement;

class UniversiteController extends Controller {

    /**

     *
     * @Route("/getformation/{id}",options = { "expose" = true },  name="get_formation")
     * @Method({"GET", "POST"})
     */
    public function getFormationAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $AcEtablissement = $em->getRepository('AppBundle:AcEtablissement')->find($id);
        $formation = $em->getRepository('AppBundle:AcEtablissement')->GetFormation($AcEtablissement, null);
        $json_data = array(
            "data" => $formation
        );
        return new Response(json_encode($json_data));
    }
    
    
      /**

     *
     * @Route("/getpromotion/{id}",options = { "expose" = true },  name="get_promotion")
     * @Method({"GET", "POST"})
     */
    public function getPromotionAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $AcFormation = $em->getRepository('AppBundle:AcFormation')->find($id);
        $promotion = $em->getRepository('AppBundle:AcEtablissement')->GetPromotion($AcFormation, null);
        $json_data = array(
            "data" => $promotion
        );
        return new Response(json_encode($json_data));
    }
    
    
      /**

     *
     * @Route("/getgroupedetail/{id}",options = { "expose" = true },  name="get_groupe_detail")
     * @Method({"GET", "POST"})
     */
    public function getGroupeDetailAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $DivisionGroupe= $em->getRepository('AppBundle:DivisionGroupe')->find($id);
        $division_groupe = $em->getRepository('AppBundle:AcEtablissement')->GetGroupeDetails($DivisionGroupe, null);
        $json_data = array(
            "data" => $division_groupe
        );
        return new Response(json_encode($json_data));
    }

}
