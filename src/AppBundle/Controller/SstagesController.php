<?php

namespace AppBundle\Controller;

use AppBundle\Entity\s_stages;
use AppBundle\Entity\SObjniv1;
use AppBundle\Entity\SObjniv2;
use AppBundle\Entity\SObjniv3;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * S_stage controller.
 *
 * @Route("s_stages")
 */
class SstagesController extends Controller {

    /**
     * 
     * list des stages
     * @Route("/list",options = { "expose" = true } , name="stages_list")
     * 
     */
    public function SstagesListAction() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $stages = $em->getRepository('AppBundle:s_stages')->findAll();




        foreach ($stages as $key => $value) {
            $nestedData = array();
            $nestedData[] = ++$key;
//$nestedData[] = $value->getId();
            $nestedData[] = $value->getDesignation();
            $nestedData[] = $value->getAbreviation();
            $nestedData[] = $value->getResponsable()->getDesignation();
            $nestedData[] = $value->getSpecialite()->getDesignation();
            $nestedData[] = $value->getService()->getDesignation();

            $nestedData[] = $value->getPromotion()->getDesignation();

            if ($value->getFermer() == 'oui'): $icon1 = "label label-white";
            else: $icon1 = "label label-success label-white middle";
            endif;

            $nestedData[] = "<span class='" . $icon1 . "'>" . $value->getFermer() . "</span>";





            $url = $this->container->get('router')->generate('s_stages_edit', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-success  ace-icon fa fa-edit bigger-120'></i> </a>";

            $url = $this->container->get('router')->generate('s_stages_show', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-warning ace-icon fa fa-eye bigger-120'></i> </a>";

            $nestedData[] = "<a class='delete_action' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";

            if ($value->getActive() == 1): $icon = "btn-success fa-unlock";
            else: $icon = "btn-danger fa-lock";
            endif;

            $nestedData[] = "<a class='disable_action' rel='" . $value->getId() . "' href='" . $value->getActive() . "'> <i class='btn btn-xs btn-warning ace-icon fa $icon bigger-120'></i></a>";


            $url = $this->container->get('router')->generate('s_stages_niv', array('id' => $value->getId()));
            $nestedData[] = "<a class='' href='" . $url . "'> <i class='btn btn-xs btn-purple  ace-icon fa  fa-info-circle bigger-120'></i> </a>";


            $nestedData["DT_RowId"] = $value->getId();
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * list des objectifs de stages de niveau 1
     *
     * @Route("/obj1/{id}",options = { "expose" = true } , name="stages_obj1")
     * @Method({"GET", "POST"})
     */
    public function Obj1ListAction($id) {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $obj1 = $em->getRepository('AppBundle:SObjniv1')->findBy(array('stage' => $id));

        if ($obj1) {
            foreach ($obj1 as $key => $value) {
                $nestedData = array();
                $nestedData[] = ++$key;
//$nestedData[] = $value->getId();
                $nestedData[] = $value->getDesignation();
                $nestedData[] = $value->getAbreviation();
                $nestedData["DT_RowId"] = $value->getId();
                $nestedData[] = "<a class='delete_action1' rel='" . $value->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * list des objectifs de stages de niveau 2
     *
     * @Route("/obj2/{id}",options = { "expose" = true } , name="stages_obj2")
     * @Method({"GET", "POST"})
     */
    public function Obj2ListAction($id) {
        $data = array();
        $em = $this->getDoctrine()->getManager();
//$this->getEntityManager()->getRepository('AppBundle:s_objniv2')->findBy(array('objNiv1' => $obj1));
        $obj1 = $em->getRepository('AppBundle:SObjniv1')->findBy(array('stage' => $id));

        if ($obj1) {
            foreach ($obj1 as $key => $value) {

                $obj2 = $em->getRepository('AppBundle:SObjniv2')->findBy(array('objNiv1' => $value));
                foreach ($obj2 as $key => $value1) {
                    $nestedData = array();
                    $nestedData[] = $value1->getId();
//$nestedData[] = $value->getId();
                    $nestedData[] = $value1->getDesignation();
                    $nestedData[] = $value1->getAbreviation();
                    $nestedData["DT_RowId"] = $value1->getId();
                    $nestedData[] = "<a class='delete_action2' rel='" . $value1->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";
                    $data[] = $nestedData;
                }
            }
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * list des objectifs de stages de niveau 3
     *
     * @Route("/obj3/{id}",options = { "expose" = true } , name="stages_obj33")
     * @Method({"GET", "POST"})
     */
    public function Obj3ListAction($id) {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $stage = $em->getRepository('AppBundle:s_stages')->findOneBy(array('id' => $id));
        $obj1 = $em->getRepository('AppBundle:SObjniv1')->findBy(array('stage' => $stage));

        if ($obj1) {
            foreach ($obj1 as $key => $value) {

                $obj2 = $em->getRepository('AppBundle:SObjniv2')->findBy(array('objNiv1' => $value));
                foreach ($obj2 as $key => $value1) {
                    $obj3 = $em->getRepository('AppBundle:SObjniv3')->findBy(array('objNiv2' => $value1));
                    foreach ($obj3 as $key => $value2) {
                        $nestedData = array();
                        $nestedData[] = $value2->getId();
//$nestedData[] = $value->getId();
                        $nestedData[] = $value2->getDesignation();
                        $nestedData[] = $value2->getAbreviation();
                        $nestedData["DT_RowId"] = $value2->getId();
                        $nestedData[] = "<a class='delete_action3' rel='" . $value2->getId() . "'><i class='btn btn-xs btn-danger  ace-icon fa fa-trash-o bigger-120'></i></a>";
                        $data[] = $nestedData;
                    }
                }
            }
        }
        $json_data = array(
            "data" => $data
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all s_stage entities.
     *
     * @Route("/", name="s_stages_index")
     * @Method("GET")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $s_stages = $em->getRepository('AppBundle:s_stages')->findAll();
        $lien = 6;
        $li = 9;
        return $this->render('s_stages/index.html.twig', array(
                    's_stages' => $s_stages,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Creates a new s_stage entity.
     *
     * @Route("/new", name="s_stages_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $s_stage = new s_stages();
        $form = $this->createForm('AppBundle\Form\s_stagesType', $s_stage);
        $form->handleRequest($request);
        $lien = 6;
        $li = 10;
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $now = date_create('now');
            $s_stage->setCreated($now);
            $s_stage->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em->persist($s_stage);
            $em->flush();
            $this->addFlash(
                    'notice', 'l\'enregistrement a été effectué avec succès'
            );
            return $this->redirectToRoute('s_stages_index');
        }

        return $this->render('s_stages/new.html.twig', array(
                    's_stage' => $s_stage,
                    'form' => $form->createView(),
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * form d'ajout des objectifs
     *
     * @Route("/{id}", options = { "expose" = true } , name="s_stages_niv")
     * @Method({"GET", "POST"})
     */
    public function affAction($id) {
        $lien = 6;
        $li = 0;

        return $this->render('s_stages/niv.html.twig', array(
                    'id' => $id,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * ajout de l'objectif 1
     *
     * @Route("/adob/{stage_id}/{designation}/{abreviation}", options = { "expose" = true } , name="s_stages_obj1")
     * @Method({"GET", "POST"})
     */
    public function addObj1Action($stage_id, $designation, $abreviation) {
//var_dump($request);
//die("request");
        $json_data = array("data" => "error");
        if ($designation != "") {


            $em = $this->getDoctrine()->getManager();
            $stage = $em->getRepository('AppBundle:s_stages')->find($stage_id);

            $obj1 = new SObjniv1();
            $obj1->setStage($stage);
            $obj1->setDesignation($designation);
            $obj1->setAbreviation($abreviation);


            $em = $this->getDoctrine()->getManager();
//$s_stage->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em->persist($obj1);
            $em->flush();

            $obj = $em->getRepository('AppBundle:s_stages')->GetObjectif2($stage, null);



            $json_data = array('data' => $obj,);
        }


        return new Response(json_encode($json_data));
    }

    /**
     * ajout de l'objectif 2
     *
     * @Route("/adob2/{obj1_id}/{designation}/{abreviation}", options = { "expose" = true } , name="s_stages_obj2")
     * @Method({"GET", "POST"})
     */
    public function addObj2Action($obj1_id, $designation, $abreviation) {
//var_dump($request);
//die("request");
        $json_data = array("data" => "error");
        if ($designation != "") {


            $em = $this->getDoctrine()->getManager();
            $obj1 = $em->getRepository('AppBundle:SObjniv1')->find($obj1_id);

            $obj2 = new SObjniv2();
            $obj2->setObjNiv1($obj1);
            $obj2->setDesignation($designation);
            $obj2->setAbreviation($abreviation);


            $em = $this->getDoctrine()->getManager();
//$s_stage->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());
            $em->persist($obj2);
            $em->flush();


            $obj3 = $em->getRepository('AppBundle:SObjniv1')->find($obj1_id);

            $obj = $em->getRepository('AppBundle:s_stages')->GetObjectif2($obj3->getStage(), null);




            $json_data = array('data' => $obj,);
        }


        return new Response(json_encode($json_data));
    }

    /**
     * ajout de l'objectif 3
     *
     * @Route("/adob3/{obj2_id}/{designation}/{abreviation}", options = { "expose" = true } , name="s_stages_obj3")
     * @Method({"GET", "POST"})
     */
    public function addObj3Action($obj2_id, $designation, $abreviation) {
//var_dump($request);
//die("request");
        $json_data = array("data" => "error");
        if ($designation != "") {


            $em = $this->getDoctrine()->getManager();
            $obj2 = $em->getRepository('AppBundle:SObjniv2')->findOneBy(array('id' => $obj2_id));

            $obj3 = new SObjniv3();
            $obj3->setObjNiv2($obj2);
            $obj3->setDesignation($designation);
            $obj3->setAbreviation($abreviation);
            $obj3->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());



            $em->persist($obj3);
            $em->flush();




            $json_data = array('data' => "bien",);
        }


        return new Response(json_encode($json_data));
    }

    /**
     * Finds 
     *
     * @Route("/obj33/{id}", options = { "expose" = true } , name="sstage_obj3")
     * @Method({"GET"})
     */
    public function obj3Action($id) {
        /* var_dump($id);
          die() */
        $em = $this->getDoctrine()->getManager();
        //$obj = $em->getRepository('AppBundle:s_objniv1')->find($id);

        $obj1 = $em->getRepository('AppBundle:s_stages')->GetObjectif3($id);

        $json_data = array('data' => $obj1,);

        return new Response(json_encode($json_data));
    }

    /**
     * Finds 
     *
     * @Route("/obj23/{id}", options = { "expose" = true } , name="sstage_obj2")
     * @Method({"GET"})
     */
    public function obj2Action($id) {
        /* var_dump($id);
          die() */
        $em = $this->getDoctrine()->getManager();
        $stage = $em->getRepository('AppBundle:s_stages')->find($id);
        $obj = $em->getRepository('AppBundle:s_stages')->GetObjectif2($stage, null);

        //$obj = $em->getRepository('AppBundle:s_objniv1')->find($id);



        $json_data = array('data' => $obj,);

        return new Response(json_encode($json_data));
    }

    /**
     * Finds and displays a s_stage entity.
     *
     * @Route("/show/{id}", name="s_stages_show")
     * @Method("GET")
     */
    public function showAction(s_stages $s_stage) {
        $deleteForm = $this->createDeleteForm($s_stage);
$lien = 6;
        $li = 0;
        return $this->render('s_stages/show.html.twig', array(
                    's_stage' => $s_stage,
                    'delete_form' => $deleteForm->createView(),
             'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Displays a form to edit an existing s_stage entity.
     *
     * @Route("/edit{id}/edit", name="s_stages_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, s_stages $s_stage) {
        $deleteForm = $this->createDeleteForm($s_stage);
        $editForm = $this->createForm('AppBundle\Form\s_stagesType', $s_stage);
        $editForm->handleRequest($request);
$lien = 6;
        $li = 0;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $now = date_create('now');
            $s_stage->setUpdated($now);
            $s_stage->setUserUpdated($this->container->get('security.token_storage')->getToken()->getUser());
            $this->getDoctrine()->getManager()->flush();
// Set a flash message
            $this->addFlash(
                    'notice', ' la modification a été effectué avec succès'
            );
            return $this->redirectToRoute('s_stages_index');
        }
        return $this->render('s_stages/edit.html.twig', array(
                    's_stages' => $s_stage,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
             'lien' => $lien,
                    'li' => $li,
        ));

// return $this->render('s_stages/index.html.twig');
    }

    /**

     *
     * @Route("/delete/{id}", options = { "expose" = true }, name="s_stages_delete")

     */
    public function deleteAction($id) {

        $em = $this->getDoctrine()->getManager();
        $s_stages = $em->getRepository('AppBundle:s_stages')->findOneBy(array('id' => $id));
        $objNiv1 = $em->getRepository('AppBundle:SObjniv1')->findBy(array('stage' => $s_stages));

        if ($objNiv1) {

            $json_data = array(
                'data' => 'Erreur lors de la suppression ',
                'a' => 0,
            );
        } else {

            $em->remove($s_stages);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
                'a' => 1,
            );
        }
        return new Response(json_encode($json_data));
    }

    /**
     *
     * @Route("/delOb1/{id}", options = { "expose" = true }, name="obj1_delete")
     */
    public function deleteObj1Action($id) {

        $em = $this->getDoctrine()->getManager();
        $s_obj1 = $em->getRepository('AppBundle:SObjniv1')->find($id);

        if ($s_obj1) {
            $obj2 = $em->getRepository('AppBundle:SObjniv2')->findBy(array('objNiv1' => $s_obj1));
            if (!$obj2) {
                $em->remove($s_obj1);
                $em->flush();
                $json_data = array(
                    'data' => 'La suppression a été effectuée avec succès',
                );
            } else {
                $json_data = array(
                    'data' => 'Erreur lors de la suppression ',
                );
            }
        } else {
            $json_data = array(
                'data' => 'Erreur lors de la suppression ',
            );
        }
        return new Response(json_encode($json_data));
    }

    /**

     *
     * @Route("/delOb2/{id}", options = { "expose" = true }, name="obj2_delete")

     */
    public function deleteObj2Action($id) {

        $em = $this->getDoctrine()->getManager();
        $s_obj2 = $em->getRepository('AppBundle:SObjniv2')->find($id);

        if ($s_obj2) {
            $obj3 = $em->getRepository('AppBundle:SObjniv3')->findBy(array('objNiv2' => $s_obj2));
            if (!$obj3) {
                $em->remove($s_obj2);
                $em->flush();
                $json_data = array(
                    'data' => 'La suppression a été effectuée avec succès',
                );
            } else {
                $json_data = array(
                    'data' => 'Erreur lors de la suppression ',
                );
            }
        } else {
            $json_data = array(
                'data' => 'Erreur lors de la suppression ',
            );
        }

        return new Response(json_encode($json_data));
    }

    /**

     *
     * @Route("/delOb3/{id}", options = { "expose" = true }, name="obj3_delete")

     */
    public function deleteObj3Action($id) {

        $em = $this->getDoctrine()->getManager();
        $s_obj3 = $em->getRepository('AppBundle:SObjniv3')->find($id);

        if ($s_obj3) {

            $em->remove($s_obj3);
            $em->flush();
            $json_data = array(
                'data' => 'La suppression a été effectuée avec succès',
            );
        } else {
            $json_data = array(
                'data' => 'Erreur lors de la suppression ',
            );
        }
        return new Response(json_encode($json_data));
    }

    /**
     * Deletes a Stages entity.
     *
     * @Route("/delete1/{id}", options = { "expose" = true }  , name="s_stages_delete1")
     *
     */
    public function deleteSstagesAction($id) {
        $em = $this->getDoctrine()->getManager();
        $s_stages = $em->getRepository('AppBundle:s_stages')->find($id);
        $stage = $em->getRepository('AppBundle:s_group_stage')->findby(array('stage' => $id));

        if ($stage) {
            $this->addFlash(
                    'error', 'Suppression a echoué, Ce Stage "' . $s_stages->getDesignation() . '" affecté à un ou plusieurs Groupes.'
            );
        } else {
            $em->remove($s_stages);
            $em->flush();
            $this->addFlash(
                    'notice', 'La suppression a été effectuée avec succès'
            );
        }

        return $this->redirectToRoute('s_stages_index');
    }

    /**
     * Creates a form to delete a s_stage entity.
     *
     * @param s_stages $s_stage The s_stage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(s_stages $s_stage) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('s_stages_delete', array('id' => $s_stage->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**

     *
     * @Route("/getannee/{etab}/{form}",options = { "expose" = true },  name="get_annee")
     * @Method({"GET", "POST"})
     */
    public function getAnneAction(Request $request, $etab, $form) {
        $em = $this->getDoctrine()->getManager();
        $AcEtablissement = $em->getRepository('AppBundle:AcEtablissement')->find($etab);
        $formation = $em->getRepository('AppBundle:AcFormation')->find($form);
        $annee = $em->getRepository('AppBundle:AcEtablissement')->GetAnnee($AcEtablissement, $formation);

        $json_data = array(
            "data" => $annee
        );
        return new Response(json_encode($json_data));
    }

    /**

     *
     * @Route("/getformation/{id}",options = { "expose" = true },  name="get_formation")
     * @Method({"GET", "POST"})
     */
    public function getFormationAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $AcEtablissement = $em->getRepository('AppBundle:AcEtablissement')->find($id);
        $formation = $em->getRepository('AppBundle:AcEtablissement')->GetFormation($AcEtablissement, null);
        $json_data = array(
            "data" => $formation
        );
        return new Response(json_encode($json_data));
    }

    /**

     *
     * @Route("/getpromotion/{id}",options = { "expose" = true },  name="get_promotion")
     * @Method({"GET", "POST"})
     */
    public function getPromotionAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $AcFormation = $em->getRepository('AppBundle:AcFormation')->find($id);
        $promotion = $em->getRepository('AppBundle:AcEtablissement')->GetPromotion($AcFormation, null);
        $json_data = array(
            "data" => $promotion
        );
        return new Response(json_encode($json_data));
    }
    
    
       /**

     *
     * @Route("/getsemestre/{id}",options = { "expose" = true },  name="get_semestre")
     * @Method({"GET", "POST"})
     */
    public function getSemestreAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $AcPromotion = $em->getRepository('AppBundle:AcPromotion')->find($id);
        $semestre = $em->getRepository('AppBundle:AcEtablissement')->GetSemestre($AcPromotion, null);
        $json_data = array(
            "data" => $semestre
        );
        return new Response(json_encode($json_data));
    }
    

    /**
     * 
     *
     * @Route("/disable/{id}/{etat}" ,options = { "expose" = true } , name="s_stages_disable")
     * 
     */
    public function DisableAction($id, $etat) {
        $text = "";
        if ($etat == 1) {
            $etat = 0;
            $text = "désactivé";
        } else {
            $etat = 1;
            $text = "activé";
        }

        $em = $this->getDoctrine()->getManager();
        $s_stages = $em->getRepository('AppBundle:s_stages')->find($id);

        $s_stages->setActive($etat);
        $em->flush();

        $json_data = array(
            'data' => 'Le champ a été ' . $text . ' avec succés',
        );


        return new Response(json_encode($json_data));
    }

}
