<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcEtablissement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Acetablissement controller.
 *
 * @Route("acetablissement")
 */
class AcEtablissementController extends Controller
{
    /**
     * Lists all acEtablissement entities.
     *
     * @Route("/", name="acetablissement_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acEtablissements = $em->getRepository('AppBundle:AcEtablissement')->findAll();

        return $this->render('acetablissement/index.html.twig', array(
            'acEtablissements' => $acEtablissements,
        ));
    }

    /**
     * Creates a new acEtablissement entity.
     *
     * @Route("/new", name="acetablissement_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $acEtablissement = new Acetablissement();
        $form = $this->createForm('AppBundle\Form\AcEtablissementType', $acEtablissement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acEtablissement);
            $em->flush();

            return $this->redirectToRoute('acetablissement_show', array('id' => $acEtablissement->getId()));
        }

        return $this->render('acetablissement/new.html.twig', array(
            'acEtablissement' => $acEtablissement,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a acEtablissement entity.
     *
     * @Route("/{id}", name="acetablissement_show")
     * @Method("GET")
     */
    public function showAction(AcEtablissement $acEtablissement)
    {
        $deleteForm = $this->createDeleteForm($acEtablissement);

        return $this->render('acetablissement/show.html.twig', array(
            'acEtablissement' => $acEtablissement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing acEtablissement entity.
     *
     * @Route("/{id}/edit", name="acetablissement_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AcEtablissement $acEtablissement)
    {
        $deleteForm = $this->createDeleteForm($acEtablissement);
        $editForm = $this->createForm('AppBundle\Form\AcEtablissementType', $acEtablissement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acetablissement_edit', array('id' => $acEtablissement->getId()));
        }

        return $this->render('acetablissement/edit.html.twig', array(
            'acEtablissement' => $acEtablissement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a acEtablissement entity.
     *
     * @Route("/{id}", name="acetablissement_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AcEtablissement $acEtablissement)
    {
        $form = $this->createDeleteForm($acEtablissement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($acEtablissement);
            $em->flush();
        }

        return $this->redirectToRoute('acetablissement_index');
    }

    /**
     * Creates a form to delete a acEtablissement entity.
     *
     * @param AcEtablissement $acEtablissement The acEtablissement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AcEtablissement $acEtablissement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acetablissement_delete', array('id' => $acEtablissement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
