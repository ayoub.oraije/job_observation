<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Etudiant controller.
 *
 * @Route("administrateur")
 */
class AdministrateurController extends Controller {

    /**
     * @Route("/", name="administrateur_index")
     */
    public function indexAction(Request $request) {
        $lien = 0;
        $li = 0;
        return $this->render('administrateur/index.html.twig', array(
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

}
