<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\TEtudiant;
use AppBundle\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use DateTime;
use AppBundle\Entity\SInsStg;

/**
 * Inscription controller.
 *
 * @Route("inscription")
 */
class InscriptionController extends Controller {

    /**
     * 
     *
     * @Route("/list",options = { "expose" = true } , name="etudiant_list1")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request) {
        $params = $request->query;

        // dump($params->get('length'));
        // dump($params->get('columns')[0]['search']['value']); 
        //die();
        $where = $totalRows = $sqlRequest = "";
        $filtre = "";
        if (!empty($params->get('columns')[0]['search']['value'])) {
            $filtre .= " and etab.id = '" . $params->get('columns')[0]['search']['value'] . "' ";
        }
        if (!empty($params->get('columns')[1]['search']['value'])) {
            $filtre .= " and frm.id = '" . $params->get('columns')[1]['search']['value'] . "' ";
        }
        if (!empty($params->get('columns')[2]['search']['value'])) {
            $filtre .= " and promo.id = '" . $params->get('columns')[2]['search']['value'] . "' ";
        }
        if (!empty($params->get('columns')[3]['search']['value'])) {
            $filtre .= " and an.id = '" . $params->get('columns')[3]['search']['value'] . "' ";
        }


        $sql = "SELECT ins.id ,adm.code as 'code_admission', ins.code , etu.nom , etu.prenom , etu.cin,etab.abreviation as 'etab_abreviation' , frm.abreviation as 'for_abreviation',promo.designation as 'designation_promotion',an.designation as 'designation_annee',st.designation as 'statut' 
                FROM t_inscription ins
                INNER JOIN t_admission adm ON ins.t_admission_id=adm.id
                INNER JOIN t_preinscription pr ON adm.t_preinscription_id=pr.id 
                INNER JOIN t_etudiant etu ON pr.t_etudiant_id=etu.id 
                INNER JOIN ac_promotion promo ON ins.ac_promotion_id=promo.id
                INNER JOIN ac_formation frm ON promo.formation_id=frm.id
                INNER JOIN ac_etablissement etab ON etab.id= frm.ac_etablissement_id
                INNER JOIN ac_annee an ON ins.ac_annee_id=an.id 
               
                INNER JOIN p_statut st ON ins.p_statut_id=st.id WHERE 1=1 /*AND an.validation_academique ='non'*/ $filtre ";

        $totalRows .= $sql;
        $sqlRequest .= $sql;




        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sqlRequest);
        $stmt->execute();
        $totalRecords = count($stmt->fetchAll());
        // var_dump($totalRecords);
        // die();
//        $em = $this->getDoctrine()->getManager(); 
//        $connection = $em->getConnection();
//        $etud = $connection->prepare($totalRows);
//
//        $etud->execute();



        $columns = array(
            0 => 'ins.id',
            1 => 'adm.code',
            2 => 'etu.nom',
            3 => 'etu.prenom',
            4 => 'etu.cin',
            5 => 'etab.abreviation',
            6 => 'frm.abreviation',
            7 => 'promo.designation',
            8 => 'an.designation',
            9 => 'st.designation'
        );


        if (!empty($params->get('search')['value'])) {
            $search = $params->get('search')['value'];
            $where .= " and ( ins.id LIKE '%$search%' ";
            $where .= " OR adm.code LIKE '%$search%' ";
            $where .= " OR etu.nom LIKE '%$search%' ";
            $where .= " OR etu.prenom LIKE '%$search%' ";
            $where .= " OR etu.cin LIKE '%$search%' ";
            $where .= " OR frm.abreviation LIKE '%$search%' ";
            $where .= " OR etab.abreviation LIKE '%$search%' ";
            $where .= " OR an.designation LIKE '%$search%' ";
            $where .= " OR promo.designation LIKE '%$search%' ";
            $where .= " OR st.designation LIKE '%$search%' )";
        }


        if (isset($where) && $where != '') {
            $totalRows .= $where;
            $sqlRequest .= $where;
        }
        //  dump($sqlRequest);
        //  die();
        $sqlRequest .= " ORDER BY " . $columns[$params->get('order')[0]['column']] . "   " . $params->get('order')[0]['dir'] . "  LIMIT " . $params->get('start') . " ," . $params->get('length') . " ";


        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sqlRequest);
        $stmt->execute();
        $result = $stmt->fetchAll();


        $data = array();
        $em = $this->getDoctrine()->getManager();
      //  $in = $em->getRepository('AppBundle:SInsStg')->findAll();
          $in = $em->getRepository('AppBundle:SInsStg')->findBy(array('statut' => 0));
        foreach ($result as $key => $row) {
            $nestedData = array();
            $var = 0;
            if (!empty($params->get('columns')[3]['search']['value'])) {
                foreach ($in as $key => $val) {
                    if ($val->getInscription()->getId() == $row['id']) {
                        $nestedData[] = "  <label><input name='form-field-checkbox[]' disabled  class='ads_Checkbox ace ace-checkbox-2' value='" . $row['id'] . "' type='checkbox'>
                                                                                        <span class='lbl'></span>
                                                                                    </label>";
                        $var = 1;
                    }
                }
            }
            if ($var == 0) {
                if (!empty($params->get('columns')[3]['search']['value'])) {
                    $nestedData[] = "  <label><input name='form-field-checkbox[]'  class='ads_Checkbox ace ace-checkbox-2 cat' value='" . $row['id'] . "' type='checkbox'>
                                                                                        <span class='lbl'></span>
                                                                                    </label>";
                } else {
                    $nestedData[] = $row['id'];
                }
            }


            $cd = $row['id'];


            $nestedData[] = $row['code_admission'];
            $nestedData[] = $row['nom'];
            $nestedData[] = $row['prenom'];
            $nestedData[] = $row['cin'];
            $nestedData[] = $row['etab_abreviation'];
            $nestedData[] = $row['for_abreviation'];
            $nestedData[] = $row['designation_promotion'];
            $nestedData[] = $row['designation_annee'];
            $nestedData[] = $row['statut'];
            $nestedData["DT_RowId"] = $cd;
            $nestedData["DT_RowClass"] = $cd;
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($params->get('draw')),
            "recordsTotal" => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data" => $data   // total data array
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all etudiant entities.
     *
     * @Route("/etudiants", name="etudiant_list_index")
     * 
     */
    public function indexAction(Request $request, FileUploader $fileUploader) {


        $em = $this->getDoctrine()->getManager();
        $etablissement = $em->getRepository('AppBundle:AcEtablissement')->GetEtablissement(null);
        $anne = $em->getRepository('AppBundle:AcEtablissement')->GetAnne(null);
        $groupe = 'xx'; //$em->getRepository('AppBundle:AcEtablissement')->GetGroupe(null);
        $lien = 7;
        $li = 11;
        return $this->render('inscription/index.html.twig', array('etablissement' => $etablissement, 'groupe' => $groupe, 'annee' => $anne,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/group/{anne}/{promo}/{semestre}", options = { "expose" = true }  , name="group_ins")
     *  @Method({"GET", "POST"})
     */
    public function groupAction(Request $request, $anne, $promo, $semestre) {
        /* var_dump($anne);
          var_dump($request->get('form-field-checkbox'));
          die(); */
        $em = $this->getDoctrine()->getManager();
//promotion// $group = $em->getRepository('AppBundle:SGroupStage')->findAll();

        $group = $em->getRepository('AppBundle:SGroupStage')->findBy(array('semestre' => $semestre));


        $incriptions = $request->get('form-field-checkbox');
        $inscri;
        for ($i = 0; $i < count($incriptions); $i++) {
            $inscri[] = $em->getRepository('AppBundle:TInscription')->find($incriptions[$i]);
        }

        //var_dump($inscri);
        //die();
        // die();
        return $this->render('inscription/group.html.twig', array('groups' => $group, 'inscription' => $inscri, 'anne' => $anne, 'promotion' => $promo));
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/groupannuler/{promo}/", options = { "expose" = true }  , name="group_annuler")
     *  @Method({"GET", "POST"})
     */
    public function groupAnnulerAction(Request $request, $promo) {

        $em = $this->getDoctrine()->getManager();


        $insStg = $em->getRepository('AppBundle:SInsStg')->findBy(array('promotion' => $promo, 'statut' => 0));


        foreach ($insStg as $key => $value) {

            $value->SetStatut(1);
            $em->flush();
        }
        $json_data = array('data' => 'le Stage est Annuler',
        );

        return new Response(json_encode($json_data));
    }

    /**
     * Displays financier informations
     *
     * @Route("/gr/", options = { "expose" = true }  , name="etu_group")
     * @Method({"GET", "POST"})
     */
    public function afectationAction(Request $request) {

        //$request->get('form-field-checkbox');


        $em = $this->getDoctrine()->getManager();


        // var_dump($request->get('gr_etu'));die('test');
        if (!empty($request->get('gr_etu'))) {

            $inscriptions = $request->get('etudient');
            $group = $em->getRepository('AppBundle:SGroupStage')->find($request->get('gr_etu'));
            $anne = $em->getRepository('AppBundle:AcAnnee')->find($request->get('annee'));
            $promotion = $em->getRepository('AppBundle:AcPromotion')->find($request->get('promotion'));

            // $user= $em->getRepository('AppBundle:User')->findBy(Array('username'=>$this->container->get('security.token_storage')->getToken()->getUser()));
            // var_dump($user);
            //    die();
            for ($i = 0; $i < count($inscriptions); $i++) {
                $inscri = $em->getRepository('AppBundle:TInscription')->find($inscriptions[$i]);


                $grEt = $em->getRepository('AppBundle:SInsStg')->findBy(Array('inscription' => $inscri,'statut'=>0));

                if (empty($grEt)) {
                    $ins = new SInsStg();
                    //$ins->setCreated($created)
                    $ins->setInscription($inscri);
                    $ins->setGroupe($group);
                    $ins->setAnnee($anne);
                    $ins->setStatut(0);
                    $ins->setPromotion($promotion);
                    // var_dump($this->container->get('security.token_storage')->getToken()->getUser());
                    //  die();
                    $now = date_create('now');
                    $ins->setCreated($now);
                    $ins->setUserCreated($this->container->get('security.token_storage')->getToken()->getUser());

                    //var_dump($ins);
                    //die();

                    $em->persist($ins);
                    $em->flush();
                } else {
                  //  $grEt->setStatut(0);
                    $grEt->setGroupe($group);
                    $em->flush();
                }
            }
            $json_data = array('data' => 'l\'enregistrement a été effectué avec succès',
                'e' => 1,
            );
        } else {
            $json_data = array('data' => 'Erreur lors de l\'insertion', 'e' => 0,
            );
        }






        return new Response(json_encode($json_data));
    }

}
