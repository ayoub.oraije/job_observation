<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Service\FileUploader;
use DateTime;

/**
 * rapport controller.
 *
 * @Route("rapport")
 */
class RapportController extends Controller {

    /**
     * @Route("/print/{id}", name="stage_print")
     */
    public function printAction($id) {


        $em = $this->getDoctrine()->getManager();

        $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));
        /* if(isset($patients->getObjEtudiant())!=null)

          {
          $objectifs=$patients->getObjEtudiant();
          } */


        /*  foreach ($patients->getCategorie()->getRubrique() as $key => $row) 
          {


          var_dump($row->getDesignation());


          }
          die(); */


        $html = $this->renderView('rapport/rapo.html.twig', array(
            'patient' => $patients,
        ));

        error_reporting(0);

        $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);

        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

        foreach ($doc->getElementsByTagName('img') as $key => $image) {


            $img = $image->getAttribute('src');
            $data = "";
            if (strpos($img, '../../../images/bar/') !== false) {
                // die('xxxxxxxxxxxxx');
                $img = str_replace('../../../images/bar/', 'images/bar/', $img);
            }



            $image->setAttribute("src", $img);
        }
        $doc->removeChild($doc->doctype);
        $content = $doc->saveHTML($doc);
        $content = str_replace('<html><body>', '', $content);
        $content = str_replace('</body></html>', '', $content);


        $mpdf = new \mPDF();
        $mpdf->SetTitle('RAPPORT ETUDIANT');
        $mpdf->WriteHTML($content);
        $mpdf->Output();
    }

    /**
     * @Route("/etudiant_rapports/{id}", name="etudiant_rapports")
     */
    public function etudiantRapportsAction($id) {

        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $id));
        // $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('inscription' => $inscription,));
        $insgr = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('inscription' => $inscription, 'statut' => 0));

        $patients = $em->getRepository('AppBundle:PPatient')->findOneBy(array('inscription' => $inscription, 'groupe' => $insgr->getGroupe()));
        $patients2 = $em->getRepository('AppBundle:PPatient')->GetPatients2($inscription->getId(), $insgr->getGroupe()->getId());
        //var_dump($patients2);
        //die();
        // $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));

        $html = $this->renderView('rapport/etudiant1.html.twig', array(
            'patient' => $patients,
            'inscription' => $inscription,
            'res' => $patients2,
        ));


        $mpdf = new \mPDF();
        $mpdf->SetTitle('RAPPORTS ETUDIANT');
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    /**
     * 
     * @Route("/list",options = { "expose" = true } , name="rapport_etudiant_list")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request) {
        $params = $request->query;

        // dump($params->get('length'));
        // dump($params->get('columns')[0]['search']['value']); 
        //die();
        $where = $totalRows = $sqlRequest = "";
        $filtre = "";
        if (!empty($params->get('columns')[0]['search']['value'])) {
            $filtre .= " and etab.id = '" . $params->get('columns')[0]['search']['value'] . "' ";
        }
        if (!empty($params->get('columns')[1]['search']['value'])) {
            $filtre .= " and frm.id = '" . $params->get('columns')[1]['search']['value'] . "' ";
        }
        if (!empty($params->get('columns')[2]['search']['value'])) {
            $filtre .= " and promo.id = '" . $params->get('columns')[2]['search']['value'] . "' ";
        }

        $sql = "SELECT ins.id ,adm.code as 'code_admission', ins.code , etu.nom , etu.prenom , etu.cin,etab.abreviation as 'etab_abreviation' , frm.abreviation as 'for_abreviation',promo.designation as 'designation_promotion',an.designation as 'designation_annee',st.designation as 'statut' 
from t_inscription ins
inner JOIN t_admission adm on ins.t_admission_id=adm.id
INNER JOIN t_preinscription pr on adm.t_preinscription_id=pr.id 
INNER JOIN t_etudiant etu on pr.t_etudiant_id=etu.id 
INNER JOIN ac_promotion promo on ins.ac_promotion_id=promo.id
INNER JOIN ac_formation frm on promo.formation_id=frm.id
INNER JOIN ac_etablissement etab on etab.id= frm.ac_etablissement_id
INNER JOIN ac_annee an on ins.ac_annee_id=an.id 
INNER JOIN s_ins_stg sti on sti.t_inscription_id=ins.id
INNER JOIN p_statut st on ins.p_statut_id=st.id WHERE 1=1 and  sti.statut=0 and 0<(select COUNT(*) from p_patient p where p.t_inscription_id=sti.t_inscription_id ) $filtre ";

        $totalRows .= $sql;
        $sqlRequest .= $sql;




        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sqlRequest);
        $stmt->execute();
        $totalRecords = count($stmt->fetchAll());
        // var_dump($totalRecords);
        // die();
//        $em = $this->getDoctrine()->getManager(); 
//        $connection = $em->getConnection();
//        $etud = $connection->prepare($totalRows);
//
//        $etud->execute();



        $columns = array(
            0 => 'ins.id',
            1 => 'adm.code',
            2 => 'etu.nom',
            3 => 'etu.prenom',
            4 => 'etu.cin',
            5 => 'etab.abreviation',
            6 => 'frm.abreviation',
            7 => 'promo.designation',
            8 => 'an.designation',
            9 => 'st.designation'
        );


        if (!empty($params->get('search')['value'])) {
            $search = $params->get('search')['value'];
            $where .= " and ( ins.id LIKE '%$search%' ";
            $where .= " OR adm.code LIKE '%$search%' ";
            $where .= " OR etu.nom LIKE '%$search%' ";
            $where .= " OR etu.prenom LIKE '%$search%' ";
            $where .= " OR etu.cin LIKE '%$search%' ";
            $where .= " OR frm.abreviation LIKE '%$search%' ";
            $where .= " OR etab.abreviation LIKE '%$search%' ";
            $where .= " OR an.designation LIKE '%$search%' ";
            $where .= " OR promo.designation LIKE '%$search%' ";
            $where .= " OR st.designation LIKE '%$search%' )";
        }


        if (isset($where) && $where != '') {
            $totalRows .= $where;
            $sqlRequest .= $where;
        }
        //  dump($sqlRequest);
        //  die();
        $sqlRequest .= " ORDER BY " . $columns[$params->get('order')[0]['column']] . "   " . $params->get('order')[0]['dir'] . "  LIMIT " . $params->get('start') . " ," . $params->get('length') . " ";


        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sqlRequest);
        $stmt->execute();
        $result = $stmt->fetchAll();


        $data = array();

        foreach ($result as $key => $row) {
            $nestedData = array();


            $cd = $row['id'];

            $nestedData[] = $row['id'];
            $nestedData[] = $row['code_admission'];
            $nestedData[] = $row['nom'];
            $nestedData[] = $row['prenom'];
            $nestedData[] = $row['cin'];
            $nestedData[] = $row['etab_abreviation'];
            $nestedData[] = $row['for_abreviation'];
            $nestedData[] = $row['designation_promotion'];
            $nestedData[] = $row['designation_annee'];
            $nestedData[] = $row['statut'];
            $nestedData["DT_RowId"] = $cd;
            $nestedData["DT_RowClass"] = $cd;
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($params->get('draw')),
            "recordsTotal" => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data" => $data   // total data array
        );


        return new Response(json_encode($json_data));
    }

    /**
     * Lists all etudiant entities.
     *
     * @Route("/", name="inscription_list_rapport")
     *  @Method("GET")
     */
    public function indexAction(Request $request, FileUploader $fileUploader) {


        $em = $this->getDoctrine()->getManager();
        $etablissement = $em->getRepository('AppBundle:AcEtablissement')->GetEtablissement(null);
        $groupe = 'xx'; //$em->getRepository('AppBundle:AcEtablissement')->GetGroupe(null);
        $lien = 8;
        $li = 12;
        return $this->render('rapport/index.html.twig', array('etablissement' => $etablissement, 'groupe' => $groupe,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }

    /**
     * 
     *
     * @Route("/modal{id}",options = { "expose" = true } , name="modal_rap")
     *  @Method("GET")
     */
    public function popRapAction($id) {



        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->find($id);
        $st = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('inscription' => $inscription, 'statut' => 0));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $st->getGroupe()));
        // var_dump($stage);
        //die();
        $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));
        $module = $em->getRepository('AppBundle:PModule')->findAll();
        //  $etablissement = $em->getRepository('AppBundle:AcEtablissement')->GetEtablissement(null);
        // $groupe = 'xx'; //$em->getRepository('AppBundle:AcEtablissement')->GetGroupe(null);

        return $this->render('rapport/popRap.html.twig', array(
                    'id' => $id,
                    'module' => $module,
                    'patients' => $patients,
                     'pid'=>$id,
        ));
    }

    /**
     * 
     * @Route("/promotion_rapports/{id}",options = { "expose" = true } , name="rapports_promotion")
     * @Method({"GET"})
     */
    public function promotionAction($id) {


        $em = $this->getDoctrine()->getManager();
        $st = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('promotion' => $id, 'statut' => 0));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $st->getGroupe()));
          // var_dump($st->getGroupe()->getId());die;
        $patients = $em->getRepository('AppBundle:PPatient')->GetPatients($id, $st->getGroupe()->getId());
       //  var_dump($patients);die;
        $html = $this->renderView('rapport/promotion.html.twig', array(
            'patients' => $patients,
        ));

        error_reporting(0);
        $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

        foreach ($doc->getElementsByTagName('img') as $key => $image) {


            $img = $image->getAttribute('src');
            $data = "";
            if (strpos($img, '../../../images/bar/') !== false) {
                // die('xxxxxxxxxxxxx');
                $img = str_replace('../../../images/bar/', 'images/bar/', $img);
            }



            $image->setAttribute("src", $img);
        }

        $doc->removeChild($doc->doctype);
        $content = $doc->saveHTML($doc);
        $content = str_replace('<html><body>', '', $content);
        $content = str_replace('</body></html>', '', $content);


        $mpdf = new \mPDF();
        $mpdf->SetTitle('RAPPORT PAR PROMOTION');
        $mpdf->WriteHTML($content);
        $mpdf->Output();


        /* $json_data = array( "data" => $data   // total data array
          );


          return new Response(json_encode($json_data)); */
    }

    /**
     * @Route("/sansobj/{id}/{type}", name="sansObjectif")
     */
    public function RapportsSansObjectifAction($id, $type) {
        $session = new Session();
//  $session->get('inscription');

        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $id));
        $st = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('inscription' => $inscription, 'statut' => 0));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $st->getGroupe()));
        $patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));


        foreach ($patients as $key => $value) {
            foreach ($value->getProcessus() as $key => $proc) {
                $txt = $em->getRepository('AppBundle:PPatient')->remove_evil_styles_v2($proc->getText());

                $proc->setText($txt);
            }
        }

// $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));
//        $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
//            'patients' => $patients
//            
//        ));

        if ($type == 'w') {
            $folder = $inscription->getAdmission()->getPreinscription()->getEtudiant()->getNom() . " " . $inscription->getAdmission()->getPreinscription()->getEtudiant()->getPrenom() . ".doc";
            header('Content-Type: application/msword');
            header("Content-Disposition: attachment; filename=" . $folder);
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');

            $html = $this->renderView('rapport/etudiantSansObjectif2.html.twig', array(
                'patients' => $patients,
                'inscription' => $inscription,
                'pic' => 'w'
            ));

            $doc = new \DOMDocument('1.0', 'UTF-8');
            // $doc = new \DOMDocument();
            //die("xx");
            libxml_use_internal_errors(true);
            // $doc->loadHTML($html);
            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));



            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $img = str_replace('../../../', '', $img);

                // var_dump($img);
                //  die();
                // $img = str_replace('data:image/jpeg;base64,', '', $img);
                // $img = str_replace('data:image/png;base64,', '', $img);
                //  $data = base64_decode($img);
                // $var = uniqid();
                // $file = 'images/rapports/test' . $var . '.png';
                //$success = file_put_contents($file, $data);
                // $del = str_replace('images/rapports', 'images/etudiant', $img);
                $del = str_replace('images/bar', 'images/rapports', $img);
                copy($img, $del);
                $del = str_replace('images/bar', '../../images/rapports', $img);

                $image->setAttribute("src", $del);

                // var_dump($del);
                //die();
                // delete unlink($file);
            }




            echo $doc->saveHTML($doc);
            //die();            
            return 1;
        } else {


            $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
                'patients' => $patients,
                'inscription' => $inscription,
                'pic' => 'p'
            ));

            error_reporting(0);
            $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);

            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $data = "";
                if (strpos($img, '../../../images/bar/') !== false) {
                    // die('xxxxxxxxxxxxx');
                    $img = str_replace('../../../images/bar/', 'images/bar/', $img);
                }



                $image->setAttribute("src", $img);
            }
            
              $doc->removeChild($doc->doctype);
          $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
             

            $mpdf = new \mPDF();
            $mpdf->SetDisplayMode('fullwidth');
            $mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
            $mpdf->setFooter('<center>page : {PAGENO}</center>');
            $mpdf->WriteHTML($content);
            $mpdf->Output();
        }









//        $mpdf = new \mPDF();
//        $mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
//        $mpdf->WriteHTML($html);
//        $mpdf->Output();
    }
    
    
      /**
     * 
     * @Route("/promotion_rapports_word/{id}",options = { "expose" = true } , name="rapports_promotion_word")
     * @Method({"GET"})
     */
    public function promotionWordAction($id) {


        $em = $this->getDoctrine()->getManager();
        $patients = $em->getRepository('AppBundle:PPatient')->GetPatients($id);
        $folder = "RAPPORT PAR PROMOTION.doc";

        header('Content-Type: application/msword');
        header("Content-Disposition: attachment; filename=" . $folder);
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $html = $this->renderView('rapport/promotion_word.html.twig', array(
            'patients' => $patients,
        ));

        // $html=file_get_contents('mydocument.htm');
        //dump($html);die;
        //  $doc = new \DOMDocument();
        $doc = new \DOMDocument('1.0', 'UTF-8');
        //  $doc->loadHTML($html);
        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        //    dump($html);die;
        foreach ($doc->getElementsByTagName('img') as $key => $image) {


            /*   $img = $image->getAttribute('src');
              $img = str_replace('data:image/jpeg;base64,', '', $img);
              $img = str_replace('data:image/png;base64,', '', $img);
              $data = base64_decode($img);
              $file = 'images/rapports/test' . $key . '.png';
              $success = file_put_contents($file, $data);
              $image->setAttribute("src", $file); */




            $img = $image->getAttribute('src');
            $img = str_replace('../../../../', '', $img);

            // var_dump($img);
            //  die();
            // $img = str_replace('data:image/jpeg;base64,', '', $img);
            // $img = str_replace('data:image/png;base64,', '', $img);
            //  $data = base64_decode($img);
            // $var = uniqid();
            // $file = 'images/rapports/test' . $var . '.png';
            //$success = file_put_contents($file, $data);
            // $del = str_replace('images/rapports', 'images/promotionimages', $img);
            $del = str_replace('images/rapports', 'images/bar', $img);
            copy($img, $del);
            $del = str_replace('images/rapports', '../../images/bar', $img);

            $image->setAttribute("src", $del);

            // var_dump($del);
            //die();
            // delete unlink($file);
        }
        //dump($doc);
        //die();
        echo $doc->saveHTML($doc);

        //die();            
        return 1;






        /* $json_data = array( "data" => $data   // total data array
          );


          return new Response(json_encode($json_data)); */
    }
    
    
    
    
    
    

    /**
     * 
     * @Route("/images_base64/{id}",options = { "expose" = true } , name="images_base")
     * @Method({"GET"})
     */
    public function ImagesBaseAction($id) {
        ini_set('display_errors', 'on');
        ini_set('memory_limit', '8192M');
        set_time_limit(7200);
        $em = $this->getDoctrine();
        $query = $em->getEntityManager()->getRepository('AppBundle:TProcessus')->findAll();

        // $patient = $em->getRepository('AppBundle:PPatient')->findOneBy(array('id' => $id));
        // $query = $em->getEntityManager()->getRepository('AppBundle:TProcessus')->findBy(array('patient' => $patient));
        foreach ($query as $key1 => $value) {
            //  $doc = new \DOMDocument();
            $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);

            // $doc->loadHTML($value->getText());

            $doc->loadHTML(mb_convert_encoding($value->getText(), 'HTML-ENTITIES', 'UTF-8'));


            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/png;base64,', '', $img);
                $data = base64_decode($img);
                $var = uniqid();
                $file = 'images/rapports/test' . $var . '.png';
                $file2 = '../../../../images/rapports/test' . $var . '.png';
                $success = file_put_contents($file, $data);
                $image->setAttribute("src", $file2);
                // copy($file, 'images/bar/test' . $var . '.png');
                // delete unlink($file);
            }
            # remove <!DOCTYPE 
            $doc->removeChild($doc->doctype);

# remove <html><body></body></html> 
            // $doc->replaceChild($doc->firstChild->firstChild->firstChild, $doc->firstChild);
            // $dom->loadHTML(mb_convert_encoding($profile, 'HTML-ENTITIES', 'UTF-8'));
            $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);
            $value->setText($content);
            $this->getDoctrine()->getManager()->flush();
        }
        //  return 1;
        $lien = 1;
        $li = 1;
        return $this->render('rapport/image.html.twig', array(
                    'id' => 1,
                    'lien' => $lien,
                    'li' => $li,
        ));
    }
    
    
         /**
     * @Route("/sansobj_clinique/{id}/{type}", name="sansObjectif_clinique")
     */
    public function RapportsSansObjectif11Action($id, $type) {
        $session = new Session();
//  $session->get('inscription');

        $em = $this->getDoctrine()->getManager();
        $inscription = $em->getRepository('AppBundle:TInscription')->findOneBy(array('id' => $id));
        $st = $em->getRepository('AppBundle:SInsStg')->findOneBy(array('inscription' => $inscription, 'statut' => 0));
        $stage = $em->getRepository('AppBundle:SGroupStage')->findOneBy(array('id' => $st->getGroupe()));
          $clinique = $em->getRepository('AppBundle:PModule')->findOneBy(array('id' => $type));
       $patients = $em->getRepository('AppBundle:PPatient')->GetPatientsByModule($inscription,$stage,$clinique);
     //  dump($patients);
      // die();
        //$patients = $em->getRepository('AppBundle:PPatient')->findBy(array('inscription' => $inscription, 'groupe' => $stage));


        foreach ($patients as $key => $value) {
            foreach ($value->getProcessus() as $key => $proc) {
                $txt = $em->getRepository('AppBundle:PPatient')->remove_evil_styles_v2($proc->getText());

                $proc->setText($txt);
            }
        }

// $patients = $em->getRepository('AppBundle:PPatient')->find(array('id' => $id));
//        $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
//            'patients' => $patients
//            
//        ));

   


            $html = $this->renderView('rapport/etudiantSansObjectif.html.twig', array(
                'patients' => $patients,
                'inscription' => $inscription,
                'pic' => 'p',
                
            ));
            error_reporting(0);
            $doc = new \DOMDocument('1.0', 'UTF-8');
            libxml_use_internal_errors(true);

            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($doc->getElementsByTagName('img') as $key => $image) {


                $img = $image->getAttribute('src');
                $data = "";
                if (strpos($img, '../../../images/bar/') !== false) {
                    // die('xxxxxxxxxxxxx');
                    $img = str_replace('../../../images/bar/', 'images/bar/', $img);
                }



                $image->setAttribute("src", $img);
            }
                   $doc->removeChild($doc->doctype);
          $content = $doc->saveHTML($doc);
            $content = str_replace('<html><body>', '', $content);
            $content = str_replace('</body></html>', '', $content);

//var_dump($html);
//die();

          
        


 $mpdf = new \mPDF();
            $mpdf->SetDisplayMode('fullwidth');
            $mpdf->SetTitle('RAPPORT ETUDIANT');
            $mpdf->setFooter('<center>page : {PAGENO}</center>');
            $mpdf->WriteHTML($content);
            $mpdf->Output();






//        $mpdf = new \mPDF();
//        $mpdf->SetTitle('RAPPORT GLOBAL SANS OBJECTIF');
//        $mpdf->WriteHTML($html);
//        $mpdf->Output();
    }

}
