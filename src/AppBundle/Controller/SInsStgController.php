<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SInsStg;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Sinsstg controller.
 *
 * @Route("sinsstg")
 */
class SInsStgController extends Controller
{
    /**
     * Lists all sInsStg entities.
     *
     * @Route("/", name="sinsstg_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sInsStgs = $em->getRepository('AppBundle:SInsStg')->findAll();

        return $this->render('sinsstg/index.html.twig', array(
            'sInsStgs' => $sInsStgs,
        ));
    }

    /**
     * Finds and displays a sInsStg entity.
     *
     * @Route("/{id}", name="sinsstg_show")
     * @Method("GET")
     */
    public function showAction(SInsStg $sInsStg)
    {

        return $this->render('sinsstg/show.html.twig', array(
            'sInsStg' => $sInsStg,
        ));
    }
    

}
