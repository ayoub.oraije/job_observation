<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcFormation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Acformation controller.
 *
 * @Route("acformation")
 */
class AcFormationController extends Controller
{
    /**
     * Lists all acFormation entities.
     *
     * @Route("/", name="acformation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acFormations = $em->getRepository('AppBundle:AcFormation')->findAll();

        return $this->render('acformation/index.html.twig', array(
            'acFormations' => $acFormations,
        ));
    }

    /**
     * Creates a new acFormation entity.
     *
     * @Route("/new", name="acformation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $acFormation = new Acformation();
        $form = $this->createForm('AppBundle\Form\AcFormationType', $acFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acFormation);
            $em->flush();

            return $this->redirectToRoute('acformation_show', array('id' => $acFormation->getId()));
        }

        return $this->render('acformation/new.html.twig', array(
            'acFormation' => $acFormation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a acFormation entity.
     *
     * @Route("/{id}", name="acformation_show")
     * @Method("GET")
     */
    public function showAction(AcFormation $acFormation)
    {
        $deleteForm = $this->createDeleteForm($acFormation);

        return $this->render('acformation/show.html.twig', array(
            'acFormation' => $acFormation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing acFormation entity.
     *
     * @Route("/{id}/edit", name="acformation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AcFormation $acFormation)
    {
        $deleteForm = $this->createDeleteForm($acFormation);
        $editForm = $this->createForm('AppBundle\Form\AcFormationType', $acFormation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acformation_edit', array('id' => $acFormation->getId()));
        }

        return $this->render('acformation/edit.html.twig', array(
            'acFormation' => $acFormation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a acFormation entity.
     *
     * @Route("/{id}", name="acformation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AcFormation $acFormation)
    {
        $form = $this->createDeleteForm($acFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($acFormation);
            $em->flush();
        }

        return $this->redirectToRoute('acformation_index');
    }

    /**
     * Creates a form to delete a acFormation entity.
     *
     * @param AcFormation $acFormation The acFormation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AcFormation $acFormation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acformation_delete', array('id' => $acFormation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
