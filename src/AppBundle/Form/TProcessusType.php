<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class TProcessusType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextareaType::class,array('label' => 'Nouveau texte','attr' => array('class'=>'textarea summernote1 col-sm-12')))
                ->add('save', SubmitType::class, array('attr' => array(
                    'class' => 'btn btn-sm btn-primary loading_spinner_etudiant no-margin-left no-margin-top no-padding no-margin-bottom,',
                    'style'=>'padding: 1px;')))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary no-margin-left no-margin-top no-margin-bottom',
                    'style'=>'padding: 1px;'
                    )));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TProcessus'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tprocessus';
    }


}
