<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class s_stagesType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('designation', TextType::class, array('label' => 'Designation', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Designation')))
                ->add('abreviation', TextType::class, array('label' => 'Abreviation', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Abreviation')))
                
  
                
                ->add('fermer', ChoiceType::class, array(
                     'label' => 'Fermer (*)',
                     'attr' => array('class' => 'col-xs-10 col-sm-5 ', 'placeholder' => 'choisie '),
                    'choices' => array(
                        'OUI' => 'oui',
                        'NON' => 'non',
                    ),))
                
             
                
                ->add('formation', EntityType::class, array(
                    'label' => 'Formation (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 formation', 'placeholder' => 'choisie formation ' ,'disabled'=>'true'),
                    'class' => 'AppBundle:AcFormation',
                    'choice_label' => 'designation',
                    'placeholder' => 'Choix formation...'))
                   ->add('etablissement', EntityType::class, array(
                    'label' => 'Etablissement (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 etablissement', 'placeholder' => 'choisie Etablissement '),
                    'class' => 'AppBundle:AcEtablissement',
                    'choice_label' => 'designation'))
                   ->add('promotion', EntityType::class, array(
                    'label' => 'Promotion (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 promotion', 'placeholder' => 'choisie promotion ','disabled'=>'true'),
                    'class' => 'AppBundle:AcPromotion',
                    'choice_label' => 'designation',
                        'placeholder' => 'Choix Promotion...',))
                
                ->add('service' , EntityType::class, array(
                    'label' => 'Service (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 ', 'placeholder' => 'choisie Responsable'),
                    'class' => 'AppBundle:Service',
                    'choice_label' => 'designation'))
                
                ->add('specialite', EntityType::class, array(
                    'label' => 'Spécialité (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 ', 'placeholder' => 'choisie Spécialité'),
                    'class' => 'AppBundle:Specialite',
                    'choice_label' => 'designation'))
                
                ->add('responsable' , EntityType::class, array(
                    'label' => 'Responsable (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 ', 'placeholder' => 'choisie un Service '),
                    'class' => 'AppBundle:Responsable',
                    'choice_label' => 'designation'))
                  ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-sm btn-primary loading_spinner_admin')))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary')))
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\s_stages'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_s_stages';
    }

        
}
