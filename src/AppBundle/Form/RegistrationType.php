<?php

// src/AppBundle/Form/RegistrationType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
                ->add('etudiant', EntityType::class, array(
                    'label' => 'Choix Etudiant',
                    'attr' => array('class' => 'form-control'),
                    'class' => 'AppBundle:TEtudiant',
                    'query_builder' => function (EntityRepository $er) {
                        $result = $er->createQueryBuilder('u')
                                ->leftJoin('u.user', 'us')
                                ->where('us.etudiant is  null');

                        return $result;
                    },
                    'choice_label' => function ($etudiant) {
                        return $etudiant->getNom() . " " . $etudiant->getPrenom();
                    },
                    'required' => true,
                    'placeholder' => 'Choisissez un etudiant'))
                ->add('roles', ChoiceType::class, array(
                    'label' => 'Rôle Etudiant',
                    'attr' => array('class' => 'form-control', 'style' => 'height :27px'),
                    'choices' =>
                    array
                        (
                        'ROLE_ETUDIANT' => 'ROLE_ETUDIANT'
                    ),
                    'multiple' => true,
                    'required' => false,
                    'empty_data' => 'ROLE_ETUDIANT'
                ))
                ->add('defaultP', HiddenType::class, array(
                    'data' => ''))

        ;
    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getEtudiant() {
        return $this->getBlockPrefix();
    }

}
