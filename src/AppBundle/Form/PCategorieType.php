<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class PCategorieType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('designation', TextType::class, array('label' => 'Designation', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Designation')))
               
                     ->add('isPatient', ChoiceType::class, array('label' => 'Patient(*)', 'attr' => array(
                     'class' => 'col-xs-10 col-sm-5', 'placeholder' => 'patient'
                     
                     
                     ),
                      'choices' => array(
                        'OUI' => '1',
                        'NON' => '0',
                    ),))
              //  ->add('module')
                
                    ->add('module', EntityType::class, array(
                    'label' => 'module (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 ', 'placeholder' => 'choisie module '),
                    'class' => 'AppBundle:PModule',
                    'choice_label' => 'designation'))
                
                
                
                   ->add('rubrique', EntityType::class, array(
                    'multiple' => 'multiple',
                    'attr' => array('multiple' => 'multiple', 'id' => 'duallist'),
                    'class' => 'AppBundle:PRubrique',
                    'choice_label' => 'designation',))
                
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-sm btn-primary loading_spinner_admin')))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary')));
    }

/**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PCategorie'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_pcategorie';
    }

}
