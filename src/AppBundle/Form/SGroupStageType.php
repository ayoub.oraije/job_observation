<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SGroupStageType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('observation', TextType::class, array('label' => 'Designation', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Designation')))
                ->add('dateDebut', DateType::class, array('widget' => 'single_text', 'label' => 'Date début', 'attr' => array('class' => 'col-xs-10 col-sm-5 ',)))
                ->add('dateFin', DateType::class, array('widget' => 'single_text', 'label' => 'Date Fin', 'attr' => array('class' => 'col-xs-10 col-sm-5 ',
            )))
                ->add('active', ChoiceType::class, array('label' => 'Active(*)', 'attr' => array(
                        'class' => 'col-xs-10 col-sm-5', 'placeholder' => 'active'
                    ),
                    'choices' => array(
                        'OUI' => '1',
                        'NON' => '0',
                    ),))
                /*  ->add('fermer', ChoiceType::class, array(
                  'label' => 'Fermer (*)',
                  'attr' => array('class' => 'col-xs-10 col-sm-5 ', 'placeholder' => 'choisie Formation'),
                  'choices' => array(
                  'OUI' => '1',
                  'NON' => '0',
                  ),)) */
                ->add('stage', EntityType::class, array(
                    'multiple' => 'multiple',
                    'attr' => array('multiple' => 'multiple', 'id' => 'duallist'),
                    'class' => 'AppBundle:s_stages',
                    'choice_label' => 'designation',))
                // ->add('stage')
                ->add('formation', EntityType::class, array(
                    'attr' => array('class' => 'col-xs-10 col-sm-5 formation','disabled'=>'true'),
                    'label' => 'Formation (*)',
                    'class' => 'AppBundle:AcFormation',
                    'choice_label' => 'designation',
                    'placeholder' => 'Choix formation...',
                    'empty_data' => null,
                ))
                ->add('etablissement', EntityType::class, array(
                    'label' => 'Etablissement (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 etablissement', 'placeholder' => 'choisie Etablissement '),
                    'class' => 'AppBundle:AcEtablissement',
                    'choice_label' => 'designation',
                    'placeholder' => 'Choix Etablissement...',))
                ->add('promotion', EntityType::class, array(
                    'label' => 'Promotion (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 promotion', 'placeholder' => 'choisie promotion ','disabled'=>'true'),
                    'class' => 'AppBundle:AcPromotion',
                    'choice_label' => 'designation',
                    'placeholder' => 'Choix Promotion...',))
                ->add('semestre', EntityType::class, array(
                    'label' => 'Semestre (*)',
                    'attr' => array('class' => 'col-xs-10 col-sm-5 semestre', 'placeholder' => 'choisie semestre ','disabled'=>'true'),
                    'class' => 'AppBundle:AcSemestre',
                    'choice_label' => 'designation',
                    'placeholder' => 'Choix Semestre...',))
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-sm btn-primary loading_spinner_admin')))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary')))
        ;
    }

/**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SGroupStage'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_sgroupstage';
    }

}
