<?php

namespace AppBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType; 
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\User;

class UserType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('firstname', TextType::class, array('label' => 'Prénom', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Prénom')))
                ->add('lastname', TextType::class, array('label' => 'Nom', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Nom')))
                ->add('username', TextType::class, array('label' => 'Non d\'utilisateur', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Nom d\'utilisateur')))
                ->add('email', EmailType::class, array('label' => 'Adresse mail', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'adresse mail')))
                //->add('roles', ChoiceType::class, array('label' => 'Role', 'mapped' => true, 'multiple' => false, 'choices' => array('ROLE_ADMIN' => 'ROLE_ADMIN', 'ROLE_USER' => 'ROLE_USER', 'ROLE_ETUDIANT' => 'ROLE_ETUDIANT', 'ROLE_ENSEIGNANT' => 'ROLE_ENSEIGNANT',), 'required' => true, 'attr' => array('class' => 'col-xs-10 col-sm-5')))
                ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                    'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options'=> array('label' => 'form.password', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Mot de passe')),
                    'second_options'=> array('label' => 'form.password_confirmation', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Répéter le mot de passe')),
                    'invalid_message'=> 'fos_user.password.mismatch',
                ))
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-sm btn-primary'),))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary')))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_user';
    }

}
