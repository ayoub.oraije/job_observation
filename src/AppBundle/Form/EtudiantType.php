<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtudiantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code')->add('codeCab')->add('inscriptionValide')->add('nom')->add('prenom')->add('urlImage')->add('titre')->add('dateNaissance')->add('lieuNaissance')->add('sexe')->add('stFamille')->add('stFamilleParent')->add('nationalite')->add('cin')->add('passeport')->add('adresse')->add('ville')->add('tel1')->add('tel2')->add('tel3')->add('mail1')->add('mail2')->add('nomP')->add('prenomP')->add('nationaliteP')->add('professionP')->add('employeP')->add('categorieP')->add('telP')->add('mailP')->add('salaireP')->add('nomM')->add('prenomM')->add('nationaliteM')->add('professionM')->add('employeM')->add('categorieM')->add('telM')->add('mailM')->add('salaireM')->add('cne')->add('idAcademie')->add('etablissement')->add('idFiliere')->add('idTypeBac')->add('anneeBac')->add('moyenneBac')->add('concoursMedbup')->add('obs')->add('idEtablissement')->add('idFormation')->add('langueConcours')->add('categoriePreinscription')->add('fraisPreinscription')->add('bourse')->add('logement')->add('parking')->add('statut')->add('actif')->add('idNatureDemande')->add('codeOrganisme')->add('nombreEnfants')->add('rangP')->add('rangS')->add('categorieListe')->add('admissionListe')->add('teleListe')->add('statutDeliberation')->add('dateCreation')->add('utilisateur');
    }
    
    
    
    
   
    
    
    
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Etudiant'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_etudiant';
    }


}
