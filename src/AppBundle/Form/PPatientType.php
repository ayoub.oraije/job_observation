<?php

namespace AppBundle\Form;

//use App\Entity\PEncadrant;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class PPatientType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('designation', TextareaType::class, array('label' => 'Observation', 'attr' => array('class' => 'col-xs-10 col-sm-5')))
                ->add('intitule', TextType::class, array('label' => 'Intitule', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Intitule')))
                ->add('nom', TextType::class, array('label' => 'Nom', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Nom')))
                ->add('prenom', TextType::class, array('label' => 'Prénom', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Prénom')))
                ->add('ipp', TextType::class, array('label' => 'IPP', 'attr' => array('class' => 'col-xs-10 col-sm-5', 'placeholder' => 'IPP')))
                ->add('cat', HiddenType::class, array('attr' => array('class' => 'catclinique'), 'mapped' => false))
                ->add('encNiv1', EntityType::class, array(
                    'label' => 'Encadrement Universitaire 1',
                    'attr' => array('class' => 'col-xs-10 col-sm-10'),
                    'class' => 'AppBundle:PEncadrant',
                    'required' => false,
                    'placeholder' => 'Choix',
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.type=:v')->setParameter('v', 'univ');
                    },
                    'choice_label' => 'nom'))
                ->add('encNiv2', EntityType::class, array(
                    'label' => 'Encadrement Universitaire 2',
                    'attr' => array('class' => 'col-xs-10 col-sm-10'),
                    'class' => 'AppBundle:PEncadrant',
                    'required' => false,
                    'placeholder' => 'Choix',
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.type=:v')->setParameter('v', 'univ');
                    },
                    'choice_label' => 'nom'))
                ->add('encHop1', EntityType::class, array(
                    'label' => 'Encadrement Hospitalier 1',
                    'attr' => array('class' => 'col-xs-10 col-sm-10'),
                    'class' => 'AppBundle:PEncadrant',
                    'required' => false,
                    'placeholder' => 'Choix',
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.type=:v')->setParameter('v', 'hop');
                    },
                    'choice_label' => 'nom'))
                ->add('encHop2', EntityType::class, array(
                    'label' => 'Encadrement Hospitalier 2',
                    'attr' => array('class' => 'col-xs-10 col-sm-10'),
                    'class' => 'AppBundle:PEncadrant',
                    'required' => false,
                    'placeholder' => 'Choix',
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.type=:v')->setParameter('v', 'hop');
                    },
                    'choice_label' => 'nom'))
                ->add('encRes1', EntityType::class, array(
                    'label' => 'Encadrement Résident 1',
                    'attr' => array('class' => 'col-xs-10 col-sm-10'),
                    'class' => 'AppBundle:PEncadrant',
                    'required' => false,
                    'placeholder' => 'Choix',
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.type=:v')->setParameter('v', 'res');
                    },
                    'choice_label' => 'nom'))
                ->add('encRes2', EntityType::class, array(
                    'label' => 'Encadrement Résident 2',
                    'attr' => array('class' => 'col-xs-10 col-sm-10'),
                    'class' => 'AppBundle:PEncadrant',
                    'required' => false,
                    'placeholder' => 'Choix',
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.type=:v')->setParameter('v', 'res');
                    },
                    'choice_label' => 'nom'))
                ->add('autre', TextareaType::class, array('label' => 'Autre', 'attr' => array('class' => 'col-xs-10 col-sm-10')))
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-sm btn-primary loading_spinner_etudiant')))
                ->add('reset', ResetType::class, array('attr' => array('class' => 'btn btn-sm btn-secondary')))
        ;
    }

/**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PPatient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_ppatient';
    }

}
